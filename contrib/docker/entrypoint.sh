#!/bin/sh
# For docker container entrypoint compatibility sake, run this in sh, not bash

nohup rsyslogd -n &
sleep 1
read -r RSYSLOG_PID <"/var/run/rsyslogd.pid"
# shellcheck disable=SC2068
$@
sleep 1
kill -INT "$RSYSLOG_PID"
sleep 1
if [ -f "/var/run/rsyslogd.pid" ]; then
  read -r RSYSLOG_PID <"/var/run/rsyslogd.pid"
  kill -TERM "$RSYSLOG_PID"
  sleep 2
  if [ -f "/var/run/rsyslogd.pid" ]; then
    read -r RSYSLOG_PID <"/var/run/rsyslogd.pid"
    kill -9 "$RSYSLOG_PID"
  fi
fi
