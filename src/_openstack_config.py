# -*- coding: utf-8 -*-
#
import sys
from os import getenv
from .utils import do_load_dotenv

do_load_dotenv()
undef = object()


module = sys.modules[__name__]
defaults = module.defaults = {
    "OS_AUTH_TYPE": "v3applicationcredential",
    "OS_AUTH_URL": "https://keystone.rc.nectar.org.au:5000/v3/",
    "OS_IDENTITY_API_VERSION": 3,
    "ST_AUTH_VERSION": 3,
    "OS_REGION_NAME": "Melbourne", # I think this is always Melbourne, for Objectstore
    "OS_INTERFACE": "public",
    "OS_APPLICATION_CREDENTIAL_ID": undef, #must provide
    "OS_APPLICATION_CREDENTIAL_SECRET": undef, #must provide
    "OS_APPLICATION_CREDENTIAL_NAME": "my-credential-name", #should override
    "OS_PROJECT_ID": "asdf-1234", #must provide real one
    "OS_PROJECT_NAME": "My-Nectar-Project-Name",
}
config = module.config = dict()

config['OS_AUTH_TYPE'] = getenv("OS_AUTH_TYPE", undef)
config['OS_AUTH_URL'] = getenv("OS_AUTH_URL", undef)
config['OS_IDENTITY_API_VERSION'] = getenv("OS_IDENTITY_API_VERSION", undef)
config['ST_AUTH_VERSION'] = getenv("ST_AUTH_VERSION", undef)
config['OS_REGION_NAME'] = getenv("OS_REGION_NAME", undef)
config['OS_INTERFACE'] = getenv("OS_INTERFACE", undef)
config['OS_APPLICATION_CREDENTIAL_ID'] = getenv("OS_APPLICATION_CREDENTIAL_ID", undef)
config['OS_APPLICATION_CREDENTIAL_SECRET'] = getenv("OS_APPLICATION_CREDENTIAL_SECRET", undef)
config['OS_APPLICATION_CREDENTIAL_NAME'] = getenv("OS_APPLICATION_CREDENTIAL_NAME", undef)
config['OS_PROJECT_ID'] = getenv("OS_PROJECT_ID", undef)
config['OS_PROJECT_NAME'] = getenv("OS_PROJECT_NAME", undef)

for k, v in defaults.items():
    if k not in config or config[k] is undef:
        if v is undef:
            raise RuntimeError(f"No default value available for config key \"{k}\"")
        config[k] = v

OS_AUTH_TYPE = config['OS_AUTH_TYPE']
OS_AUTH_URL = config['OS_AUTH_URL']
OS_IDENTITY_API_VERSION = int(config['OS_IDENTITY_API_VERSION'])
ST_AUTH_VERSION = int(config['ST_AUTH_VERSION'])
OS_REGION_NAME = config['OS_REGION_NAME']
OS_INTERFACE = config['OS_INTERFACE']
OS_APPLICATION_CREDENTIAL_ID = config['OS_APPLICATION_CREDENTIAL_ID']
OS_APPLICATION_CREDENTIAL_SECRET = config['OS_APPLICATION_CREDENTIAL_SECRET']
OS_APPLICATION_CREDENTIAL_NAME = config['OS_APPLICATION_CREDENTIAL_NAME']
OS_PROJECT_ID = config['OS_PROJECT_ID']
OS_PROJECT_NAME = config['OS_PROJECT_NAME']
