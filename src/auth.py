import base64
import datetime
import string
from asyncio import AbstractEventLoop
from hashlib import sha1
from inspect import isawaitable
from functools import wraps, partial
from typing import TYPE_CHECKING

from authlib.common.security import generate_token
from authlib.oauth2.rfc6749 import OAuth2Token
from cachetools import TTLCache, cached
from authlib.integrations.httpx_client import AsyncOAuth2Client
from sanic import Blueprint
from sanic.request import Request
from sanic.exceptions import Forbidden, Unauthorized, ServerError
from sanic.response import redirect, text

if TYPE_CHECKING:
    from typing import Dict
    from authlib import oauth2
    from sanic.response import HTTPResponse


try:
    from .utils import aiocached
except ImportError:
    from src.utils import aiocached

try:
    from pandas._libs.json import ujson_loads as loads, ujson_dumps as dumps
except ImportError:
    try:
        from ujson import loads, dumps
    except ImportError:
        from json import loads, dumps

from ._auth_config import TERN_KEYCLOAK_CLIENT_ID, TERN_KEYCLOAK_CLIENT_SECRET, TERN_KEYCLOAK_TOKEN_URL
from ._auth_config import LANDSCAPES_KEYCLOAK_CLIENT_ID, LANDSCAPES_KEYCLOAK_CLIENT_SECRET, LANDSCAPES_KEYCLOAK_TOKEN_URL, LANDSCAPES_KEYCLOAK_AUTH_URL, LANDSCAPES_KEYCLOAK_CALLBACK_URI
from ._auth_config import KEY_VALIDATE_ENDPOINT, LANDSCAPES_KEYCLOAK_USERINFO_URL, LANDSCAPES_KEYCLOAK_COOKIE_DOMAIN


TERN_APP_CLIENT = AsyncOAuth2Client(
    client_id=TERN_KEYCLOAK_CLIENT_ID,
    client_secret=TERN_KEYCLOAK_CLIENT_SECRET,
    token_endpoint=TERN_KEYCLOAK_TOKEN_URL,
    token_endpoint_auth_method="client_secret_post",
    grant_type="client_credentials",
    scope="offline_access",
)


LANDSCAPES_APP_CLIENT = AsyncOAuth2Client(
    client_id=LANDSCAPES_KEYCLOAK_CLIENT_ID,
    client_secret=LANDSCAPES_KEYCLOAK_CLIENT_SECRET,
    token_endpoint=LANDSCAPES_KEYCLOAK_TOKEN_URL,
    redirect_uri=LANDSCAPES_KEYCLOAK_CALLBACK_URI,
    #token_endpoint_auth_method="client_secret_post",
    grant_type="authorization_code",
    scope="email offline_access",
)

auth_bp = Blueprint("auth_bp")


def do_refresh_token(client: AsyncOAuth2Client, loop: AbstractEventLoop, repeat: bool = False):
    loop.create_task(_refresh_token(client, loop, repeat=repeat))


async def _refresh_token(client: AsyncOAuth2Client, loop: AbstractEventLoop, repeat: bool = False):
    rtoken = client.token.get('refresh_token')
    url = client.metadata.get('token_endpoint')
    if not url or not rtoken:
        return
    # refresh_token should inherit scope from client.scope (including offline_access)
    a = await client.refresh_token(url, refresh_token=rtoken)
    print(f"{client.client_id} Token refreshed!", flush=True)
    if a is None or "access_token" not in a or not a["access_token"]:
        raise ServerError(f"Could not refresh access token for {repr(client)}.")
    if repeat and "refresh_token" in a:
        try:
            expires_in = int(a["refresh_expires_in"])
        except ValueError:
            expires_in = 3600  # 1 hour
        if expires_in >= 80:
            interval = expires_in - 20  # give a 20s buffer
        elif expires_in >= 4:
            interval = expires_in - 2  # 2s buffer
        else:
            return  # can't auto-renew
        loop.call_later(interval, do_refresh_token, client, loop, repeat)


@auth_bp.before_server_start
async def make_tern_client_token(app, loop: AbstractEventLoop):
    a = await TERN_APP_CLIENT.fetch_token(grant_type="client_credentials", scope="offline_access")
    if a is None or "access_token" not in a or not a["access_token"]:
        raise ServerError("Cannot log in to the TERN Keycloak to get our access token.")
    app.ctx.tern_app_client = TERN_APP_CLIENT
    if "refresh_token" in a:
        try:
            expires_in = int(a["refresh_expires_in"])
        except ValueError:
            expires_in = 3600  # 1 hour
        if expires_in >= 80:
            interval = expires_in - 20  # give a 20s buffer
        elif expires_in >= 4:
            interval = expires_in - 2  # 2s buffer
        else:
            return  # can't auto-renew
        loop.call_later(interval, do_refresh_token, TERN_APP_CLIENT, loop, True)


@auth_bp.before_server_start
async def init_landscapes_client(app, loop: AbstractEventLoop):
    app.ctx.tern_session = TTLCache(1000, 300)  # max 1000 memory, each max 300 seconds
    app.ctx.landscapes_app_client = LANDSCAPES_APP_CLIENT
    #a = await LANDSCAPES_APP_CLIENT.fetch_token(grant_type="authorization_code")
    ...


@auth_bp.route("/login", methods=("GET","HEAD","OPTIONS"))
async def login_route(request: Request):
    app_ctx = request.app.ctx
    from_ = request.args.get("from", None)
    redir_url, state = LANDSCAPES_APP_CLIENT.create_authorization_url(LANDSCAPES_KEYCLOAK_AUTH_URL)
    app_ctx.tern_session[state] = {"from": from_}
    response = redirect(redir_url, status=302)
    salt = generate_token(8, string.digits)
    state_raw = "".join((salt, state))
    response.cookies["landscapes_login_salt"] = salt
    response.cookies["landscapes_login_salt"]["samesite"] = "lax"
    response.cookies["landscapes_login_salt"]["max-age"] = 300

    response.cookies["landscapes_login_state"] = base64.b64encode(sha1(state_raw.encode("ascii")).digest()).decode("ascii")
    response.cookies["landscapes_login_state"]["samesite"] = "lax"
    response.cookies["landscapes_login_state"]["max-age"] = 300

    return response


@auth_bp.route("/callback", methods=("GET","HEAD","OPTIONS"))
async def keycloak_callback(request: Request):
    app_ctx = request.app.ctx
    landscapes_app_client = app_ctx.landscapes_app_client
    state = request.args.get("state", None)
    code = request.args.get("code", None)
    if state is None:
        return text("Bad callback state.", status=403)
    request_state = request.cookies.get("landscapes_login_state", None)
    request_state_salt = request.cookies.get("landscapes_login_salt", None)
    if request_state is None or request_state_salt is None:
        return text("Bad callback browser state.", status=403)
    state_raw_compare = "".join((request_state_salt, state))
    state_raw_compare_hash = sha1(state_raw_compare.encode("ascii")).digest()
    state_raw_hash = base64.b64decode(request_state)
    if state_raw_compare_hash != state_raw_hash:
        return text("Bad callback browser state.", status=403)
    if code is None:
        return text("Callback must receive a code from KeyCloak.", status=403)
    state_dict = app_ctx.tern_session.get(state, None)
    if state_dict is None:
        return text("Bad callback state (callback timed out).", status=403)
    try:
        token = await landscapes_app_client.fetch_token(grant_type="authorization_code", code=code, state=state, scope="email offline_access")
    except Exception as e:
        return text("Bad authorization code. Perhaps Keycloak is misbehaving.", status=403)
    finally:
        # Never keep the token in here.
        landscapes_app_client.token = None
    from_ = state_dict.get("from", None)
    redir_url = from_ if from_ is not None and len(from_) else "/"
    response = redirect(redir_url, status=302)
    response.cookies["landscapes_access_token"] = dumps(token)
    response.cookies["landscapes_access_token"]["samesite"] = "lax"
    response.cookies["landscapes_access_token"]["max-age"] = 86400  # 24h
    if LANDSCAPES_KEYCLOAK_COOKIE_DOMAIN:
        response.cookies["landscapes_access_token"]["domain"] = LANDSCAPES_KEYCLOAK_COOKIE_DOMAIN
    return response

@aiocached(cache=TTLCache(maxsize=2048, ttl=3600))
async def check_tern_token(app=None, token=None):
    if app is None or token is None:
        raise Unauthorized("No token given")
    client: AsyncOAuth2Client = app.ctx.tern_app_client
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
    }
    rq = client.request("POST", KEY_VALIDATE_ENDPOINT, headers=headers,
                        content=b"{\"apikey\": \"%s\"}" % token.encode("ascii"))
    resp = await rq
    if resp.status_code in (400, 401, 403):
        raise Unauthorized("Bad APIKey")
    elif resp.status_code != 200:
        raise Unauthorized("Cannot determine APIKey Validity")
    try:
        user = loads(resp.content)
    except Exception:
        raise Unauthorized("Bad user")
    if "email" not in user or not user["email"]:
        raise Unauthorized("Unknown user")
    return user



@aiocached(cache=TTLCache(maxsize=2048, ttl=1200))  # 3600=1h, 1800=30m, 1200=20m
async def check_landscapes_token(app, token: str = None):
    if token is None:
        raise Unauthorized("No token given")

    client: AsyncOAuth2Client = app.ctx.landscapes_app_client
    headers = {
        "Accept": "application/json"
    }
    client_auth = client.client_auth("client_secret_post")
    rq = client.request('POST', f"{LANDSCAPES_KEYCLOAK_TOKEN_URL}/introspect", headers=headers, auth=client_auth, data={"token": token})
    resp = await rq
    if resp.status_code in (400, 401, 403):
        raise Unauthorized("Invalid Landscapes Access Token")
    elif resp.status_code != 200:
        raise Unauthorized("Cannot determine Token Validity")
    try:
        intro = loads(resp.content)
    except Exception:
        raise Unauthorized("Bad access token response.")
    if "active" not in intro or not intro["active"] or intro["active"] == "false":
        return False
    expires = intro.get("exp", intro.get("expires", 0))
    if "sub" in intro and "email" in intro:
        user = intro
    elif "sub" in intro and "preferred_username" in intro:
        intro["email"] = f"{intro['preferred_username']}@auth.tern.org.au"
        user = intro
    else:
        token = OAuth2Token({"access_token": token, "token_type": "bearer", "scope": "email"})
        token_auth = client.token_auth_class(token=token)
        rq = client.request('GET', LANDSCAPES_KEYCLOAK_USERINFO_URL, headers=headers, auth=token_auth)
        resp = await rq
        if resp.status_code in (400, 401, 403):
            raise Unauthorized("Bad Landscapes Access Token")
        elif resp.status_code != 200:
            raise Unauthorized("Cannot determine Token Validity")
        try:
            user = loads(resp.content)
        except Exception:
            raise Unauthorized("Bad user")
    if "email" not in user or not user["email"]:
        if "preferred_username" in user:
            user["email"] = f"{user['preferred_username']}@auth.tern.org.au"
        else:
            raise Unauthorized("Unknown user")
    return user, expires


async def renew_landscapes_token(app, token: dict):
    client: AsyncOAuth2Client = app.ctx.landscapes_app_client
    refresh_url = client.metadata.get('token_endpoint')
    new_token = await client.refresh_token(refresh_url, refresh_token=token['refresh_token'])
    token.update(new_token)
    client.token = None
    return token


async def check_and_update_landscapes_token(app, token: dict):
    now = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).timestamp()
    success = await check_landscapes_token(app, token['access_token'])
    if not success:
        return False, {}
    user, expires = success
    if expires is None or expires == 0:
        # Can't renew token
        return user, token
    if "refresh_token" in token and (expires - 80) < now:
        old_access_token = token['access_token']
        token = await renew_landscapes_token(app, token)
        new_access_token = token['access_token']
        if old_access_token == new_access_token:
            check_landscapes_token.uncache(app, old_access_token)
        success = await check_landscapes_token(app, new_access_token)
        if not success:
            return False, {}
        user, expires = success
    return user, token


async def _require_auth(request: Request, *args, real_f=None, dologin: bool = True, **kwargs):
    auth_header = request.headers.getone("authorization", None)
    q_apikey = request.args.get("apikey", None)
    tern_token = None
    landscapes_token = None
    if q_apikey:
        tern_token = q_apikey
    elif auth_header is not None:
        parts = auth_header.split(" ", 1)
        if len(parts) < 2:
            raise Unauthorized("Invalid authorization header.", scheme="Basic")
        scheme = parts[0].lower()
        if scheme == "apikey-v1":
            request.ctx.tern_token = tern_token = parts[1]
        elif scheme == "bearer":
            landscapes_token = OAuth2Token({"access_token": parts[1], "token_type": "bearer", "scope": "email"})
            request.ctx.landscapes_token = landscapes_token
        elif scheme == "basic":
            try:
                decoded = base64.b64decode(parts[1]).decode('utf-8')
            except Exception:
                raise
            basic_parts = [b.strip() for b in decoded.split(":", 1)]
            if len(basic_parts) < 2:
                raise Unauthorized("Invalid authorization header.", scheme="Basic")
            if basic_parts[0].lower() == "apikey":
                request.ctx.tern_token = tern_token = basic_parts[1]
            elif basic_parts[0].lower() == "landscapes":
                landscapes_token = OAuth2Token(
                    {"access_token": basic_parts[1], "token_type": "bearer", "scope": "email"}
                )
                request.ctx.landscapes_token = landscapes_token
            else:
                request.ctx.tern_token = tern_token = basic_parts[0]
        else:
            raise Unauthorized("Please use HTTP-Basic or Apikey-v1 authorization.", scheme="Basic")
    if tern_token is None and landscapes_token is None and "landscapes_access_token" in request.cookies:
        landscapes_token_json = request.cookies["landscapes_access_token"]
        request.ctx.landscapes_token = landscapes_token = loads(landscapes_token_json)
    user = None
    update_access_token_cookie = False
    if tern_token:
        user = await check_tern_token(app=request.app, token=tern_token)
    elif landscapes_token:
        user, token = await check_and_update_landscapes_token(app=request.app, token=landscapes_token)
        update_access_token_cookie = dumps(token)
    if not user:
        if not dologin:
            raise Unauthorized("Not authorized", scheme="Basic")
        from_ = f"{request.path}?{request.query_string}" if request.query_string else request.path
        return redirect(f"/login?from={from_}")
    request.ctx.user = user
    if not real_f:
        resp = HTTPResponse()
    else:
        resp = real_f(request, *args, **kwargs)
    if isawaitable(resp):
        resp = await resp  # type: HTTPResponse
    if update_access_token_cookie:
        resp.cookies["landscapes_access_token"] = update_access_token_cookie
        resp.cookies["landscapes_access_token"]["samesite"] = "lax"
        resp.cookies["landscapes_access_token"]["max-age"] = 86400  # 24h
        if LANDSCAPES_KEYCLOAK_COOKIE_DOMAIN:
            resp.cookies["landscapes_access_token"]["domain"] = LANDSCAPES_KEYCLOAK_COOKIE_DOMAIN
    return resp

async def _optional_auth(request: Request, *args, real_f=None, **kwargs):
    auth_header = request.headers.getone("authorization", None)
    q_apikey = request.args.get("apikey", None)
    tern_token = None
    landscapes_token = None
    if q_apikey:
        tern_token = q_apikey
    elif auth_header is not None:
        parts = auth_header.split(" ", 1)
        if len(parts) < 2:
            request.ctx.landscapes_token = None
            request.ctx.tern_token = None
        else:
            scheme = parts[0].lower()
            if scheme == "apikey-v1":
                request.ctx.tern_token = tern_token = parts[1]
            elif scheme == "bearer":
                landscapes_token = OAuth2Token(
                    {"access_token": parts[1], "token_type": "bearer", "scope": "email"}
                )
                request.ctx.landscapes_token = landscapes_token
            elif scheme == "basic":
                try:
                    decoded = base64.b64decode(parts[1]).decode('utf-8')
                except Exception:
                    decoded = ""
                basic_parts = [b.strip() for b in decoded.split(":", 1)]
                if len(basic_parts) < 2:
                    request.ctx.tern_token = tern_token = None
                else:
                    if basic_parts[0].lower() == "apikey":
                        request.ctx.tern_token = tern_token = basic_parts[1]
                    elif basic_parts[0].lower() == "landscapes":
                        landscapes_token = OAuth2Token(
                            {"access_token": basic_parts[1], "token_type": "bearer", "scope": "email"}
                        )
                        request.ctx.landscapes_token = landscapes_token
                    else:
                        request.ctx.tern_token = tern_token = basic_parts[0]
    if tern_token is None and landscapes_token is None and "landscapes_access_token" in request.cookies:
        landscapes_token_json = request.cookies["landscapes_access_token"]
        request.ctx.landscapes_token = landscapes_token = loads(landscapes_token_json)
    user = None
    update_access_token_cookie = False
    if tern_token:
        user = await check_tern_token(app=request.app, token=tern_token)
    elif landscapes_token:
        user, token = await check_and_update_landscapes_token(app=request.app, token=landscapes_token)
        update_access_token_cookie = dumps(token)
    if not user:
        request.ctx.user = None
    else:
        request.ctx.user = user
    if not real_f:
        resp = HTTPResponse()
    else:
        resp = real_f(request, *args, **kwargs)
    if isawaitable(resp):
        resp = await resp  # type: HTTPResponse
    if user and update_access_token_cookie:
        resp.cookies["landscapes_access_token"] = update_access_token_cookie
        resp.cookies["landscapes_access_token"]["samesite"] = "lax"
        resp.cookies["landscapes_access_token"]["max-age"] = 86400  # 24h
        if LANDSCAPES_KEYCLOAK_COOKIE_DOMAIN:
            resp.cookies["landscapes_access_token"]["domain"] = LANDSCAPES_KEYCLOAK_COOKIE_DOMAIN
    return resp


def require_auth(*args, dologin=True, **kwargs):
    if len(args) == 1 and (isinstance(args[0], type) or callable(args[0])):
        # calling the decorator with no params
        f_or_type = args[0]
        if isinstance(f_or_type, type):
            # decorating a class
            dec = f_or_type._decorators
            if not dec:
                dec = f_or_type._decorators = []
            dec.append((require_auth, [], {}))
            return f_or_type
        elif callable(f_or_type):
            f = f_or_type
            return wraps(f)(partial(_require_auth, real_f=f, dologin=True))
        else:
            return f_or_type

    # calling the decorator with params
    def require_auth_wrapper(f_or_type2):
        if isinstance(f_or_type2, type):
            # decorating a class
            dec = f_or_type2._decorators
            if not dec:
                dec = f_or_type2._decorators = []
            dec.append((require_auth, args, {"dologin": dologin, **kwargs}))
            return f_or_type2
        else:
            return wraps(f_or_type2)(partial(_require_auth, real_f=f_or_type2, dologin=dologin))
    return require_auth_wrapper

def optional_auth(*args, **kwargs):
    if len(args) == 1 and (isinstance(args[0], type) or callable(args[0])):
        # calling the decorator with no params
        f_or_type = args[0]
        if isinstance(f_or_type, type):
            # decorating a class
            dec = f_or_type._decorators
            if not dec:
                dec = f_or_type._decorators = []
            dec.append((optional_auth, [], {}))
            return f_or_type
        elif callable(f_or_type):
            f = f_or_type
            return wraps(f)(partial(_optional_auth, real_f=f))
        else:
            return f_or_type

    # calling the decorator with params
    def optional_auth_wrapper(f_or_type2):
        if isinstance(f_or_type2, type):
            # decorating a class
            dec = f_or_type2._decorators
            if not dec:
                dec = f_or_type2._decorators = []
            dec.append((optional_auth, args, kwargs))
            return f_or_type2
        else:
            return wraps(f_or_type2)(partial(_optional_auth, real_f=f_or_type2))
    return optional_auth_wrapper


