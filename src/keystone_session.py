import logging
import os
import sys
from functools import partial
from os import path
from shutil import rmtree

import swiftclient
from keystoneauth1.identity import V3ApplicationCredential, V3Token
from keystoneauth1 import session
from ._openstack_config import config
from .utils import isostring_to_datetime


def get_keystone_app_credential_session(options) -> session.Session:
    if get_keystone_app_credential_session.cached is not None:
        return get_keystone_app_credential_session.cached
    args = {
        'application_credential_secret': options.get('os_application_credential_secret', None),
        'application_credential_id': options.get('os_application_credential_id', None),
        'application_credential_name': options.get('os_application_credential_name', None),

        #'project_id': options.get('os_project_id', None),
        #'project_name': options.get('os_project_name', None),

        #Application credentials cannot request a scope
        'project_id': None,
        'project_name': None,
    }
    auth = V3ApplicationCredential(options['auth'], **args)
    s = session.Session(auth=auth)
    get_keystone_app_credential_session.cached = s
    return s

get_keystone_app_credential_session.cached = None

def get_keystone_token_session(options, token) -> session.Session:
    if get_keystone_token_session.cached is not None:
        return get_keystone_token_session.cached

    auth = V3Token(options['auth'], token, **options)
    s = session.Session(auth=auth)
    get_keystone_token_session.cached = s
    return s
get_keystone_token_session.cached = None


