#
#
"""

"""
from typing import Optional

import pywps.configuration as config
from pywps import WPSRequest
from pywps.app import Process
from pywps.app.exceptions import ProcessError
from pywps.processing import Scheduler, MultiProcessing, Job, Processing
from pywps.response.basic import WPSResponse
from pywps.response.status import WPS_STATUS
from pywps import processing
import asyncio
import logging
import os
import sys

LOGGER = logging.getLogger("PYWPS")

class AsyncProcess(Process):
    def _run_async(self, wps_request, wps_response):
        loop = asyncio.get_event_loop()
        if loop is None:
            raise RuntimeError("Cannot start a asyncio processing task if not in an eventloop.")
        process = processing.Process(
            process=self,
            wps_request=wps_request,
            wps_response=wps_response)
        LOGGER.debug("Starting async process for request: {}".format(self.uuid))
        loop.create_task(process.start())

    async def _run_process(self, wps_request, wps_response):
        LOGGER.debug("Started processing request: {}".format(self.uuid))
        try:
            self._set_grass(wps_request)
            # if required set HOME to the current working directory.
            if config.get_config_value('server', 'sethomedir') is True:
                os.environ['HOME'] = self.workdir
                LOGGER.info('Setting HOME to current working directory: {}'.format(os.environ['HOME']))
            LOGGER.debug('ProcessID={}, HOME={}'.format(self.uuid, os.environ.get('HOME')))
            wps_response._update_status(WPS_STATUS.STARTED, 'PyWPS Process started', 0)
            await self.handler(wps_request, wps_response)  # the user must update the wps_response.
            # Ensure process termination
            if wps_response.status != WPS_STATUS.SUCCEEDED and wps_response.status != WPS_STATUS.FAILED:
                # if (not wps_response.status_percentage) or (wps_response.status_percentage != 100):
                LOGGER.debug('Updating process status to 100% if everything went correctly')
                wps_response._update_status(WPS_STATUS.SUCCEEDED, f'PyWPS Process {self.title} finished', 100)
        except Exception as e:
            import traceback
            traceback.print_exc()
            LOGGER.debug('Retrieving file and line number where exception occurred')
            exc_type, exc_obj, exc_tb = sys.exc_info()
            found = False
            while not found:
                # search for the _handler method
                m_name = exc_tb.tb_frame.f_code.co_name
                if m_name == '_handler':
                    found = True
                else:
                    if exc_tb.tb_next is not None:
                        exc_tb = exc_tb.tb_next
                    else:
                        # if not found then take the first
                        exc_tb = sys.exc_info()[2]
                        break
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            method_name = exc_tb.tb_frame.f_code.co_name

            # update the process status to display process failed

            msg = 'Process error: method={}.{}, line={}, msg={}'.format(fname, method_name, exc_tb.tb_lineno, e)
            LOGGER.error(msg)
            # In case of a ProcessError use the validated exception message.
            if isinstance(e, ProcessError):
                msg = "Process error: {}".format(e)
            # Only in debug mode we use the log message including the traceback ...
            elif config.get_config_value("logging", "level") != "DEBUG":
                # ... otherwise we use a sparse common error message.
                msg = 'Process failed, please check server error log'
            wps_response._update_status(WPS_STATUS.FAILED, msg, 100)

        finally:
            # The run of the next pending request if finished here, weather or not it successful
            self.launch_next_process()

        return wps_response

class AsyncioProcessing(Processing):
    """
    :class:`Processing` is an interface for running jobs.
    """

    job: Job
    task: Optional[asyncio.Task]

    def __init__(self, process, wps_request, wps_response):
        #self.job = Job(process, wps_request, wps_response)
        super(AsyncioProcessing, self).__init__(process, wps_request, wps_response)
        self.task = None

    async def start(self):
        loop = asyncio.get_event_loop()
        if loop is None:
            raise RuntimeError("Cannot start a asyncio processing task if not in an eventloop.")
        method = getattr(self.job.process, self.job.method)
        self.task = loop.create_task(method(self.job.wps_request, self.job.wps_response))

    def cancel(self):
        if self.task:
            self.task.cancel("Async wps task cancelled")

class ProcessFactory(object):
    def __init__(self):
        super(ProcessFactory, self).__init__()

    def __call__(self, process: Process, wps_request: WPSRequest, wps_response: WPSResponse):
        mode = config.get_config_value("processing", "mode")
        print("Processing mode: {}".format(mode))
        if mode == "scheduler":
            process = Scheduler(process, wps_request, wps_response)
        elif asyncio.iscoroutinefunction(getattr(process, "_run_process")):
            process = AsyncioProcessing(process, wps_request, wps_response)
        else:
            process = MultiProcessing(process, wps_request, wps_response)
        return process

def patch_pywps():
    setattr(processing, "Process", ProcessFactory())
