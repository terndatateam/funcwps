#
#
"""
Wrapper around pywps Service object
"""
import asyncio

from werkzeug.exceptions import HTTPException
from types import SimpleNamespace
from typing import TYPE_CHECKING, Sequence, Optional, Dict
from hashlib import md5
from pywps import Service, WPSRequest
from pywps.app.basic import get_default_response_mimetype, get_response_type
from pywps.dblog import store_status, log_request
from pywps.exceptions import NoApplicableCode, MissingParameterValue
from pywps.response.basic import WPSResponse
from pywps.response.capabilities import CapabilitiesResponse
from pywps.response.describe import DescribeResponse
from pywps.response.execute import ExecuteResponse
from pywps.response.status import WPS_STATUS
from sanic import Request, HTTPResponse
from sanic.exceptions import SanicException, ServerError, InvalidUsage
from sanic.log import logger
from uuid import uuid1
from pandas._libs.json import ujson_dumps as pandas_dumps
from werkzeug.datastructures import MIMEAccept
from werkzeug.http import parse_accept_header
from .async_process import patch_pywps
from ...audit import audit_this
from ...auth import require_auth, optional_auth


def make_response(doc, content_type):
    """response serializer"""
    if not content_type:
        content_type = get_default_response_mimetype()
    response = HTTPResponse(doc, content_type=content_type)
    return response



class AsyncWPSResponse(object):
    __slots__ = ("resp","_decorated_call")

    _decorators: list = []

    if TYPE_CHECKING:
        resp: WPSResponse

    def __init__(self, wps_response: WPSResponse):
        self.resp = wps_response
        _call = self._call
        if self._decorators:
            for (dec, args, kwargs) in self._decorators:
                if len(args) < 1 and len(kwargs) < 1:
                    _call = dec(_call)
                else:
                    _call = dec(*args, **kwargs)(_call)
        self._decorated_call = _call

    def _update_status(self, status, msg, percentage):
        return self.resp._update_status(status, msg, percentage)

    async def _call(self, sanic_request, wkz_request, *args, **kwargs):
        try:
            doc, content_type = self.resp.get_response_doc()
            return make_response(doc, content_type=content_type)
        except NoApplicableCode as e:
            return e
        except AttributeError as e:
            print(e, flush=True)
            raise ServerError()
        except Exception as e:
            print(e, flush=True)
            return NoApplicableCode(str(e))

    def __call__(self, sanic_request, wkz_request, *args, **kwargs):
        return self._decorated_call(sanic_request, wkz_request, *args, **kwargs)

@optional_auth
@audit_this
class AsyncCapabilitiesResponse(AsyncWPSResponse):
    __slots__ = tuple()

    def __init__(self, capabilities_response: CapabilitiesResponse):
        super(AsyncCapabilitiesResponse, self).__init__(capabilities_response)


@optional_auth
@audit_this
class AsyncDescribeResponse(AsyncWPSResponse):
    __slots__ = tuple()

    def __init__(self, describe_response: DescribeResponse):
        super(AsyncDescribeResponse, self).__init__(describe_response)

@optional_auth
@audit_this
class AsyncExecuteResponse(AsyncWPSResponse):
    __slots__ = tuple()

    def __init__(self, exec_response: ExecuteResponse):
        super(AsyncExecuteResponse, self).__init__(exec_response)

    async def _call(self, sanic_request, wkz_request, *args, **kwargs):
        # This function must return sanic HTTPResponse or an Exception
        resp = self.resp  # type: ExecuteResponse
        accept_json_response, accepted_mimetype = get_response_type(
            resp.wps_request.http_request.accept_mimetypes, resp.wps_request.default_mimetype)
        if resp.wps_request.raw:
            if resp.status == WPS_STATUS.FAILED:
                return NoApplicableCode(resp.message)
            else:
                wps_output_identifier = next(iter(resp.wps_request.outputs))  # get the first key only
                wps_output_value = resp.outputs[wps_output_identifier]
                response = wps_output_value.data
                if response is None:
                    return NoApplicableCode("Expected output was not generated")
                suffix = ''
                # if isinstance(wps_output_value, ComplexOutput):
                data_format = None
                if hasattr(wps_output_value, 'output_format'):
                    # this is set in the response, thus should be more precise
                    data_format = wps_output_value.output_format
                elif hasattr(wps_output_value, 'data_format'):
                    # this is set in the process' response _handler function, thus could have a few supported formats
                    data_format = wps_output_value.data_format
                if data_format is not None:
                    mimetype = data_format.mime_type
                    if data_format.extension is not None:
                        suffix = data_format.extension
                else:
                    # like LiteralOutput
                    mimetype = resp.wps_request.outputs[wps_output_identifier].get('mimetype', None)
                if not isinstance(response, (str, bytes, bytearray)):
                    if not mimetype:
                        mimetype = accepted_mimetype
                    json_response = mimetype and 'json' in mimetype
                    if json_response:
                        mimetype = 'application/json'
                        suffix = '.json'
                        # ensure_ascii=False means we can pass UTF-8 characters in the JSON
                        response = pandas_dumps(response, ensure_ascii=False)
                    else:
                        response = str(response)
                if not mimetype:
                    mimetype = None
                return HTTPResponse(response, content_type=mimetype,
                                    headers={'Content-Disposition': 'attachment; filename="{}"'
                                             .format(wps_output_identifier + suffix)})
        else:
            if not resp.doc:
                return NoApplicableCode("Output was not generated")
            return HTTPResponse(resp.doc, content_type=accepted_mimetype)


class AsyncService(Service):

    def __init__(self, app, bp=None, processes: Optional[Sequence] = None,
                 cfgfiles=None, preprocessors: Optional[Dict] = None):
        if processes is None:
            processes = []
        patch_pywps()
        super(AsyncService, self).__init__(processes=processes, cfgfiles=cfgfiles, preprocessors=preprocessors)
        self.app = app
        self.bp = bp

    async def get_capabilities(self, wps_request, uuid):
        cap = super(AsyncService, self).get_capabilities(wps_request, uuid)
        return AsyncCapabilitiesResponse(cap)

    async def describe(self, wps_request, uuid, identifiers):
        desc = super(AsyncService, self).describe(wps_request, uuid, identifiers)
        return AsyncDescribeResponse(desc)

    async def execute(self, identifier, wps_request, uuid):
        exer = super(AsyncService, self).execute(identifier, wps_request, uuid)
        if asyncio.iscoroutine(exer):
            exer = await exer
        return AsyncExecuteResponse(exer)

    async def __call__(self, http_request: Request):
        try:
            # This try block handle Exception generated before the request is accepted. Once the request is accepted
            # a valid wps_response must exist. To report error use the wps_response using
            # wps_response._update_status(WPS_STATUS.FAILED, ...).
            #
            # We need this behaviour to handle the status file correctly, once the request is accepted, a
            # status file may be created and failure must be reported in this file instead of a raw ows:ExceptionReport
            #
            # Exeception from CapabilityResponse and DescribeResponse are always catched by this try ... except close
            # because they never have status.

            request_uuid = uuid1()
            # This converts a Sanic Request to something
            # that looks like a Werkzeug HTTPRequest, for PyWPS
            wkz_http_request = SimpleNamespace(
                accept_mimetypes=parse_accept_header(http_request.headers.getone("accept", ""), MIMEAccept),
                method=http_request.method,
                args=http_request.args,
                path=http_request.path,
                content_type=http_request.content_type,
                content_length=0 if http_request.method != "POST" else int(http_request.headers.getone("content-length", "0")),
                get_data=(lambda : http_request.body),
            )
            try:
                wps_request = WPSRequest(wkz_http_request, self.preprocessors)
            except MissingParameterValue as m:
                raise InvalidUsage("Missing Parameter: {} in {}".format(m.description, m.locator))
            except AttributeError as e:
                print(e)
                raise InvalidUsage("Bad Request")
            logger.info(f'Request: {wps_request.operation}')
            if wps_request.operation in ['getcapabilities',
                                         'describeprocess',
                                         'execute']:
                log_request(request_uuid, wps_request)
                try:
                    response = None
                    # These set obj_path for audit logging purposes
                    if wps_request.operation == 'getcapabilities':
                        http_request.ctx.obj_path = f"wps:getcapabilities/{request_uuid}"
                        response = await self.get_capabilities(wps_request, request_uuid)
                    elif wps_request.operation == 'describeprocess':
                        identifiers = ",".join(wps_request.identifiers)
                        http_request.ctx.obj_path = f"wps:describeprocess/{request_uuid}/{identifiers}"
                        response = await self.describe(wps_request, request_uuid, wps_request.identifiers)
                    elif wps_request.operation == 'execute':
                        http_request.ctx.obj_path = f"wps:execute/{request_uuid}/{wps_request.identifier}"
                        response = await self.execute(
                            wps_request.identifier,
                            wps_request,
                            request_uuid
                        )
                    if isinstance(response, HTTPException):
                        raise
                    if isinstance(response, AsyncWPSResponse):
                        response = await response(http_request, wkz_http_request)
                    elif isinstance(response, WPSResponse) and callable(response):
                        response = response(wkz_http_request)

                except Exception:
                    # This ensures that logged request get terminated in case of exception while the request is not
                    # accepted
                    store_status(request_uuid, WPS_STATUS.FAILED, 'Request rejected due to exception', 100)
                    raise
                if isinstance(response, NoApplicableCode):
                    # PyWPS returned an error instead of a response?
                    raise response
                elif isinstance(response, HTTPException):
                    # PyWPS returned a Werkzeug error?
                    raise response
                elif isinstance(response, SanicException):
                    # Someone returned an exception instead of throwing it?
                    raise response
                return response
            else:
                raise ServerError("Unknown operation {}".format(wps_request.operation))

        except NoApplicableCode as e:
            # Turn a PyWPS exception into a Sanic Exception
            raise ServerError(e.get_description(), status_code=e.code)
        except HTTPException as e:
            # Turn a Flask/Werkzeug exception into a Sanic Exception
            raise ServerError(e.get_description(), status_code=e.code)
        except SanicException:
            # this is already a sanic error, let it pass directly to the sanic response handler
            raise
        except Exception:
            msg = "No applicable error code, please check error log."
            raise ServerError(msg, status_code=500)
