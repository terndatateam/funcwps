#
#
"""
Implementation of OGS WPS endpoints
"""
import os
import re
import time
from asyncio import AbstractEventLoop
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from pathlib import Path
from types import SimpleNamespace
from typing import Union
from sanic import Sanic, Blueprint, Request, HTTPResponse
from sanic.log import logger
import jinja2
from pywps import configuration as wps_config
from sanic.exceptions import ServerError, NotFound
from .async_service import AsyncService
from .processes import Sleep, Drill, Subset2D, Subset3D, Stats2D, Stats3D, Subset1D
from src.swift_session import AsyncSwiftService, get_conn_session
from ...audit import audit_this
from ...auth import optional_auth

service_re = re.compile(r"^(https?)://(.+?)/v1/(.+)$")

def find_template_dir(here, search_for="templates") -> Path:
    h = Path(here)
    while len(h.name) and (h.parent is not h):
        subdirs = [x for x in h.iterdir() if x.is_dir()]
        found = list(filter(lambda p: p.name == search_for, subdirs))
        if found:
            h = h / search_for
            break
        h = h.parent
    else:
        raise ServerError("Cannot find template dir for wps templates.")
    return h

mywps = Blueprint("wps")


# Set up dataset layer names, dataset index locations, and other dataset attributes in here
# Add a new entry here when we have new datasets or dataset layers exposed
# This config dict gets passed into the init setup for each process, they each keep a copy of it.
datasets_config = {
    "dataset_layers": {
      "smips": ["totalbucket", "SMindex"],
      "aet": ["ETa"]
    },
    "index_locations": {
        #"smips": "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-smips-public/v1_0/index.shp",
        "smips": "/vsiswift/landscapes-csiro-smips-public/v1_0/index.shp",
        "aet": "/vsiswift/landscapes-csiro-aet-public/v2_2/index.shp",
    },
    "estimate_size_ratio": {
        "smips:totalbucket": 0.0405,
        "smips:SMindex": 0.0405,
        "aet:ETa": 25.6,
    },
    "units": {
        "aet:ETa": "mm/d",
    },
    "date_format_strings": {
        "aet": "%Y-%m-%d"
    }
}

fs_loader = jinja2.FileSystemLoader(searchpath=find_template_dir(Path(__file__).parent))
j2 = jinja2.Environment(loader=fs_loader, enable_async=True)


@mywps.before_server_start
def create_wps_service_for_app(app: Sanic, loop=None):
    # Create our own context within the application context
    app.ctx.mywps = ctx = getattr(app.ctx, 'mywps', SimpleNamespace())
    s = getattr(ctx, "wps_service", None)
    if s:
        raise RuntimeError("WPS service already registered on this app!")
    env_max_workers = os.getenv("WPS_MAX_PROCESS_WORKERS", None)
    if env_max_workers:
        max_workers = int(env_max_workers)
    else:
        # cpu count x 2, because http waiting time on most jobs
        # note, this might also double the Memory usage, watch out!
        max_workers = os.cpu_count() * 2
    ppe = ProcessPoolExecutor(max_workers=max_workers)
    shared_kwargs = {"ppe": ppe, "datasets_config": datasets_config}
    processes = [
        Sleep(),
        Drill(**shared_kwargs),
        Subset1D(**shared_kwargs),
        Subset2D(**shared_kwargs),
        Subset3D(**shared_kwargs),
        Stats2D(**shared_kwargs),
        Stats3D(**shared_kwargs),
    ]
    process_descriptor = {}
    for process in processes:
        abstract = process.abstract
        identifier = process.identifier
        process_descriptor[identifier] = abstract
    ctx.ppe = ppe
    ctx.datasets_config = datasets_config
    ctx.process_descriptor = process_descriptor
    service = AsyncService(app, mywps, processes, cfgfiles=["pywps.cfg"])
    ctx.wps_service = service

@mywps.before_server_start
def set_up_server_url(app: Sanic, loop=None):
    server_name = app.config.get("SERVER_NAME", None)
    # SERVER_NAME should have port built-in, don't re-add it if missing
    if not server_name:
        server_name = os.getenv("SERVER_LISTEN_HOST", "127.0.0.1")
    if ":" in server_name:
        server_name, port = server_name.split(":", 1)
        port = int(port)
    else:
        port = int(os.getenv("SERVER_INTERNAL_PORT", "8082"))
    if port != 80 and port != 443:
        server_name += f":{port}"
    scheme = "http:" if port in (80, 8080, 8081, 8082) else "https:"
    old_url = wps_config.get_config_value("server", "url")
    new_url = old_url.replace("http:", scheme).replace("localhost:8082", server_name)
    wps_config.CONFIG.set("server", "url", new_url)
    old_output_url = wps_config.get_config_value("server", "outputurl")
    new_output_url = old_output_url.replace("http:", scheme).replace("localhost:8082", server_name)
    wps_config.CONFIG.set("server", "outputurl", new_output_url)

@mywps.before_server_start
async def create_objectstore_connection(app: Sanic, loop=None):
    # Create our own context within the application context
    app.ctx.mywps = ctx = getattr(app.ctx, 'mywps', SimpleNamespace())
    service = AsyncSwiftService()
    s, con = get_conn_session(service._options)
    storage_url, token = con.get_service_auth()
    match = service_re.match(storage_url)
    if not match:
        raise ServerError("Cannot find object store URL from Keystone Services Catalogue.")
    scheme = match[1]
    url = match[2]
    authname = match[3]
    ctx.swift_service = service
    ctx.nectar_objectstore_url = f"{scheme}://{url}/v1/{authname}"
    ctx.nectar_objectstore_authname = authname
    ctx.nectar_objectstore_url = storage_url
    ctx.nectar_keystone_token = con.token
    do_refresh_keystone_token(s, ctx, "nectar_keystone_token", loop=loop, repeat=3600.0)


def do_refresh_keystone_token(session, ctx: SimpleNamespace, key: str, loop: AbstractEventLoop, repeat: float = 0.0):
    loop.create_task(_refresh_keystone_token(session, ctx, key, loop, repeat=repeat))


async def _refresh_keystone_token(session, ctx: SimpleNamespace, key: str, loop: AbstractEventLoop, repeat: float = 0.0):
    token = session.get_token()
    setattr(ctx, key, token)
    logger.info("WPS: Refreshed keystone auth token")
    if repeat > 0.0:
        # call again in an hour
        loop.call_later(repeat, do_refresh_keystone_token, session, ctx, key, loop, repeat)



@mywps.middleware
def make_request(request):
    request.ctx.wps_request = None

@mywps.route("/", methods=['GET', 'HEAD', 'OPTIONS'])
async def index(request: Request) -> HTTPResponse:
    if request.method.upper() in ("HEAD", "OPTIONS"):
        return HTTPResponse(body=None, content_type="text/html")
    server_url = wps_config.get_config_value("server", "url")
    request_url = request.url
    ctx = getattr(request.app.ctx, 'mywps', None)
    if ctx is None:
        process_descriptor = {}
    else:
        process_descriptor = getattr(ctx, 'process_descriptor', {})
    out = await j2.get_template("home.html").render_async(
        request_url=request_url,
        server_url=server_url,
        process_descriptor=process_descriptor
    )
    return HTTPResponse(body=out, content_type="text/html")


@mywps.route('/wps', methods=['GET', 'POST', 'HEAD', 'OPTIONS'])
async def wps(request: Request) -> HTTPResponse:
    if request.method.upper() in ("HEAD", "OPTIONS"):
        return HTTPResponse(body=None, content_type="text/xml")
    service = request.app.ctx.mywps.wps_service
    return await service(request)

@mywps.route('/processes', methods=['GET', 'POST', 'HEAD', 'OPTIONS'])
async def wps(request: Request) -> HTTPResponse:
    if request.method.upper() in ("HEAD", "OPTIONS"):
        return HTTPResponse(body=None, content_type="application/json")
    service = request.app.ctx.mywps.wps_service
    return await service(request)
@mywps.route('/jobs', methods=['GET', 'POST', 'HEAD', 'OPTIONS'])
async def wps(request: Request) -> HTTPResponse:
    if request.method.upper() in ("HEAD", "OPTIONS"):
        return HTTPResponse(body=None, content_type="application/json")
    service = request.app.ctx.mywps.wps_service
    return await service(request)
@mywps.route('/api', methods=['GET', 'POST', 'HEAD', 'OPTIONS'])
async def wps(request: Request) -> HTTPResponse:
    if request.method.upper() in ("HEAD", "OPTIONS"):
        return HTTPResponse(body=None, content_type="application/json")
    service = request.app.ctx.mywps.wps_service
    return await service(request)

STREAM_CHUNK_SIZE = 16384 #4k to 16k is a good size for http streaming

async def _respond_to_multirange_requests(request, range_requests, f, whole_file_size: int,
                                          headers=None, mime_type=None):
    if headers is None:
        headers = {}
    part_headers_total = 0
    ranges_total = 0
    my_delim = "3d6b6a416f9b5"
    part_range_configs = []

    for i, (part_begin, part_end) in enumerate(range_requests):
        if part_begin < 0:
            backoff_count = part_end
            part_end = whole_file_size - 1
            part_begin = max(0, part_end - (backoff_count - 1))
        if part_end < 0:
            # manipulate part_end so `-1` becomes the last readable byte index
            part_end = whole_file_size + part_end # this add is actually a subtr, because part_end is negative
        part_total: int = (part_end - part_begin) + 1  # Plus one because the range is _inclusive_
        if i == 0:
            part_header = f"--{my_delim}\r\nContent-Range: bytes {part_begin}-{part_end}/{whole_file_size}\r\n"
        else:
            part_header = f"\r\n--{my_delim}\r\nContent-Range: bytes {part_begin}-{part_end}/{whole_file_size}\r\n"
        if mime_type:
            part_header = part_header + f"Content-Type: {mime_type}\r\n"
        part_header = part_header + "\r\n"
        part_headers_total = part_headers_total + len(part_header)
        ranges_total = ranges_total + part_total
        part_range_configs.append((part_begin, part_total, part_header))
    end_delim = f"\r\n--{my_delim}--".encode('latin-1')
    part_headers_total = part_headers_total + len(end_delim)
    if ranges_total < 1 or ranges_total > whole_file_size:
        headers.update({"content-length": "0", "content-range": f"bytes 0-0/{whole_file_size}"})
        return HTTPResponse(b"", status=206, headers=headers, content_type=mime_type)
    all_tot = part_headers_total + ranges_total
    headers.update({"content-type": f"multipart/byteranges; boundary={my_delim}", "content-length": str(all_tot)})
    if all_tot > STREAM_CHUNK_SIZE:
        all_bytes_sent = 0
        resp = await request.respond(status=206, headers=headers)
        for (part_offset, part_size, part_header) in part_range_configs:
            part_bytes_sent = 0
            part_header_bytes = part_header.encode('latin-1')
            f.seek(part_offset)
            first_chunk = True
            max_first_chunk_size = STREAM_CHUNK_SIZE - len(part_header_bytes)
            chunk = f.read(min(max_first_chunk_size, part_size))
            while chunk is not None and len(chunk):
                chunk_len = len(chunk)
                if first_chunk is True:
                    await resp.send(part_header_bytes+chunk)
                    first_chunk = False
                    all_bytes_sent = all_bytes_sent + len(part_header_bytes) + chunk_len
                else:
                    await resp.send(chunk)
                    all_bytes_sent = all_bytes_sent + chunk_len
                all_bytes_remain = all_tot - all_bytes_sent
                part_bytes_sent = part_bytes_sent + chunk_len
                part_bytes_remain = part_size - part_bytes_sent
                if part_bytes_remain < 1:
                    break
                read_size = min(STREAM_CHUNK_SIZE, part_bytes_remain)
                chunk = f.read(read_size)
        await resp.send(end_delim)
        all_bytes_sent = all_bytes_sent + len(end_delim)
        await resp.eof()
        return None
    else:
        #Put all chunks in one response send
        output_buffer = b""
        for (part_offset, part_size, part_header) in part_range_configs:
            f.seek(part_offset)
            output_buffer += part_header.encode('latin-1')
            part_content: bytes = f.read(part_size)
            output_buffer += part_content
        output_buffer += end_delim
        return HTTPResponse(output_buffer, status=206, headers=headers)
async def _respond_to_range_request(request, range_request, f, whole_file_size: int, headers=None, mime_type=None):
    if headers is None:
        headers = {}
    (range_begin, range_end) = range_request
    if range_begin < 0:
        backoff_count = range_end
        range_end = whole_file_size - 1
        range_begin = max(0, range_end - (backoff_count-1))
    if range_end < 0:
        # manipulate range_end so `-1` becomes the last readable byte index
        range_end = whole_file_size + range_end  # this add is actually a subtr, because range_end is negative
    range_total: int = (range_end - range_begin) + 1  # Plus one because the range is _inclusive_
    if range_total < 1:
        headers.update({"content-length": "0", "content-range": f"bytes 0-0/{whole_file_size}"})
        return HTTPResponse(b"", status=206, headers=headers, content_type=mime_type)
    elif range_total >= whole_file_size:
        range_total = whole_file_size
    f.seek(range_begin)
    headers.update({"content-range": f"bytes {range_begin}-{range_end}/{whole_file_size}"})
    if range_total > STREAM_CHUNK_SIZE:
        sent_bytes = 0
        headers.update({"content-length": str(range_total)})
        resp = await request.respond(status=206, headers=headers, content_type=mime_type)
        chunk = f.read(STREAM_CHUNK_SIZE)
        while chunk is not None and len(chunk):
            await resp.send(chunk)
            sent_bytes += len(chunk)
            bytes_left = (range_total - sent_bytes)
            if bytes_left < 1:
                break
            read_size = min(STREAM_CHUNK_SIZE, bytes_left)
            chunk = f.read(read_size)
        await resp.eof()
        return None
    else:
        # range-begin offset is set by the f.seek() call above
        file_content: bytes = f.read(range_total)
        return HTTPResponse(file_content, status=206, headers=headers, content_type=mime_type)

@mywps.route('/outputs/<filename:path>', methods=['GET', 'HEAD', 'OPTIONS'])
@optional_auth
@audit_this
async def outputfile(request: Request, filename: str) -> Union[HTTPResponse, None]:
    targetfile = os.path.join('outputs', filename)
    file_ext = os.path.splitext(targetfile)[1]
    if 'xml' in file_ext:
        mime_type = 'text/xml'
    elif 'tif' in file_ext:
        mime_type = 'image/tiff'
    elif 'csv' in file_ext:
        mime_type = 'text/csv'
    else:
        mime_type = None
    headers = {
        "accept-ranges": "bytes",
    }
    if not os.path.isfile(targetfile):
        return HTTPResponse(body=None, status=404, headers=headers, content_type=mime_type)

    with open(targetfile, mode='rb') as f:
        fd = f.fileno()
        s = os.fstat(fd)
        # time.time ~should~ always be in GMT/UTC
        whole_file_size = s.st_size
        if request.method.upper() in ("HEAD", "OPTIONS"):
            headers.update({"content-length": str(whole_file_size)})
            return HTTPResponse(None, status=200, headers=headers, content_type=mime_type)
        now_s: float = time.time()
        now_ns = int(now_s * 1_000_000_000.0)
        try:
            # explicitly update access time, we can't be sure that open() and read() does that.
            os.utime(fd, ns=(now_ns, s.st_mtime_ns))
        except Exception as e:
            # cannot change the access_time
            pass
        range_headers = request.headers.getall("range", [])
        range_requests = []
        for rh in range_headers:
            if "bytes=" not in rh:
                return HTTPResponse(None, status=416, headers=headers)
            bytes_string = (rh.split("bytes=", 1)[-1]).strip()
            parts = [str(s).strip() for s in bytes_string.split(',')]
            for part in parts:
                if '-' not in part:
                    return HTTPResponse(None, status=416, headers=headers)
                if part.startswith('-'):
                    # Range with no start, only end
                    begin_end = ["-1", part[1:].strip()]
                elif part.endswith('-'):
                    begin_end = [part[:-1].strip(), "-1"]
                else:
                    begin_end = [p.strip() for p in part.split('-', 1)]
                try:
                    begin_b = int(begin_end[0])
                    end_b = int(begin_end[1])
                except ValueError:
                    return HTTPResponse("Non integer range", status=416, headers=headers)
                if (begin_b > end_b > 0) and (end_b < begin_b > 0):
                    return HTTPResponse("Begin greater than end.", status=416, headers=headers)
                range_requests.append((begin_b, end_b))
        try:
            f.seek(0)
            seekable = True
        except Exception:
            # If we can't seek, then we can't fulfill range requests
            seekable = False
        if range_requests and seekable:
            if len(range_requests) > 1:
                return await _respond_to_multirange_requests(request, range_requests, f, whole_file_size, headers,
                                                             mime_type)
            return await _respond_to_range_request(request, range_requests[0], f, whole_file_size, headers, mime_type)

        else:
            # No ranges, send or stream the file conventionally
            if whole_file_size > STREAM_CHUNK_SIZE:
                headers.update({"content-length": str(whole_file_size)})
                resp = await request.respond(status=200, headers=headers, content_type=mime_type)
                chunk = f.read(STREAM_CHUNK_SIZE)
                while chunk is not None and len(chunk):
                    await resp.send(chunk)
                    chunk = f.read(STREAM_CHUNK_SIZE)
                await resp.eof()
                return None
            else:
                file_content: bytes = f.read()
                return HTTPResponse(file_content, content_type=mime_type)

__all__ = ("mywps",)
