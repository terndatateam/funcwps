import asyncio
import datetime
import gc
import os
import tempfile
from _weakref import ProxyType
from copy import deepcopy, copy
from math import ceil
from typing import Union
from weakref import proxy
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from urllib.parse import urljoin
import time
from ciso8601 import parse_datetime
from osgeo.ogr import DataSource
from pywps import LiteralInput, LiteralOutput, ComplexInput, FORMATS, Format, ComplexOutput, WPSRequest
from pywps.app.Common import Metadata
from pywps.exceptions import MissingParameterValue, InvalidParameterValue
from pywps.inout.literaltypes import AnyValue
from pywps.response import get_response
from pywps.response.execute import ExecuteResponse
from pywps.response.status import WPS_STATUS
from pandas._libs.json import ujson_loads, ujson_dumps
import dateutil
from osgeo import ogr, gdal
from shapely.geometry import shape
from distributed import Client, Security
from dask.distributed import Client
from dask import delayed
from src.blueprints.wps.processes.util import TRUTHS, get_identifier_for_feature, \
    roughly_split_shape_by_area, set_output_filename_for_complexoutput, complete_tasks
from src.blueprints.wps.processes.shared_functions import stats2D
from src.utils import TempEnv, is_debugging

try:
    from ..async_process import AsyncProcess
except ImportError:
    from src.blueprints.wps.async_process import AsyncProcess


GEOJSON = Format('application/vnd.geo+json', schema="http://geojson.org/geojson-spec.html#", extension='.geojson')
GEOJSON_POINT = Format('application/vnd.geo+json', schema="http://geojson.org/geojson-spec.html#point", extension='.geojson')
GEOJSON_POLYGON = Format('application/vnd.geo+json', schema="http://geojson.org/geojson-spec.html#polygon", extension='.geojson')
GEOJSON_ANY = Format(
    "application/vnd.geo+json", schema="http://geojson.org/geojson-spec.html#geojson", extension=".geojson"
)
TJS_JSON = Format("application/vnd.terriajs.catalog-member+json", schema="https://tools.ietf.org/html/rfc7159", extension=".json")
OUT_CSV = Format("text/csv", extension=".csv")
MODE = os.getenv("WPS_PROCESSING_MODE", "multiprocessing")
MAX_JOB_OUTPUT_MB = float(os.getenv("WPS_MAX_JOB_OUTPUT_MB", "350.0"))
MAX_JOB_MEMORY_MB = float(os.getenv("WPS_MAX_JOB_MEMORY_MB", "700.0"))

MAX_OPTIM_GROUP = 8  # This is a tunable, how many lookups can we do on the same file in the same task?

if MODE == "dask":
    if __name__ == "__main__":
        sec = Security(require_encryption=False)
        dask_client = Client("tcp://140.253.176.93:8999", security=sec, asynchronous=True)
else:
    dask_client = None

ogr.UseExceptions()


class Stats3D(AsyncProcess):
    SUCCESS_MESSAGE = 'Finished timeseries stats'
    allowed_returns = ["auto", "terria", "csv", "link"]

    def __init__(self, ppe: Union[ProcessPoolExecutor, ThreadPoolExecutor, None] = None, datasets_config: dict = {}):
        allowed_datasets = []
        for ds, ds_layers in datasets_config["dataset_layers"].items():
            for l_name in ds_layers:
                allowed_datasets.append("{}:{}".format(str(ds), str(l_name)))
        inputs = [
            LiteralInput('datasetId',
                         'dataset identifier',
                         min_occurs=1,
                         allowed_values=allowed_datasets,
                         default="smips:totalbucket"),
            LiteralInput('startDate',
                         'Date of start of timeseries',
                         data_type='date',
                         min_occurs=1,
                         allowed_values=AnyValue,
                         default="1970-01-01"),
            LiteralInput('endDate',
                         'Date of end of timeseries',
                         data_type='date',
                         min_occurs=1,
                         allowed_values=AnyValue,
                         default="19-01-2038"),
            LiteralInput('excelDates',
                         'CSV Date Column will be Excel-compatible',
                         data_type='boolean',
                         min_occurs=0,
                         allowed_values=AnyValue,
                         default=False),
            LiteralInput('wideTable',
                         'Extra polygons are appended as extra columns',
                         data_type='boolean',
                         min_occurs=0,
                         allowed_values=AnyValue,
                         default=False),
            LiteralInput('returnType',
                         'Optional, specify function return type',
                         min_occurs=0,
                         allowed_values=self.allowed_returns,
                         default="auto"),
            ComplexInput("polygon", "Polygon", [GEOJSON_ANY])
        ]
        outputs = [
            LiteralOutput("download_link", "CSV Download Link", data_type="string"),
            ComplexOutput("csv", "CSV Results", [TJS_JSON, OUT_CSV])
        ]

        super(Stats3D, self).__init__(
            self._handler,
            identifier='stats3d',
            version='None',
            title='Stats 3D',
            abstract="Return CSV file containing timeseries stats within given 2D polygons.",
            profile='',
            metadata=[Metadata('slice'), Metadata('cut'), Metadata('stats'), Metadata('timeseries')],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True
        )
        if ppe is None:
            #self.ppe = ProcessPoolExecutor(32)
            self.ppe = ThreadPoolExecutor(max_workers=os.cpu_count())
        else:
            if not isinstance(ppe, ProxyType):
                self.ppe = proxy(ppe)
            else:
                self.ppe = ppe
        self.datasets_config = datasets_config
        self.debugging = is_debugging()

    def __deepcopy__(self, memodict={}):
        ppe_ref = self.ppe
        self.ppe = None
        self.handler = None
        ourcopy = deepcopy(super(Stats3D, self), memodict)
        self.ppe = ppe_ref
        self.handler = self._handler
        ourcopy.ppe = ppe_ref
        ourcopy.handler = ourcopy._handler
        return ourcopy

    async def _handler(self, request: WPSRequest, response: ExecuteResponse):
        try:
            app = self.service.app
            ctx = app.ctx.mywps
            has_ctx = True
        except (AttributeError, LookupError):
            #raise RuntimeError("Cannot get reference to App in Async Process.")
            has_ctx = False
        started = time.perf_counter()
        response._update_status(WPS_STATUS.STARTED, 'Landscapes WPS Process started', 0)
        dataset_id_lit: LiteralInput = request.inputs['datasetId'][0]
        dataset_id: str = dataset_id_lit.data
        if ":" in dataset_id:
            dataset_id, layer_id = dataset_id.split(":", 1)
        else:
            layer_id = None

        startdate_lit: LiteralInput = request.inputs['startDate'][0]
        startdate: datetime.datetime = startdate_lit.data

        enddate_lit: LiteralInput = request.inputs['endDate'][0]
        enddate: datetime.datetime = enddate_lit.data


        try:
            ret_type_lit: LiteralInput = request.inputs['returnType'][0]
            ret_type = ret_type_lit.data
        except LookupError:
            ret_type = 'auto'

        try:
            excel_dates_lit: LiteralInput = request.inputs['excelDates'][0]
            excel_dates_b = excel_dates_lit.data
        except LookupError:
            excel_dates_b = False

        try:
            wide_table_lit: LiteralInput = request.inputs['wideTable'][0]
            wide_table_b = wide_table_lit.data
        except LookupError:
            wide_table_b = False

        poly: ComplexInput = request.inputs['polygon'][0]
        try:
            poly_data = ujson_loads(poly.data)
        except (ValueError, TypeError):
            raise
        polys = []
        try:
            crs = None
            features = []
            p_type = poly_data['type'].lower()
            if p_type == "featurecollection":
                # All features from featurecollection
                crs = poly_data.get('crs', crs)
                _features = poly_data.get('features', [])
                for (_ix, f) in enumerate(_features):
                    if 'geometry' in f and f['geometry'] is not None:
                        _id = get_identifier_for_feature(f, _ix)
                        features.append((_id, f['geometry']))
            elif p_type == "geometrycollection":
                # Get all geometries from geometrtycollection
                crs = poly_data.get('crs', crs)
                _geoms = poly_data.get('geometries', [])
                for (_ix, g) in enumerate(_geoms):
                    _id = get_identifier_for_feature(g, _ix)
                    features.append((_id, g))
            elif p_type == "feature":
                crs = poly_data.get('crs', crs)
                if 'geometry' in poly_data and poly_data['geometry'] is not None:
                    _id = get_identifier_for_feature(poly_data, 0)
                    features.append((_id, poly_data['geometry']))
            elif p_type == "polygon":
                _id = get_identifier_for_feature(poly_data, 0)
                polys.append((_id, poly_data))
            elif p_type == "multipolygon":
                _id = get_identifier_for_feature(poly_data, 0)
                polys.append((_id, poly_data))
            elif p_type == "point":
                # Weird, but we can do it. Need to get rid of Z
                poly_data['coordinates'] = poly_data['coordinates'][:2]
                _id = get_identifier_for_feature(poly_data, 0)
                polys.append((_id, poly_data))
            for (_id, f) in features:
                if f['type'].lower() == "polygon":
                    polys.append((_id, f))
                elif f['type'].lower() == "multipolygon":
                    polys.append((_id, f))
                elif f["type"].lower() == "point":
                    # Need to get rid of Z
                    f['coordinates'] = f['coordinates'][:2]
                    polys.append((_id, f))
            if crs is None:
                # All Geojson is assumed EPSG:4326 unless it has a CRS
                # Note, CRS is no longer part of the GeoJSON standard
                crs = poly_data.get("crs", "epsg:4326")
        except LookupError:
            raise
        total_sq_degrees = 0
        try:
            estimate_size_ratio = self.datasets_config["estimate_size_ratio"][f"{dataset_id}:{layer_id}"]
        except LookupError:
            estimate_size_ratio = 20
        poly_shapes = []
        for (_id, p) in polys:
            in_shape = shape(p)
            # Get area in square degrees
            total_sq_degrees += in_shape.area
            poly_shapes.append((_id, in_shape))
        estimate_size = total_sq_degrees * estimate_size_ratio

        # This flag is set if data for the whole poly area can't fit into the allowed memory.
        # When it is set, we operate on each feature without multiprocessing
        operate_individually = False

        if estimate_size >= MAX_JOB_MEMORY_MB:
            #response.status = WPS_STATUS.FAILED
            #response.update_status("Geometry area results in dataset subset that cannot fit in the server's memory.", status_percentage=100)
            #return response
            operate_individually = True

        if isinstance(startdate, datetime.date):
            startdate = datetime.datetime(
                startdate.year, startdate.month, startdate.day,
                tzinfo=datetime.timezone.utc
            )

        if isinstance(enddate, datetime.date):
            enddate = datetime.datetime(
                enddate.year, enddate.month, enddate.day,
                hour=23, minute=59, second=59, microsecond=999999, tzinfo=datetime.timezone.utc
            )

        if startdate.tzinfo is None:
            startdate = startdate.replace(tzinfo=datetime.timezone.utc)
        if enddate.tzinfo is None:
            enddate = enddate.replace(tzinfo=datetime.timezone.utc)
        output_as_ref = False
        if "download_link" in request.outputs:
            output_type = "link_literal"
            output_as_ref = request.outputs["download_link"].get("asReference", "") in TRUTHS
            if output_as_ref:
                response.status = WPS_STATUS.FAILED
                response.update_status("Literal response cannot be asRef.", status_percentage=100)
                return response
            mimetype = request.outputs["download_link"].get("mimetype", "text/csv")
        elif "csv" in request.outputs:
            output_type = "csv_file"
            output_as_ref = request.outputs["csv"].get("asReference", "") in TRUTHS
            mimetype = request.outputs["csv"].get("mimetype", "text/csv")
            if mimetype == "text/csv":
                output_type = "csv_file"
            else:
                # Assume mimetype is "application/vnd.terriajs.catalog-member+json"
                # but of course TerriaJS doesn't set this option on the request
                output_type = "tjs"
        else:
            if ret_type in self.allowed_returns:
                if ret_type == "csv":
                    output_type = "csv_file"
                    mimetype = "text/csv"
                elif ret_type == "link":
                    output_type = "link_literal"
                    mimetype = "text/csv"
                elif ret_type == "terria":
                    output_type = "tjs"
                    mimetype = "application/vnd.terriajs.catalog-member+json"
                else:
                    # TODO: Determine how to detect a TerriaJS request automatically
                    output_type = "link_literal"
                    mimetype = "text/csv"
            else:
                response.status = WPS_STATUS.FAILED
                response.update_status("Cannot determine output type", status_percentage=100)
                return response


        index_loc = self.datasets_config["index_locations"].get(dataset_id, None)
        if not index_loc:
            response.update_status("", status_percentage=100)
            return response
        if index_loc.startswith("/vsiswift"):
            if has_ctx:
                env_vars = {
                    "SWIFT_STORAGE_URL": ctx.nectar_objectstore_url,
                    "SWIFT_AUTH_TOKEN": ctx.nectar_keystone_token
                }
            else:
                env_vars = {
                    "SWIFT_STORAGE_URL": os.getenv("SWIFT_STORAGE_URL", ""),
                    "SWIFT_AUTH_TOKEN": os.getenv("SWIFT_AUTH_TOKEN", "")
                }
        else:
            env_vars = {}

        driver_name = "ESRI Shapefile"
        with TempEnv(**env_vars):
            # All of this needs to be atomic, to preserve the env variables.
            # no thread swapping or async operations
            drv = ogr.GetDriverByName(driver_name)
            if drv is None:
                raise RuntimeError("{} driver not available.\n".format(driver_name))
            if index_loc.startswith("https") or index_loc.startswith("http"):
                ds: DataSource = drv.Open(f"/vsicurl/{index_loc}")
            else:
                ds = drv.Open(index_loc)
            if not ds:
                del drv
                raise LookupError(f"Cannot open index file for dataset {dataset_id}")
            layers = ds.GetLayerCount()
            if layers < 1 or layers > 1:
                if not self.debugging:
                    del ds
                del drv
                raise LookupError(f"Wrong number of layers in index for {dataset_id}")
            layer = next(iter(ds))
            to_get = []
            gc.disable()
            for feature in layer:  # type: osgeo.ogr.Feature
                if layer_id is not None:
                    lay = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("layer")))
                    if lay.lower() != layer_id.lower():
                        del feature
                        continue
                datestr = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("date")))
                dateat = parse_datetime(datestr)
                if not(startdate <= dateat <= enddate):
                    del feature
                    continue
                loc = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("location")))
                to_get.append((dateat, loc))
                del feature
                #gc.collect()  # GC Collect here helps with memory management, but significantly slows down the loop
            len_gets = len(to_get)
            if not self.debugging:
                del layer
                del ds
            del drv
            gc.collect()
            gc.enable()

        if len_gets < 1:
            response.status = WPS_STATUS.FAILED
            response.update_status("No dataset slices found in that time period.", status_percentage=100)
            return response

        # After a potentially long index lookup, hand back to sanic to handle other requests waiting
        await asyncio.sleep(0)

        ds_date_format_string = self.datasets_config.get("date_format_strings", {}).get(dataset_id, None)
        if not ds_date_format_string:
            ds_date_format_string = "%Y-%m-%d"

        if MODE == "multiprocessing":
            estimate_size = estimate_size * min(len_gets, self.ppe._max_workers)
            if estimate_size >= MAX_JOB_MEMORY_MB:
                operate_individually = True
                # response.status = WPS_STATUS.FAILED
                # response.update_status("Geometry area results in dataset subset that cannot fit in the server's memory.", status_percentage=100)
                # return response
        response.update_status(f"Found {len_gets} time slices to process,", status_percentage=0)
        tasks_meta = {}
        tasks = []
        unscheduled_ppe = []
        worker_pool = None
        if MODE == "dask" and not operate_individually:
            delays = []
            delays_dates = {}
            delays_polyids = {}
            delays_taskids = {}
            #c = Client(address=cluster, asynchronous=True)
            c = dask_client
            task_id = -1
            for (d_date, d_loc) in to_get:
                slice_loc = urljoin(index_loc, d_loc)
                optim_group = []
                # TODO: send optim_group for Dask
                # But that is harder because of the meta used.
                for poly_id, poly_shape in poly_shapes:
                    task_id += 1
                    # TODO: work out how to turn layer_id into band number (1)
                    band_number = 1
                    task_tuple = (task_id, slice_loc, band_number, poly_shape)
                    d = delayed(stats2D)([task_tuple], "auto", env_vars)
                    delays.append(d)
                    delays_dates[d.key] = d_date
                    delays_polyids[d.key] = poly_id
                    delays_taskids[d.key] = task_id
            futures = c.compute(delays, traverse=False)
            for f in futures:
                d = delays_dates[f.key]
                poly_id = delays_polyids[f.key]
                task_id = delays_taskids[f.key]
                g = c.gather((f,))
                t = asyncio.create_task(g)
                tasks.append(t)
                tasks_meta[t] = {task_id: {"date": d, "poly": poly_id, "task_id": task_id}}
        elif MODE == "multiprocessing" and not operate_individually:
            # Don't put too many jobs on the queue at once, so others can share process pool too
            worker_pool = self.ppe
            task_id = -1
            for (d_date, d_loc) in to_get:
                slice_loc = urljoin(index_loc, d_loc)
                optim_group = []
                task_metas = {}
                for poly_id, poly_shape in poly_shapes:
                    task_id += 1
                    # TODO: work out how to turn layer_id into band number (1)
                    band_number = 1
                    task_tuple = (task_id, slice_loc, band_number, poly_shape)
                    task_meta = {"task_id": task_id, "date": d_date, "poly": poly_id}
                    task_metas[task_id] = task_meta
                    optim_group.append(task_tuple)
                    if len(optim_group) < MAX_OPTIM_GROUP:
                        continue
                    else:
                        unscheduled_ppe.append((stats2D, (optim_group, "auto", env_vars), task_metas))
                        task_metas = {}
                        optim_group = []
                if len(optim_group) > 0:
                    unscheduled_ppe.append((stats2D, (optim_group, "auto", env_vars), task_metas))
                del poly_shape
        else:
            single_executor = ThreadPoolExecutor(1)
            worker_pool = single_executor
            async def dummy_async(*args, **kwargs):
                _loop = asyncio.get_event_loop()
                res = await _loop.run_in_executor(worker_pool, stats2D, *args, **kwargs)
                return res

            if len(poly_shapes) > 1:
                response.update_status(
                    "Geometry area is too large to fit in system memory. Cannot run the job in parallel, falling back to serialized execution. This operation will take longer than usual to complete.",
                    status_percentage=0)

            poly_splits_required = {}
            for poly_id, poly_shape in poly_shapes:
                estimate_mem = (poly_shape.area * estimate_size_ratio)
                if estimate_mem >= MAX_JOB_MEMORY_MB:
                    response.update_status(
                        "Geometry area is too large to fit in system memory. Chunking geometry to save memory. This operation will take much longer than usual to complete, and may fail.",
                        status_percentage=0)
                    splits_required = ceil(estimate_mem / MAX_JOB_MEMORY_MB)
                    # splits_required = 4
                    if splits_required > 8:
                        response.status = WPS_STATUS.FAILED
                        response.update_status(
                            "Geometry area results in dataset subset that cannot fit in the server's memory.",
                            status_percentage=100)
                        return response
                else:
                    splits_required = 0
                poly_splits_required[poly_id] = splits_required
            task_id = -1
            for (d_date, d_loc) in to_get:
                slice_loc = urljoin(index_loc, d_loc)
                optim_group = []
                task_metas = {}
                for poly_id, poly_shape in poly_shapes:
                    band_number = 1
                    splits_required = poly_splits_required[poly_id]
                    if splits_required > 0:
                        try:
                            splits = roughly_split_shape_by_area(poly_shape, splits_required)
                            for split_id, (split_poly, split_weight) in enumerate(splits):
                                task_id += 1
                                task_tuple = (task_id, slice_loc, band_number, split_poly)
                                task_meta = {"task_id": task_id, "date": d_date, "poly": poly_id, "split": True,
                                            "split_id": split_id, "split_weight": split_weight}
                                unscheduled_ppe.append((dummy_async, ([task_tuple], "auto", env_vars), {task_id: task_meta}))
                            del split_poly
                        except Exception as e:
                            print(e)
                    else:
                        task_id += 1
                        task_tuple = (task_id, slice_loc, band_number, poly_shape)
                        task_meta = {"task_id": task_id, "date": d_date, "poly": poly_id, "split": False}
                        task_metas[task_id] = task_meta
                        optim_group.append(task_tuple)
                        if len(optim_group) < MAX_OPTIM_GROUP:
                            continue
                        else:
                            unscheduled_ppe.append((dummy_async, (optim_group, "auto", env_vars), task_metas))
                            task_metas = {}
                            optim_group = []
                if len(optim_group) > 0:
                    unscheduled_ppe.append((dummy_async, (optim_group, "auto", env_vars), task_metas))
                del poly_shape
        del poly_shapes
        gc.collect()
        all_done = await complete_tasks(tasks, tasks_meta, response, worker_pool, unscheduled_ppe)
        # unpack the ones that have optim_groups
        computed_results = []
        for (task_metas, task_res_list) in all_done:
            for task_res in task_res_list:
                task_id = task_res[0]
                task_meta = task_metas[task_id]
                computed_results.append((task_meta, task_res[1:]))

        # This is the default ordering for the non-wide table format
        # sort first by polyid, then by date, from the task meta
        computed_results = sorted(computed_results, key=lambda x: (x[0]['poly'],x[0]['date']))
        finished = time.perf_counter()
        elapsed = finished - started
        print("Elapsed: {}".format(elapsed))
        # Make the correct header for the type of CSV we are generating:
        if output_type == "tjs":
            # Headers designed for TerriaJS rendering
            csvString = f"time,FID,min,max,mean,valid,stddev\n"
        else:
            if excel_dates_b:
                date_col_name = "Date_YYYY_MM_DD"
            else:
                date_col_name = "date"
            if wide_table_b:
                csvString = f"{date_col_name}"
                poly_ids_in_header = []
                for task_meta, _ in computed_results:
                    poly_id = task_meta['poly']
                    if poly_id in poly_ids_in_header:
                        continue
                    csvString += f",min_{poly_id},max_{poly_id},mean_{poly_id},valid_{poly_id},stddev_{poly_id}"
                    poly_ids_in_header.append(poly_id)
                csvString += "\n"  # EOL on header line
            else:
                csvString = f"{date_col_name},feature,min,max,mean,valid,stddev\n"

        wide_table_frame = {}

        reassemble_split_stats = False
        for (task_meta, _) in computed_results:
            if task_meta.get("split", False):
                reassemble_split_stats = True


        if reassemble_split_stats:
            poly_parts = {}
            for (task_meta, val) in computed_results:
                dt = task_meta["date"]
                poly_id = task_meta["poly"]
                split_id = task_meta.get("split_id", 1)
                split_weight = task_meta.get("split_weight", 1.0)
                if (poly_id, dt) not in poly_parts:
                    poly_parts[(poly_id, dt)] = {}
                poly_parts[(poly_id, dt)][split_id] = (split_weight, val)
            for (poly_id, _dt), _parts in poly_parts.items():
                if len(_parts) < 1:
                    continue
                part_count = 0
                (_, first_vals) = _parts[next(iter(_parts.keys()))]
                _min = first_vals[0]
                _max = first_vals[1]
                _mean_accum = 0
                _valid_percent_accum = 0
                _std_deviation_accum = 0
                for (split_id, (split_weight, vals)) in _parts.items():
                    part_count += 1
                    if vals[0] < _min:
                        _min = vals[0]
                    if vals[1] > _max:
                        _min = vals[1]
                    _mean_accum += vals[2] * split_weight
                    _valid_percent_accum += vals[3] * split_weight
                    _std_deviation_accum += vals[4] * split_weight
                _std_dev = _std_deviation_accum / part_count
                _vld_percent = _valid_percent_accum / part_count
                _mean = _mean_accum / part_count
                if wide_table_b:
                    try:
                        this_row = wide_table_frame[dt]
                    except LookupError:
                        this_row = wide_table_frame[dt] = {}
                    this_row[poly_id] = {
                        "min": _min,
                        "max": _max,
                        "mean": _mean,
                        "valid": _vld_percent,
                        "stddev": _std_dev
                    }
                else:
                    date_string = _dt.isoformat() if not excel_dates_b else _dt.strftime(ds_date_format_string)
                    csvString += "".join(
                        (date_string, ',', str(poly_id), ",", str(_min), ',', str(_max), ',', str(_mean), ',',
                         str(_vld_percent), ',', str(_std_dev), '\n'))
        else:
            for (task_meta, val) in computed_results:
                dt = task_meta["date"]
                poly_id = task_meta["poly"]
                if wide_table_b:
                    try:
                        this_row = wide_table_frame[dt]
                    except LookupError:
                        this_row = wide_table_frame[dt] = {}
                    this_row[poly_id] = {
                        "min": val[0],
                        "max": val[1],
                        "mean": val[2],
                        "valid": val[3],
                        "stddev": val[4]
                    }
                else:
                    date_string = dt.isoformat() if not excel_dates_b else dt.strftime(ds_date_format_string)
                    csvString += "".join(
                        (date_string, ',', str(poly_id), ",", str(val[0]), ',', str(val[1]), ',', str(val[2]), ',',
                         str(val[3]), ',', str(val[4]), '\n'))

        if wide_table_b and len(wide_table_frame.keys()) > 0:
            sorted_poly_ids = sorted(wide_table_frame[next(iter(wide_table_frame.keys()))].keys())
            for _dt, _colsets in wide_table_frame.items():
                date_string = _dt.isoformat() if not excel_dates_b else _dt.strftime(ds_date_format_string)
                row_string = f"{date_string}"
                for _s_poly_id in sorted_poly_ids:
                    col_parts = _colsets[_s_poly_id]
                    row_string += "".join((",", str(col_parts['min']), ',', str(col_parts['max']),
                                          ',', str(col_parts['mean']), ',', str(col_parts['valid']), ',', str(col_parts['stddev'])))
                row_string += "\n"
                csvString += row_string

        if output_type == "link_literal":
            new_filename = "{}.csv".format(str(time.time()))
            filepath = "outputs/{}".format(new_filename)
            with open(filepath, "w") as f:
                f.write(csvString)
            output_url = str(self.status_url).rsplit("/", 1)[0]
            link = "/".join((output_url, new_filename))
            response.outputs['download_link'].data = link
            del response.outputs['csv']
        elif output_type == "csv_file":
            response.outputs['csv'].data = csvString
            if output_as_ref and request.store_execute:
                response.outputs['csv'].as_reference = True
                set_output_filename_for_complexoutput(response.outputs['csv'], "stats3d.csv")
            response.outputs['csv'].mimetype = "text/csv"
            response.outputs['csv'].schema = ""
            response.outputs['csv'].data_format = OUT_CSV
            del response.outputs['download_link']
        elif output_type == "tjs":
            resp_obj = ujson_dumps({
                "csvString": csvString,
                "type": "csv",
                "name": layer_id,
                "columns": [
                    {
                        "name": "time",
                        "type": "time",
                    },
                    {
                        "name": "FID",
                        "type": "region",
                    },
                    {
                        "name": "mean",
                        "type": "scalar",
                    },
                    {
                        "name": "min",
                        "type": "scalar",
                    },
                    {
                        "name": "max",
                        "type": "scalar",
                    },
                    {
                        "name": "valid",
                        "type": "scalar",
                    },
                    {
                        "name": "stddev",
                        "type": "scalar",
                    }
                ],
                "styles": [{
                    "id": "style1",
                    "chart": {
                        "xAxisColumn": "time",
                        "lines": [{
                                "name": "mean",
                                "yAxisColumn": "mean",
                                "isSelectedInWorkbench": True
                            },
                            {
                                "yAxisColumn": "min",
                                "name": "min",
                                "isSelectedInWorkbench": True
                            },
                            {
                                "yAxisColumn": "max",
                                "name": "max",
                                "isSelectedInWorkbench": True
                            },
                        ]
                    },
                    "time": {
                        "timeColumn": "time",
                        "idColumns": ["FID"]
                    }
                }],
                "activeStyle": "style1"
            }, ensure_ascii=False)
            response.outputs['csv'].data = resp_obj
            response.outputs['csv'].mimetype = "application/vnd.terriajs.catalog-member+json"
            response.outputs['csv'].schema = "https://tools.ietf.org/html/rfc7159"
            response.outputs['csv'].data_format = TJS_JSON
            del response.outputs['download_link']
        else:
            resp_obj = {
                "type": "FeatureConnection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Point",
                            "coordinates": [1, 2]
                        },
                        "properties": {
                            "totalbucket": 10.0,
                            "datetime": startdate
                        }
                    }
                ]
            }
            response.outputs['csv'].data = ujson_dumps(resp_obj, ensure_ascii=False)
            del response.outputs['download_link']
        response._update_status(WPS_STATUS.SUCCEEDED, 'Landscapes WPS Process succeeded', 100)


example_feature = '''\
{
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "coordinates": [
          [
            [
              149.04336954668958,
              -23.759386642991814
            ],
            [
              148.61011814596174,
              -23.92407440331671
            ],
            [
              148.45257218205995,
              -24.222035630486957
            ],
            [
              148.95334328160112,
              -24.493702583335832
            ],
            [
              149.57790049563727,
              -24.180978733433122
            ],
            [
              149.04336954668958,
              -23.759386642991814
            ]
          ]
        ],
        "type": "Polygon"
      }
    },
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "coordinates": [
          [
            [
              152.53665538885065,
              -25.127787989517927
            ],
            [
              152.53665538885065,
              -24.724689616517864
            ],
            [
              152.1146572712592,
              -24.724689616517864
            ],
            [
              152.1146572712592,
              -25.127787989517927
            ],
            [
              152.53665538885065,
              -25.127787989517927
            ]
          ]
        ],
        "type": "Polygon"
      }
    },
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              152.834867476118,
              -26.847139409141228
            ],
            [
              152.79135479170378,
              -26.849039908467095
            ],
            [
              152.7482567841598,
              -26.854723294008753
            ],
            [
              152.70598430110218,
              -26.86413539579049
            ],
            [
              152.6649405625224,
              -26.877186486936214
            ],
            [
              152.6255174240897,
              -26.89375211355012
            ],
            [
              152.5880917327239,
              -26.913674247078035
            ],
            [
              152.55302180473558,
              -26.936762749719215
            ],
            [
              152.52064405640945,
              -26.96279714082435
            ],
            [
              152.4912698163436,
              -26.99152864962923
            ],
            [
              152.46518234813524,
              -27.02268253714855
            ],
            [
              152.4426341110936,
              -27.05596066760467
            ],
            [
              152.42384428553808,
              -27.091044307410176
            ],
            [
              152.40899658787538,
              -27.127597127477696
            ],
            [
              152.3982373990175,
              -27.1652683825183
            ],
            [
              152.39167422777044,
              -27.203696239033093
            ],
            [
              152.38937452857868,
              -27.24251122192726
            ],
            [
              152.3913648904204,
              -27.28133974810862
            ],
            [
              152.39763061071582,
              -27.319807714102932
            ],
            [
              152.40811566482358,
              -27.35754410365523
            ],
            [
              152.4227230780671,
              -27.394184580520076
            ],
            [
              152.44131570327875,
              -27.42937503120356
            ],
            [
              152.46371740260142,
              -27.46277502233278
            ],
            [
              152.48971462779318,
              -27.49406113761923
            ],
            [
              152.51905838860358,
              -27.522930160071034
            ],
            [
              152.5514665939986,
              -27.549102066209453
            ],
            [
              152.58662674619467,
              -27.57232280056582
            ],
            [
              152.62419896271174,
              -27.5923668006755
            ],
            [
              152.6638192970751,
              -27.60903924513713
            ],
            [
              152.70510332448723,
              -27.62217800004992
            ],
            [
              152.74764995486274,
              -27.63165524225202
            ],
            [
              152.79104543216718,
              -27.63737874122124
            ],
            [
              152.834867476118,
              -27.639292785220672
            ],
            [
              152.87868952006886,
              -27.63737874122124
            ],
            [
              152.9220849973733,
              -27.63165524225202
            ],
            [
              152.9646316277488,
              -27.62217800004992
            ],
            [
              153.00591565516092,
              -27.60903924513713
            ],
            [
              153.04553598952427,
              -27.5923668006755
            ],
            [
              153.08310820604135,
              -27.57232280056582
            ],
            [
              153.1182683582374,
              -27.549102066209453
            ],
            [
              153.15067656363243,
              -27.522930160071034
            ],
            [
              153.18002032444286,
              -27.49406113761923
            ],
            [
              153.20601754963462,
              -27.46277502233278
            ],
            [
              153.2284192489573,
              -27.42937503120356
            ],
            [
              153.24701187416892,
              -27.394184580520076
            ],
            [
              153.26161928741246,
              -27.35754410365523
            ],
            [
              153.27210434152022,
              -27.319807714102932
            ],
            [
              153.27837006181565,
              -27.28133974810862
            ],
            [
              153.28036042365733,
              -27.24251122192726
            ],
            [
              153.2780607244656,
              -27.203696239033093
            ],
            [
              153.27149755321855,
              -27.1652683825183
            ],
            [
              153.26073836436063,
              -27.127597127477696
            ],
            [
              153.24589066669793,
              -27.091044307410176
            ],
            [
              153.2271008411424,
              -27.05596066760467
            ],
            [
              153.20455260410077,
              -27.02268253714855
            ],
            [
              153.17846513589242,
              -26.99152864962923
            ],
            [
              153.14909089582656,
              -26.96279714082435
            ],
            [
              153.11671314750043,
              -26.936762749719215
            ],
            [
              153.08164321951213,
              -26.913674247078035
            ],
            [
              153.0442175281463,
              -26.89375211355012
            ],
            [
              153.00479438971365,
              -26.877186486936214
            ],
            [
              152.96375065113386,
              -26.86413539579049
            ],
            [
              152.92147816807622,
              -26.854723294008753
            ],
            [
              152.87838016053223,
              -26.849039908467095
            ],
            [
              152.834867476118,
              -26.847139409141228
            ]
          ]
        ]
      }
    }
  ]
}'''


def main():
    """Example of how to debug this process, running outside a PyWPS instance.
    """
    datasets_config = {
        "dataset_layers": {
            "smips": ["totalbucket", "SMindex"],
            "aet": ["ETa"]
        },
        "index_locations": {
            #"smips": "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-smips-public/v1_0/index.shp",
            "smips": "/vsiswift/landscapes-csiro-smips-public/v1_0/index.shp",
            "aet": "/vsiswift/landscapes-csiro-aet-public/v2_2/index.shp",
            #"aet": "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-aet-public/v2_2/index.shp",
        },
        "estimate_size_ratio": {
            "smips:totalbucket": 0.0405,
            "smips:SMindex": 0.0405,
            "aet:ETa": 25.6,
        }
    }
    import asyncio
    stats_op = Stats3D(ppe=None, datasets_config=datasets_config)
    request = WPSRequest()
    request.check_and_set_language(None)
    workdir = os.path.abspath('./../../../../workdir')
    tempdir = tempfile.mkdtemp(prefix='pywps_process_', dir=workdir)
    stats_op.set_workdir(tempdir)
    request.store_execute = True
    request.outputs['csv'] = {"mimetype": 'text/csv', 'asReference': "true"}
    response_cls = get_response("execute")
    response = response_cls(request, process=stats_op, uuid="test")
    dataset_in = stats_op.inputs[0]
    #dataset_in.data = "smips:totalbucket"
    dataset_in.data = "aet:ETa"
    request.inputs["datasetId"] = [dataset_in]
    startdate_in = stats_op.inputs[1]
    startdate_in.data = datetime.datetime(2020,1,9)
    request.inputs["startDate"] = [startdate_in]
    enddate_in = stats_op.inputs[2]
    enddate_in.data = datetime.datetime(2020,6,11)
    request.inputs["endDate"] = [enddate_in]
    exceldates_in = stats_op.inputs[3]
    exceldates_in.data = True
    request.inputs['excelDates'] = [exceldates_in]
    widetable_in = stats_op.inputs[4]
    widetable_in.data = True
    request.inputs['wideTable'] = [widetable_in]
    rettype_in = stats_op.inputs[5]
    rettype_in.data = "auto"
    request.inputs["returnType"] = [rettype_in]
    poly_in = stats_op.inputs[6]
    poly_in.data = example_feature
    request.inputs["polygon"] = [poly_in]
    loop = asyncio.get_event_loop()
    loop.run_until_complete(stats_op._handler(request, response))
    #print(response.outputs['csv'].data)
    doc = response._construct_doc()
    print("All good!")


if __name__ == "__main__":
    main()
