import random
from typing import TYPE_CHECKING
from numba import njit
from numba.core.types import int16
import numpy as np
from shapely import Point, MultiPoint, ops
from sklearn.cluster import KMeans
import asyncio
import ujson

if TYPE_CHECKING:
    from pywps import ComplexOutput
    from pywps.inout.basic import DataHandler


TRUTHS = {"TRUE", "true", "True", True, 1, "1", "T", "Y", "yes", "YES", "y"}
GEOJSON_FEATURE_ID_PROP = "_id_"

#signature="(float64,float64,float64,float64)(array, float64)"
@njit(nogil=True)
def minmaxmean(x: np.ndarray, nodata=np.NaN):
    """Return minimum, maximum, mean, and invalid from 2D array for axis=0."""
    m, n = len(x), len(x[0])
    default = np.NaN
    # find first non-NaN value to use as the initial min/max value.
    for i in range(m):
        for j in range(n):
            v = x[i, j]
            if np.isnan(v) or v == nodata:
                continue
            default = v
            break
        else:
            continue
        break
    # Didn't find any valid values?
    if np.isnan(default):
        return np.NaN, np.NaN, np.NaN, 0.0, 0.0
    mi = ma = default
    sum_total = 0.0
    count = 0
    invalid = 0
    valid = 0
    sum_sq = 0.0
    for i in range(m):
        for j in range(n):
            v = x[i, j]
            count += 1
            if np.isnan(v) or v == nodata:
                invalid += 1
                continue
            else:
                valid += 1
            sum_total += v
            sum_sq += v*v
            if v > ma:
                ma = v
            elif v < mi:
                mi = v
    mean = 0.0 if valid < 1 else (sum_total / valid)
    variance = 0.0 if valid < 1 else ((valid * sum_sq)-(sum_total*sum_total)) / (valid*valid)
    std_dev = variance ** 0.5  # square root of variance
    valid_percent = 0.0 if count < 1 else ((valid * 100) / count)
    return mi, ma, mean, valid_percent, std_dev

@njit("float64(Array(i2, 2, 'A'), i2)",nogil=True)
def int16_nonanmean(x: np.ndarray, nodata: int16 = -9999):
    """Return mean for an array, ignoring nodata and NaN values"""
    m, n = len(x), len(x[0])
    total = 0.0
    count = 0.0
    invalid = 0.0
    valid = 0.0
    for i in range(m):
        for j in range(n):
            v = x[i, j]
            count += 1.0
            if v == nodata:
                invalid += 1.0
                continue
            else:
                valid += 1.0
            total += float(v)
    mean = 0.0 if valid < 1.0 else (total / valid)
    return mean


async def complete_tasks(tasks, tasks_meta, response, worker_pool=None, unscheduled_ppe=[]):
    all_done = []
    loop = asyncio.get_event_loop()
    # if there are no scheduled tasks, first schedule some, before continuing
    if len(tasks) < 1 and worker_pool is not None and unscheduled_ppe:
        _new_slots = min(len(unscheduled_ppe), getattr(worker_pool, "_max_workers", 16))
        for _ in range(_new_slots):
            (exec_fn, exec_args, exec_metas) = unscheduled_ppe.pop(0)
            a = loop.run_in_executor(worker_pool, exec_fn, *exec_args)
            tasks_meta[a] = exec_metas
            tasks.append(a)
    task_count = len(tasks) + len(unscheduled_ppe)

    finished, pending = await asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED)
    # We're being cautious with the Pool, add more slowly
    if worker_pool is not None and unscheduled_ppe:
        pending = list(pending)
        _new_slots = min(len(unscheduled_ppe), len(finished), getattr(worker_pool, "_max_workers", 16))
        for _ in range(_new_slots):
            (exec_fn, exec_args, exec_metas) = unscheduled_ppe.pop(0)
            a = loop.run_in_executor(worker_pool, exec_fn, *exec_args)
            tasks_meta[a] = exec_metas
            pending.append(a)
    all_done.extend((tasks_meta[f], f.result()) for f in finished)
    percentage = (len(all_done) * 100) // task_count
    if percentage > 1:
        response.update_status(f"Completed {len(all_done)} of {task_count} computation tasks", percentage)
    while len(pending):
        finished, pending = await asyncio.wait(pending, return_when=asyncio.FIRST_COMPLETED)
        # We're being cautious with the Pool, add more slowly
        if worker_pool is not None and unscheduled_ppe:
            pending = list(pending)
            _new_slots = min(len(unscheduled_ppe), len(finished), getattr(worker_pool, "_max_workers", 16))
            for _ in range(_new_slots):
                (exec_fn, exec_args, exec_metas) = unscheduled_ppe.pop(0)
                a = loop.run_in_executor(worker_pool, exec_fn, *exec_args)
                tasks_meta[a] = exec_metas
                pending.append(a)
        all_done.extend((tasks_meta[f], f.result()) for f in finished)
        new_percentage = (len(all_done) * 100) // task_count
        if new_percentage > percentage:
            response.update_status(f"Completed {len(all_done)} of {task_count} computation tasks", new_percentage)
            percentage = new_percentage
    return all_done


def roughly_split_shape_by_area(shp, n_portions=8):
    """Returns list of tuples: [ ( slice_geom, slice_weight ) ]"""
    rand_points = []
    encompassing_points = []
    i = 0
    while len(encompassing_points) < 8192:
        rand_points.clear()
        for _ in range(8192):  # len(rand_points) < 10_000:
            x_pt = random.uniform(shp.bounds[0], shp.bounds[2])
            y_pt = random.uniform(shp.bounds[1], shp.bounds[3])
            p = Point([x_pt, y_pt])
            i = i + 1
            rand_points.append(p)
        m = MultiPoint(points=rand_points)
        d = m.intersection(shp)
        encompassing_points.extend((p.x, p.y) for p in d.geoms)
    np_points = np.vstack(encompassing_points)

    km = KMeans(n_clusters=n_portions, n_init=10)
    labels = km.fit_predict(np_points)
    cluster_geoms = []
    centroids = []
    for i in range(n_portions):
        cluster_xy = np_points[labels == i]
        g = MultiPoint(points=cluster_xy)
        cluster_geoms.append(g)
        centroids.append(g.centroid)
    c_geom = MultiPoint(points=centroids)
    # Envelope here does _nothing_, see this bug: https://github.com/shapely/shapely/issues/981
    d = ops.voronoi_diagram(c_geom, envelope=shp)
    geoms = []
    whole_area = shp.area
    equal_area = whole_area / n_portions
    for a in d.geoms:
        a = a.intersection(shp)
        geoms.append((a, a.area / equal_area))
    return geoms

def get_identifier_for_feature(feature, fallback=None):
    found = None

    search_for = ("name", "identifier", GEOJSON_FEATURE_ID_PROP, "id", "_id")

    search_in = ("ste_name", "ste_code")

    try:
        keys = feature.keys()
    except AttributeError:
        try:
            keys = dict(feature).keys()
        except ValueError:
            raise

    for k in keys:
        if str(k).lower() in search_for:
            try:
                found = feature[k]
                break
            except LookupError:
                try:
                    found = getattr(feature, k)
                    break
                except AttributeError:
                    found = None

    if found is None:
        for s in search_in:
            for k in keys:
                _lower = str(k).lower()
                if s in _lower:
                    try:
                        found = feature[k]
                        break
                    except LookupError:
                        try:
                            found = getattr(feature, k)
                            break
                        except AttributeError:
                            found = None
            if found:
                break

    if found is None and "properties" in keys:
        try:
            properties = feature['properties']
        except LookupError:
            try:
                properties = getattr(feature, "properties")
            except AttributeError:
                properties = None

        if properties is not None:
            found = get_identifier_for_feature(properties, fallback=fallback)

    if found is None:
        found = fallback
    else:
        # Remove any commas in the ID, to it doesn't mess with the CSV formatting.
        found = str(found).replace(",","")
    return found

def set_output_filename_for_complexoutput(output: 'ComplexOutput', filepath: str):
    """
    Note, this also writes the file! Any further changes to data will not be written to the file!
    Filepath is relative to server workdir folder.
    (It gets copied to output dir as a different step later)
    :param output:
    :type output:
    :param filepath:
    :type filepath:
    :return:
    :rtype:
    """
    handler: 'DataHandler' = output._iohandler
    if handler is None:
        print("Cannot set output filename, no data handler is present. Set data first.")
        return
    if handler._file is None:
        internal_data = handler.data
        if internal_data is None:
            print("Cannot set output filename, no data on handler. Set data first.")
            return
        handler._file = handler._ref()._build_file_name(href=filepath)
        openmode = handler._openmode(internal_data)
        kwargs = {} if 'b' in openmode else {'encoding': 'utf8'}
        with open(handler._file, openmode, **kwargs) as fh:
            if isinstance(internal_data, (bytes, str)):
                fh.write(internal_data)
            else:
                ujson.dump(internal_data, fh)
