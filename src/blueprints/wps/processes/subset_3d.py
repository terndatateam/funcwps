import asyncio
import datetime
import gc
import os
import tempfile
from _weakref import ProxyType
from copy import deepcopy, copy
from io import BytesIO
from os.path import splitext
from typing import Union
from weakref import proxy
from asyncio import FIRST_COMPLETED
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from urllib.parse import urljoin
import time
from zipfile import ZipFile, ZIP_DEFLATED

import rasterio
from ciso8601 import parse_datetime
from osgeo.ogr import DataSource
from pywps import LiteralInput, LiteralOutput, ComplexInput, FORMATS, Format, ComplexOutput, WPSRequest
from pywps.app.Common import Metadata
from pywps.response.status import WPS_STATUS
from pywps.inout.literaltypes import AnyValue
from pandas._libs.json import ujson_dumps, ujson_loads
from pywps.response import get_response
from pywps.response.execute import ExecuteResponse
import dateutil
from osgeo import ogr, gdal
import rasterio as rio
from rasterio.mask import mask
import rioxarray
from distributed import Client, Security
from dask.distributed import Client
from dask import delayed
from shapely.geometry import shape
from netCDF4 import Dataset
import numpy as np
from src.blueprints.wps.processes.util import minmaxmean, get_identifier_for_feature, TRUTHS
from src.utils import TempEnv, is_debugging

try:
    from ..async_process import AsyncProcess
except ImportError:
    from src.blueprints.wps.async_process import AsyncProcess


GEOJSON = Format('application/vnd.geo+json', schema="http://geojson.org/geojson-spec.html#", extension='.geojson')
GEOJSON_POINT = Format('application/vnd.geo+json', schema="http://geojson.org/geojson-spec.html#point", extension='.geojson')
GEOJSON_POLYGON = Format('application/vnd.geo+json', schema="http://geojson.org/geojson-spec.html#polygon", extension='.geojson')
GEOJSON_ANY = Format(
    "application/vnd.geo+json", schema="http://geojson.org/geojson-spec.html#geojson", extension=".geojson"
)
TJS_JSON = Format("application/vnd.terriajs.catalog-member+json", schema="https://tools.ietf.org/html/rfc7159", extension=".json")
OUT_ZIP = Format("application/zip", extension=".zip")
OUT_NC4 = Format("application/x-netcdf4", extension=".nc")
USE_COG_DRIVER = True  # When stitching together GeoTiffs, use the GDAL_Warp COG output driver
MODE = os.getenv("WPS_PROCESSING_MODE", "multiprocessing")
MAX_JOB_OUTPUT_MB = float(os.getenv("WPS_MAX_JOB_OUTPUT_MB", "3099.0"))
MAX_JOB_MEMORY_MB = float(os.getenv("WPS_MAX_JOB_MEMORY_MB", "700.0"))
#MODE = "dask"


if MODE == "dask":
    if __name__ == "__main__":
        sec = Security(require_encryption=False)
        dask_client = Client("tcp://140.253.176.93:8999", security=sec, asynchronous=True)
else:
    dask_client = None

ogr.UseExceptions()

def subset2D(filename: str, layer: int, input_polygon: dict, extra_tags={}, save=True, virtual=False, env_vars={}):
    rets = []
    with TempEnv(**env_vars):
        if filename.startswith("/vsicurl/"):
            filename = filename[9:]
        ds = rio.open(filename)  # type: rio.DatasetReader
        masked, affine = mask(ds, [input_polygon], crop=True, filled=True, indexes=[layer])
        subset_data = masked[0]
    time.sleep(0)
    if not save:
        return (subset_data, affine)
    else:
        profile = copy(ds.profile)
        profile["transform"] = affine
        profile["count"] = 1
        profile["driver"] = "COG"
        profile["width"] = subset_data.shape[1]
        profile["height"] = subset_data.shape[0]
        profile["compress"] = "deflate"
        min, max, mean, valid_percent, std_dev = minmaxmean(subset_data, ds.nodata)
        if virtual:
            filepath = BytesIO()
        else:
            new_filename = "{}.cog.tif".format(str(time.time()))
            filepath = "outputs/{}".format(new_filename)
        with rasterio.Env(CPL_DEBUG=True):
            with rio.open(filepath, "w", **profile) as dst:
                dst.write(subset_data, 1)
                del subset_data
                del masked
                dst.update_tags(**ds.tags())
                dst.update_tags(**extra_tags)
                del ds
                dst.update_tags(
                    1,
                    STATISTICS_MAXIMUM=max,
                    STATISTICS_MINIMUM=min,
                    STATISTICS_MEAN=mean,
                    STATISTICS_VALID_PERCENT=valid_percent,
                    STATISTICS_STDDEV=std_dev,
                )
        gc.collect()
        return filepath


def make_zipfile_from_results(all_done):
    zipfile_name = f"outputs/{str(time.time())}.zip"
    zf = ZipFile(zipfile_name, "x", compression=ZIP_DEFLATED)
    try:
        for ai, (astats, aresult) in enumerate(all_done):
            _date: datetime.datetime = astats['date']
            _poly_id = astats['poly']
            _f, _ext = splitext(aresult)
            arcname = f"subset_{str(_poly_id)}-{str(ai)}_{_date.year}_{_date.month}_{_date.day}{_ext}"
            zf.write(aresult, arcname=arcname)
    finally:
        zf.close()
    for (_, aresult) in all_done:
        try:
            os.unlink(aresult)
        except Exception:
            pass
    return zipfile_name

crs_wkt =   'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["degree",0.0174532925199433]]'
crs_wkt_a = 'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]]'


def make_netcdf_from_results(all_done):
    ds_filename = f"outputs/{str(time.time())}.nc"
    outcube = Dataset(ds_filename, mode='w', format='NETCDF4')
    try:
        outcube.Conventions = 'CF-1.6'
        outcube.summary = 'subset data'
        outcube.title = 'subset3d'
        outcube.setncattr("spatial_ref", crs_wkt_a)
        ll_var = outcube.createVariable("crs", 'i1')
        ll_var.setncatts({
            "grid_mapping_name": "latitude_longitude",
            "long_name": "crs",
            "longitude_of_prime_meridian": 0.0,
            "semi_major_axis": 6378137.0,
            "inverse_flattening": 298.257223563,
            "spatial_ref": crs_wkt_a,
            "crs_wkt": crs_wkt_a
        })
        if len(all_done) < 1:
            return ds_filename
        first_meta, first_file = all_done[0]
        with rio.open(first_file) as first_ds: # type: rio.DatasetReader
            ds_shape = copy(first_ds.shape)
            ds_nodata = first_ds.nodata
            nrows = ds_shape[0]
            ncols = ds_shape[1]

            (_r, _l, _t, _b) = (first_ds.bounds.right, first_ds.bounds.left, first_ds.bounds.top, first_ds.bounds.bottom)
            x_width = (_r - _l) if _r >= _l else (_l - _r)
            y_height = (_t - _b) if _t >= _b else (_b - _t)
            x_step = x_width / (ncols-1)
            y_step = y_height / (nrows-1)
            lons = list(_l + (x_step * _i) for _i in range(0, ncols))
            lats = list(_b + (y_step * _i) for _i in range(0, nrows))
            dstype = str(first_ds.dtypes[0]).lower()
            if dstype == "int16":
                nctype = "i2"
            elif dstype == "float32":
                nctype = "f4"
            else:
                nctype = "f8"  # f8 (double) is native Python number datatype
        outcube.createDimension('lon', ncols)
        outcube.createDimension('lat', nrows)
        xlon = outcube.createVariable('lon', 'f8', ('lon',))
        ylat = outcube.createVariable('lat', 'f8', ('lat',))
        xlon.setncatts(
            {"standard_name": "longitude", "long_name": "longitude", "units": "degrees_east", 'axis': 'X'})
        ylat.setncatts(
            {"standard_name": "latitude", "long_name": "latitude", "units": "degrees_north", 'axis': 'Y'})
        outcube.createDimension('time', None)  # days
        nctime = outcube.createVariable('time', 'u4', ('time',))
        nctime.setncatts(
            {"standard_name": "time", "long_name": "time", "units": "days since 1900-01-01 00:00:00.0 -0:00", "calendar": "gregorian"})
        ncvals = outcube.createVariable("value", nctype, ('time', 'lat', 'lon'), zlib=True, fill_value=ds_nodata)
        ncvals.setncatts({"long_name": "value", "units": "", "grid_mapping": "crs"})

        xlon[:] = lons
        ylat[:] = lats

        times = []
        for ai, (ameta, afilename) in enumerate(all_done):
            with rio.open(afilename) as ds:  # type: rio.DatasetReader
                band1 = ds.read(1)
                # always flip when reading geotiffs.
                # rasterio can't read TIFFTAG_ORIENTATION, but assume its 1, and flip it
                ncvals[ai, :] = np.flipud(band1[:])
                atdate = ameta['date']
                dayssince = (atdate - datetime.datetime(1900, 1, 1, 0, tzinfo=datetime.timezone.utc)).days
                times.append(dayssince)
        nctime[:] = times[:]

    finally:
        outcube.close()
    for (_, aresult) in all_done:
        try:
            os.unlink(aresult)
        except Exception:
            pass
    return ds_filename

class Subset3D(AsyncProcess):
    SUCCESS_MESSAGE = 'finished 3d subset'
    allowed_returns = ["auto", "zip", "netcdf", "link", "terria"]

    def __init__(self, ppe: Union[ProcessPoolExecutor, ThreadPoolExecutor, None] = None, datasets_config: dict = {}):
        allowed_datasets = []
        for ds, ds_layers in datasets_config["dataset_layers"].items():
            for l_name in ds_layers:
                allowed_datasets.append("{}:{}".format(str(ds), str(l_name)))
        inputs = [
            LiteralInput('datasetId',
                         'dataset identifier',
                         min_occurs=1,
                         allowed_values=allowed_datasets,
                         default="smips:totalbucket"),
            LiteralInput('startDate',
                         'Date of start of timeseries',
                         data_type='date',
                         min_occurs=1,
                         allowed_values=AnyValue,
                         default="1970-01-01"),
            LiteralInput('endDate',
                         'Date of end of timeseries',
                         data_type='date',
                         min_occurs=1,
                         allowed_values=AnyValue,
                         default="19-01-2038"),
            LiteralInput('returnType',
                         'Optional, specify function return type',
                         min_occurs=0,
                         allowed_values=self.allowed_returns,
                         default="auto"),
            ComplexInput("polygon", "Polygon", [GEOJSON_ANY])
        ]
        outputs = [
            ComplexOutput("outfile", "Results", [TJS_JSON, OUT_ZIP, OUT_NC4]),
            LiteralOutput("download_link", "Download Link", data_type="string")
        ]

        super(Subset3D, self).__init__(
            self._handler,
            identifier='subset3d',
            version='None',
            title='SubSet 3D',
            abstract="Return a Zipfile or NetCDF4 file containing the dataset subset over time by the given 2D polygon.",
            profile='',
            metadata=[Metadata('slice'), Metadata('cut'), Metadata('subset'), Metadata('time')],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True
        )
        if ppe is None:
            self.ppe = ProcessPoolExecutor(4)
            #self.ppe = ThreadPoolExecutor(max_workers=os.cpu_count())
        else:
            if not isinstance(ppe, ProxyType):
                self.ppe = proxy(ppe)
            else:
                self.ppe = ppe
        self.datasets_config = datasets_config
        self.debugging = is_debugging()

    def __deepcopy__(self, memodict={}):
        ppe_ref = self.ppe
        self.ppe = None
        self.handler = None
        ourcopy = deepcopy(super(Subset3D, self), memodict)
        self.ppe = ppe_ref
        self.handler = self._handler
        ourcopy.ppe = ppe_ref
        ourcopy.handler = ourcopy._handler
        return ourcopy

    async def _handler(self, request, response: ExecuteResponse):
        try:
            app = self.service.app
            ctx = app.ctx.mywps
            has_ctx = True
        except (AttributeError, LookupError):
            #raise RuntimeError("Cannot get reference to App in Async Process.")
            has_ctx = False
        started = time.perf_counter()
        dataset_id_lit: LiteralInput = request.inputs['datasetId'][0]
        dataset_id: str = dataset_id_lit.data
        if ":" in dataset_id:
            dataset_id, layer_id = dataset_id.split(":", 1)
        else:
            layer_id = None

        startdate_lit: LiteralInput = request.inputs['startDate'][0]
        startdate: datetime.datetime = startdate_lit.data

        enddate_lit: LiteralInput = request.inputs['endDate'][0]
        enddate: datetime.datetime = enddate_lit.data

        try:
            ret_type_lit: LiteralInput = request.inputs['returnType'][0]
            ret_type = ret_type_lit.data
        except LookupError:
            ret_type = 'auto'

        poly: ComplexInput = request.inputs['polygon'][0]
        try:
            poly_data = ujson_loads(poly.data)
        except (ValueError, TypeError):
            raise
        polys = []
        try:
            crs = None
            features = []
            p_type = poly_data['type'].lower()
            if p_type == "featurecollection":
                # Get the first feature from featurecollection
                crs = poly_data.get('crs', crs)
                _features = poly_data.get('features', [])
                for (_ix, f) in enumerate(_features):
                    if 'geometry' in f and f['geometry'] is not None:
                        _id = get_identifier_for_feature(f, _ix)
                        features.append((_id, f['geometry']))
            elif p_type == "geometrycollection":
                # Get all geometries from geometrtycollection
                crs = poly_data.get('crs', crs)
                _geoms = poly_data.get('geometries', [])
                for (_ix, g) in enumerate(_geoms):
                    _id = get_identifier_for_feature(g, _ix)
                    features.append((_id, g))
            elif p_type == "feature":
                crs = poly_data.get('crs', crs)
                if 'geometry' in poly_data and poly_data['geometry'] is not None:
                    _id = get_identifier_for_feature(poly_data, 0)
                    features.append((_id, poly_data['geometry']))
            elif p_type == "polygon":
                _id = get_identifier_for_feature(poly_data, 0)
                polys.append((_id, poly_data))
            elif p_type == "multipolygon":
                _id = get_identifier_for_feature(poly_data, 0)
                polys.append((_id, poly_data))
            elif p_type == "point":
                # Weird, but we can do it. Need to get rid of Z
                poly_data['coordinates'] = poly_data['coordinates'][:2]
                _id = get_identifier_for_feature(poly_data, 0)
                polys.append((_id, poly_data))
            for (_id, f) in features:
                if f['type'].lower() == "polygon":
                    polys.append((_id, f))
                elif f['type'].lower() == "multipolygon":
                    polys.append((_id, f))
                elif f["type"].lower() == "point":
                    # Need to get rid of Z
                    f['coordinates'] = f['coordinates'][:2]
                    polys.append((_id, f))
            if crs is None:
                # All Geojson is assumed EPSG:4326 unless it has a CRS
                # Note, CRS is no longer part of the GeoJSON standard
                crs = poly_data.get("crs", "epsg:4326")
        except LookupError:
            raise

        try:
            estimate_size_ratio = self.datasets_config["estimate_size_ratio"][f"{dataset_id}:{layer_id}"]
        except LookupError:
            estimate_size_ratio = 20
        poly_shapes = []
        total_sq_degrees = 0
        for (_id, p) in polys:
            in_shape = shape(p)
            # Get area in square degrees
            total_sq_degrees += in_shape.area
            poly_shapes.append((_id, in_shape))
        estimate_size = total_sq_degrees * estimate_size_ratio

        if estimate_size >= MAX_JOB_OUTPUT_MB:
            response.status = WPS_STATUS.FAILED
            response.update_status("Geometry area results in dataset subset that is too large to serve on this server.", status_percentage=100)
            return response
        # This flag is set if data for the whole poly area can't fit into the allowed memory.
        # When it is set, we operate on each feature without multiprocessing
        operate_individually = False
        if estimate_size >= MAX_JOB_MEMORY_MB:
            operate_individually = True

        if isinstance(startdate, datetime.date):
            startdate = datetime.datetime(
                startdate.year, startdate.month, startdate.day,
                tzinfo=datetime.timezone.utc
            )
        if isinstance(enddate, datetime.date):
            enddate = datetime.datetime(
                enddate.year, enddate.month, enddate.day,
                hour=23, minute=59, second=59, microsecond=999999, tzinfo=datetime.timezone.utc
            )

        if startdate.tzinfo is None:
            startdate = startdate.replace(tzinfo=datetime.timezone.utc)
        if enddate.tzinfo is None:
            enddate = enddate.replace(tzinfo=datetime.timezone.utc)

        output_as_ref = False
        if "download_link" in request.outputs:
            output_type = "link_literal"
            output_as_ref = request.outputs["download_link"].get("asReference", "") in TRUTHS
            if output_as_ref:
                response.status = WPS_STATUS.FAILED
                response.update_status("Literal response cannot be asRef.", status_percentage=100)
                return response
            mimetype = request.outputs["download_link"].get("mimetype", "application/zip")
        elif "outfile" in request.outputs:
            output_as_ref = request.outputs["outfile"].get("asReference", "") in TRUTHS
            mimetype = request.outputs["outfile"].get("mimetype", "application/zip")
            if mimetype == "application/zip":
                output_type = "zip_file"
            elif mimetype == "application/x-netcdf4" or mimetype == "application/netcdf4":
                output_type = "netcdf_file"
            elif mimetype == "application/vnd.terriajs.catalog-member+json":
                output_type = "tjs"
            else:
                response.status = WPS_STATUS.FAILED
                response.update_status("Unsupported output Mimetype", status_percentage=100)
                return response

        else:
            if ret_type in self.allowed_returns:
                if ret_type == "zip":
                    output_type = "zip_file"
                    mimetype = "application/zip"
                elif ret_type == "netcdf":
                    output_type = "netcdf_file"
                    mimetype = "application/x-netcdf4"
                elif ret_type == "link":
                    output_type = "link_literal"
                    mimetype = "application/zip"
                elif ret_type == "terria":
                    output_type = "tjs"
                    mimetype = "application/vnd.terriajs.catalog-member+json"
                else:
                    # TODO: Determine how to detect a TerriaJS request automatically
                    output_type = "link_literal"
                    mimetype = "application/zip"
            else:
                response.status = WPS_STATUS.FAILED
                response.update_status("Cannot determine output type", status_percentage=100)
                return response

        index_loc = self.datasets_config["index_locations"].get(dataset_id, None)
        if not index_loc:
            response.update_status("", status_percentage=100)
            return response
        if index_loc.startswith("/vsiswift"):
            if has_ctx:
                env_vars = {
                    "SWIFT_STORAGE_URL": ctx.nectar_objectstore_url,
                    "SWIFT_AUTH_TOKEN": ctx.nectar_keystone_token
                }
            else:
                env_vars = {
                    "SWIFT_STORAGE_URL": os.getenv("SWIFT_STORAGE_URL", ""),
                    "SWIFT_AUTH_TOKEN": os.getenv("SWIFT_AUTH_TOKEN", "")
                }

        else:
            env_vars = {}

        driver_name = "ESRI Shapefile"
        with TempEnv(**env_vars):
            # All of this needs to be atomic, to preserve the env variables.
            # no thread swapping or async operations
            drv = ogr.GetDriverByName(driver_name)
            if drv is None:
                raise RuntimeError("{} driver not available.\n".format(driver_name))
            if index_loc.startswith("https") or index_loc.startswith("http"):
                ds: DataSource = drv.Open(f"/vsicurl/{index_loc}")
            else:
                ds = drv.Open(index_loc)
            if not ds:
                del drv
                raise LookupError(f"Cannot open index file for dataset {dataset_id}")
            layers = ds.GetLayerCount()
            if layers < 1 or layers > 1:
                if not self.debugging:
                    del ds
                del drv
                raise LookupError(f"Wrong number of layers in index for {dataset_id}")
            layer = next(iter(ds))
            to_get = []
            gc.disable()
            for feature in layer:  # type: osgeo.ogr.Feature
                if layer_id is not None:
                    lay = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("layer")))
                    if lay.lower() != layer_id.lower():
                        del feature
                        continue
                datestr = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("date")))
                dateat = parse_datetime(datestr)
                if not (startdate <= dateat <= enddate):
                    del feature
                    continue
                loc = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("location")))
                to_get.append((dateat, loc))
                del feature
                # gc.collect()  # GC Collect here helps with memory management, but significantly slows down the loop
            len_gets = len(to_get)
            if not self.debugging:
                del layer
                del ds
            del drv
            gc.collect()
            gc.enable()
        if len_gets < 1:
            response.update_status("No slices found in that time period.", status_percentage=100)
            return response
        response.update_status(f"Found {len_gets} time slices to subset from.", status_percentage=1)

        # After a potentially long index lookup, hand back to sanic to handle other requests waiting
        await asyncio.sleep(0)

        total_estimate_dl_size = estimate_size * len_gets
        if total_estimate_dl_size >= MAX_JOB_OUTPUT_MB:
            response.status = WPS_STATUS.FAILED
            response.update_status(
                "Given time dimension results in dataset subset that is too large to serve on this server.",
                status_percentage=100)
            return response

        if MODE == "multiprocessing":
            estimate_mem_size = estimate_size * min(len_gets, self.ppe._max_workers)
            if estimate_mem_size >= MAX_JOB_MEMORY_MB:
                operate_individually = True

        tasks_meta = {}
        tasks = []
        if MODE == "dask":
            delays = []
            delays_dates = {}
            #c = Client(address=cluster, asynchronous=True)
            c = dask_client
            for (d_date, d_loc) in to_get:
                slice_loc = urljoin(index_loc, d_loc)
                d = delayed(subset2D)(slice_loc, 1, poly_data, env_vars)
                delays.append(d)
                delays_dates[d.key] = d_date
            futures = c.compute(delays, traverse=False)
            for f in futures:
                d = delays_dates[f.key]
                g = c.gather((f,))
                t = asyncio.create_task(g)
                tasks.append(t)
                tasks_meta[t] = {"date": d}
        elif MODE == "multiprocessing" and not operate_individually:
            loop = asyncio.get_event_loop()
            for poly_id, poly_shape in poly_shapes:
                for (d_date, d_loc) in to_get:
                    slice_loc = urljoin(index_loc, d_loc)
                    # TODO: work out how to turn layer_id into band number (1)
                    extratags = {
                        "TIFFTAG_DATETIME": d_date.isoformat(),
                        "TIFFTAG_ORIENTATION": 1,
                        "TIMESTAMP": d_date.isoformat(),
                    }
                    a = loop.run_in_executor(self.ppe, subset2D, slice_loc, 1, poly_shape, extratags, True, False, env_vars)
                    tasks_meta[a] = {"date": d_date, "poly": poly_id}
                    tasks.append(a)
        else:
            single_executor = ThreadPoolExecutor(1)

            async def dummy_async(*args, **kwargs):
                _loop = asyncio.get_event_loop()
                res = await _loop.run_in_executor(single_executor, subset2D, *args, **kwargs)
                return res

            if len(poly_shapes) > 1:
                response.update_status(
                    "Geometry area is too large to fit in system memory. Cannot run the job in parallel, falling back to serialized execution. This operation will take longer than usual to complete.",
                    status_percentage=0)
            raise NotImplementedError()
        del poly_shapes
        all_done = []
        task_count = len(tasks)
        gc.collect()
        finished, pending = await asyncio.wait(tasks, return_when=FIRST_COMPLETED)
        all_done.extend((tasks_meta[f], f.result()) for f in finished)
        percentage = (len(all_done)*100)//task_count
        if percentage > 1:
            response.update_status(f"Completed {len(all_done)} lookups", min(percentage, 99))
        while len(pending):
            finished, pending = await asyncio.wait(pending, return_when=FIRST_COMPLETED)
            all_done.extend((tasks_meta[f], f.result()) for f in finished)
            new_percentage = (len(all_done) * 100) // task_count
            if new_percentage > percentage:
                response.update_status(f"Completed {len(all_done)} lookups", min(new_percentage, 99))
                percentage = new_percentage
        all_done = sorted(all_done, key=lambda x: (x[0]['poly'], x[0]['date']))
        finished = time.perf_counter()
        elapsed = finished - started
        print("Elapsed: {}".format(elapsed))
        #output_type = "tjs"
        if output_type == "zip_file":
            zipfile_name = make_zipfile_from_results(all_done)
            response.outputs['outfile'].file = zipfile_name
            if output_as_ref and request.store_execute:
                response.outputs['outfile'].as_reference = True
            response.outputs['outfile'].mimetype = "application/zip"
            response.outputs['outfile'].schema = ""
            response.outputs['outfile'].data_format = OUT_ZIP
            del response.outputs["download_link"]
        elif output_type == "netcdf_file":
            nc_name = make_netcdf_from_results(all_done)
            response.outputs['outfile'].file = nc_name
            if output_as_ref and request.store_execute:
                response.outputs['outfile'].as_reference = True
            response.outputs['outfile'].mimetype = "application/x-netcdf4"
            response.outputs['outfile'].schema = ""
            response.outputs['outfile'].data_format = OUT_NC4
            del response.outputs["download_link"]
        elif output_type == "tjs":
            new_filename = make_zipfile_from_results(all_done)
            response.outputs['outfile'].file = new_filename
            response.outputs['outfile'].as_reference = True
            # This generates the output file, copies it to the outputs dir, and makes a URL for it
            resp_parts = response.outputs['outfile'].json
            resp_obj = ujson_dumps({
                "url": resp_parts['href'],
                "type": "url-reference",
                "name": f"Timeseries Subset of {layer_id}",
                "isMappable": True,
                "isOpenInWorkbench": True
            })
            response.outputs['outfile'].as_reference = False
            response.outputs['outfile'].data = resp_obj
            response.outputs['outfile'].mimetype = "application/vnd.terriajs.catalog-member+json"
            response.outputs['outfile'].schema = "https://tools.ietf.org/html/rfc7159"
            response.outputs['outfile'].data_format = TJS_JSON
            del response.outputs['download_link']
        elif output_type == "link_literal":
            new_filename = make_zipfile_from_results(all_done)
            response.outputs['outfile'].file = new_filename
            response.outputs['outfile'].as_reference = True
            # This generates the output file, copies it to the outputs dir, and makes a URL for it
            resp_parts = response.outputs['outfile'].json
            response.outputs['download_link'].data = resp_parts['href']
            response.outputs['download_link'].mimetype = "application/zip"
            del response.outputs['outfile']
        else:
            resp_obj = {
                "type": "FeatureConnection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Point",
                            "coordinates": [1, 2]
                        },
                        "properties": {
                            "totalbucket": 10.0,
                            "datetime": startdate
                        }
                    }
                ]
            }
            response.outputs['results'].data = ujson_dumps(resp_obj)
        response._update_status(WPS_STATUS.SUCCEEDED, 'Landscapes WPS Process succeeded', 100)


def main():
    """Example of how to debug this process, running outside a PyWPS instance.
    """
    datasets_config = {
        "dataset_layers": {
            "smips": ["totalbucket", "SMindex"],
            "aet": ["ETa"]
        },
        "index_locations": {
            # "smips": "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-smips-public/v1_0/index.shp",
            "smips": "/vsiswift/landscapes-csiro-smips-public/v1_0/index.shp",
            "aet": "/vsiswift/landscapes-csiro-aet-public/v2_2/index.shp",
            # "aet": "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-aet-public/v2_2/index.shp",
        },
        "estimate_size_ratio": {
            "smips:totalbucket": 0.0405,
            "smips:SMindex": 0.0405,
            "aet:ETa": 25.6,
        }
    }
    import asyncio
    import shapefile
    import json
    subset_op = Subset3D(ppe=None, datasets_config=datasets_config)
    request = WPSRequest()
    request.check_and_set_language(None)
    workdir = os.path.abspath('./../../../../workdir')
    tempdir = tempfile.mkdtemp(prefix='pywps_process_', dir=workdir)
    subset_op.set_workdir(tempdir)
    request.store_execute = True
    # request.outputs['download_link'] = {"mimetype": 'image/tiff', 'asReference': "false"}
    #request.outputs['outfile'] = {"mimetype": 'application/x-netcdf4', 'asReference': "true"}
    request.outputs['outfile'] = {"mimetype": 'application/vnd.terriajs.catalog-member+json', 'asReference': "false"}
    response_cls = get_response("execute")
    response = response_cls(request, process=subset_op, uuid="test")
    dataset_in = subset_op.inputs[0]
    dataset_in.data = "aet:ETa"
    # dataset_in.data = "smips:totalbucket"
    request.inputs["datasetId"] = [dataset_in]
    startdate_in = subset_op.inputs[1]
    startdate_in.data = datetime.datetime(2016, 1, 1)
    request.inputs["startDate"] = [startdate_in]
    enddate_in = subset_op.inputs[2]
    enddate_in.data = datetime.datetime(2016, 2, 1)
    request.inputs["endDate"] = [enddate_in]
    rettype_in = subset_op.inputs[3]
    rettype_in.data = "auto"
    request.inputs["returnType"] = [rettype_in]
    poly_in = subset_op.inputs[4]
    #r = shapefile.Reader("../../../../states/STE_2021_AUST_GDA94")
    #shp_type = str(r.__geo_interface__['type']).lower()
    #poly_in.data = json.dumps(r.__geo_interface__)
    #poly_in.data = '{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[143.07458284473583,-25.299778026434737],[145.48941912557842,-24.191150157964923],[147.0971879845574,-25.018840622094245],[146.32395844216842,-26.294764561422262],[144.92154403855236,-26.871773668571137],[143.39132901313374,-26.679774495095096],[143.07458284473583,-25.299778026434737]]]}}]}'
    brisbane_poly = '''{
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "coordinates": [
          [
            [
              152.5233615970626,
              -27.606631709810152
            ],
            [
              152.8583116653001,
              -27.806203014365778
            ],
            [
              153.30678188645726,
              -27.81650604726859
            ],
            [
              153.4815105300732,
              -27.59477583833349
            ],
            [
              153.42326764420915,
              -27.341558834219995
            ],
            [
              152.9081303574169,
              -27.080272828628743
            ],
            [
              152.73303599218883,
              -27.286171674539688
            ],
            [
              152.5233615970626,
              -27.606631709810152
            ]
          ]
        ],
        "type": "Polygon"
      }
    }
  ]
}'''
    poly_in.data = brisbane_poly
    request.inputs["polygon"] = [poly_in]
    loop = asyncio.get_event_loop()
    loop.run_until_complete(subset_op._handler(request, response))
    #    print(response.outputs['outfile'].data)
    doc = response._construct_doc()
    print(doc)


if __name__ == "__main__":
    main()
