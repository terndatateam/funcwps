import asyncio
import datetime
import gc
import os
import tempfile
from _weakref import ProxyType
from copy import deepcopy, copy
from weakref import proxy
from asyncio import FIRST_COMPLETED
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from urllib.parse import urljoin
import time
from ciso8601 import parse_datetime
from pywps import LiteralInput, LiteralOutput, ComplexInput, FORMATS, Format, ComplexOutput, WPSRequest
from pywps.app.Common import Metadata
from pywps.response.status import WPS_STATUS
from pywps.exceptions import MissingParameterValue, InvalidParameterValue
from pywps.inout.literaltypes import AnyValue
from pywps.inout.formats import Supported_Formats
from pandas._libs.json import ujson_loads, ujson_dumps
from pywps.response import get_response
from pywps.response.execute import ExecuteResponse
from osgeo import ogr, gdal
import rasterio as rio
from rasterio.mask import mask
from io import BytesIO
import numpy as np
import rioxarray
from distributed import Client, Security
from dask.distributed import Client
from dask import delayed
from src.blueprints.wps.processes.util import minmaxmean, TRUTHS, get_identifier_for_feature, \
    set_output_filename_for_complexoutput

from src.utils import TempEnv, is_debugging

try:
    from ..async_process import AsyncProcess
except ImportError:
    from src.blueprints.wps.async_process import AsyncProcess


GEOJSON = Format("application/vnd.geo+json", schema="http://geojson.org/geojson-spec.html#", extension=".geojson")
GEOJSON_POINT = Format(
    "application/vnd.geo+json", schema="http://geojson.org/geojson-spec.html#point", extension=".geojson"
)
GEOJSON_LINESTRING = Format(
    "application/vnd.geo+json", schema="http://geojson.org/geojson-spec.html#linestring", extension=".geojson"
)
TJS_JSON = Format(
    "application/vnd.terriajs.catalog-member+json", schema="https://tools.ietf.org/html/rfc7159", extension=".json"
)
OUT_CSV = Format("text/csv", extension=".csv")
MODE = os.getenv("WPS_PROCESSING_MODE", "multiprocessing")


ogr.UseExceptions()

def subset1D(filename: str, layer: int, x1: float, x2: float, y1: float, y2: float, num: int = 512, env_vars={}):
    x_parts = np.linspace(x1, x2, num=num, endpoint=True)
    y_parts = np.linspace(y1, y2, num=num, endpoint=True)
    xy_parts = np.stack((x_parts, y_parts), axis=-1)
    with TempEnv(**env_vars):
        if filename.startswith("/vsicurl/"):
            filename = filename[9:]
        ds = rio.open(filename)  # type: rio.DatasetReader
        try:
            first = ds.sample(xy=(xy_parts[0],), indexes=[layer])
            first = list(first)
            if len(first) < 1:
                return []
            dtype = first[0][0].dtype
        except Exception as e:
            print(e)
            return []
        samples = ds.sample(xy=xy_parts, indexes=[layer])
    rets = np.fromiter(samples, dtype=dtype)
    return rets

class Subset1D(AsyncProcess):
    SUCCESS_MESSAGE = "Finished 1d transect"
    allowed_returns = ["auto", "terria", "csv", "link"]

    def __init__(self, ppe: ProcessPoolExecutor, datasets_config: dict = {}):
        allowed_datasets = []
        for ds, ds_layers in datasets_config["dataset_layers"].items():
            for l_name in ds_layers:
                allowed_datasets.append("{}:{}".format(str(ds), str(l_name)))
        inputs = [
            LiteralInput(
                "datasetId",
                "dataset identifier",
                min_occurs=1,
                allowed_values=allowed_datasets,
                default="smips:totalbucket",
            ),
            LiteralInput(
                "atDate",
                "Date of imagery to use",
                data_type="date",
                min_occurs=1,
                allowed_values=AnyValue,
                default="2019-01-01",
            ),
            LiteralInput('returnType',
                         'Optional, specify function return type',
                         min_occurs=0,
                         allowed_values=self.allowed_returns,
                         default="auto"),
            ComplexInput("line", "Line", [GEOJSON_LINESTRING]),
        ]
        outputs = [
            LiteralOutput("download_link", "CSV Download Link", data_type="string"),
            ComplexOutput("csv", "CSV Results", [TJS_JSON, OUT_CSV]),
        ]

        super(Subset1D, self).__init__(
            self._handler,
            identifier="subset1d",
            version="None",
            title="SubSet 1D",
            abstract="Return a CSV file containing the profile of the data along the line.",
            profile="",
            metadata=[Metadata("transect"), Metadata("profile"), Metadata("subset"), Metadata("stats")],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True,
        )
        if ppe is None:
            #self.ppe = ProcessPoolExecutor(32)
            self.ppe = ThreadPoolExecutor(max_workers=os.cpu_count())
        else:
            if not isinstance(ppe, ProxyType):
                self.ppe = proxy(ppe)
            else:
                self.ppe = ppe
        self.datasets_config = datasets_config
        self.debugging = is_debugging()

    def __deepcopy__(self, memodict={}):
        ppe_ref = self.ppe
        self.ppe = None
        self.handler = None
        ourcopy = deepcopy(super(Subset1D, self), memodict)
        self.ppe = ppe_ref
        self.handler = self._handler
        ourcopy.ppe = ppe_ref
        ourcopy.handler = ourcopy._handler
        return ourcopy

    async def _handler(self, request, response: ExecuteResponse):
        try:
            app = self.service.app
            ctx = app.ctx.mywps
            has_ctx = True
        except (AttributeError, LookupError):
            #raise RuntimeError("Cannot get reference to App in Async Process.")
            has_ctx = False
        started = time.perf_counter()
        dataset_id_lit: LiteralInput = request.inputs["datasetId"][0]
        dataset_id: str = dataset_id_lit.data
        if ":" in dataset_id:
            dataset_id, layer_id = dataset_id.split(":", 1)
        else:
            layer_id = None

        at_date_lit: LiteralInput = request.inputs["atDate"][0]
        at_date: datetime.datetime = at_date_lit.data

        try:
            ret_type_lit: LiteralInput = request.inputs['returnType'][0]
            ret_type = ret_type_lit.data
        except LookupError:
            ret_type = 'auto'

        in_line: ComplexInput = request.inputs["line"][0]
        try:
            line_data = ujson_loads(in_line.data)
        except (ValueError, TypeError):
            raise
        linecoord_groups = []
        try:
            crs = None
            feature_geoms = []
            l_type = line_data['type'].lower()
            if l_type == "featurecollection":
                # Get the first feature from featurecollection
                crs = line_data.get("crs", crs)
                _features = line_data.get('features', [])
                for (_ix, f) in enumerate(_features):
                    if 'geometry' in f and f['geometry'] is not None:
                        _id = get_identifier_for_feature(f, _ix)
                        feature_geoms.append((_id, f['geometry']))
            elif l_type == "geometrycollection":
                # Get all geometries from geometrtycollection
                crs = line_data.get('crs', crs)
                _geoms = line_data.get('geometries', [])
                for (_ix, g) in enumerate(_geoms):
                    _id = get_identifier_for_feature(g, _ix)
                    feature_geoms.append((_id, g))
            elif l_type == "feature":
                crs = line_data.get("crs", crs)
                if 'geometry' in line_data and line_data['geometry'] is not None:
                    _id = get_identifier_for_feature(line_data, 0)
                    feature_geoms.append((_id, line_data['geometry']))
            elif l_type == "multilinestring":
                crs = line_data.get("crs", crs)
                _id = get_identifier_for_feature(line_data, 0)
                for cid, coords in enumerate(line_data["coordinates"]):
                    _id_2 = f"{str(_id)}-{str(cid)}"
                    linecoord_groups.append((_id_2, coords))
            elif l_type == "linestring":
                crs = line_data.get("crs", crs)
                _id = get_identifier_for_feature(line_data, 0)
                linecoord_groups.append((_id, line_data["coordinates"]))
            else:
                response.status = WPS_STATUS.FAILED
                response.update_status("Bad GeoJSON geometry type", status_percentage=100)
                return response
            for (_id, fgeom) in feature_geoms:
                if fgeom['type'].lower() == "linestring":
                    linecoord_groups.append((_id, fgeom['coordinates']))
                elif fgeom['type'].lower() == "multilinestring":
                    for cid, coords in enumerate(fgeom["coordinates"]):
                        _id_2 = f"{str(_id)}-{str(cid)}"
                        linecoord_groups.append((_id_2, coords))

            if crs is None:
                # All Geojson is assumed EPSG:4326 unless it has a CRS
                # Note, CRS is no longer part of the GeoJSON standard
                crs = "epsg:4326"
        except LookupError:
            raise
        if len(linecoord_groups) < 1:
            response.status = WPS_STATUS.FAILED
            response.update_status("Not enough points in your line!", status_percentage=100)
            return response
        else:
            (_id_3, _coordlist) = linecoord_groups[0]
            if len(_coordlist) < 1:
                response.status = WPS_STATUS.FAILED
                response.update_status("Not enough points in your line!", status_percentage=100)
                return response


        if isinstance(at_date, datetime.date):
            startdate = datetime.datetime(at_date.year, at_date.month, at_date.day, tzinfo=datetime.timezone.utc)

        if startdate.tzinfo is None:
            startdate = startdate.replace(tzinfo=datetime.timezone.utc)

        output_as_ref = False
        if "download_link" in request.outputs:
            output_type = "link_literal"
            output_as_ref = request.outputs["download_link"].get("asReference", "") in TRUTHS
            if output_as_ref:
                response.status = WPS_STATUS.FAILED
                response.update_status("Literal response cannot be asRef.", status_percentage=100)
                return response
            mimetype = request.outputs["download_link"].get("mimetype", "text/csv")
        elif "csv" in request.outputs:
            output_type = "csv_file"
            output_as_ref = request.outputs["csv"].get("asReference", "") in TRUTHS
            mimetype = request.outputs["csv"].get("mimetype", "text/csv")
            if mimetype == "text/csv":
                output_type = "csv_file"
            else:
                # Assume mimetype is "application/vnd.terriajs.catalog-member+json"
                # but of course TerriaJS doesn't set this option on the request
                output_type = "tjs"
        else:
            if ret_type in self.allowed_returns:
                if ret_type == "csv":
                    output_type = "csv_file"
                    mimetype = "text/csv"
                elif ret_type == "link":
                    output_type = "link_literal"
                    mimetype = "text/csv"
                elif ret_type == "terria":
                    output_type = "tjs"
                    mimetype = "application/vnd.terriajs.catalog-member+json"
                else:
                    # TODO: Determine how to detect a TerriaJS request automatically
                    output_type = "tjs"
                    mimetype = "application/vnd.terriajs.catalog-member+json"
            else:
                response.status = WPS_STATUS.FAILED
                response.update_status("Cannot determine output type", status_percentage=100)
                return response

        index_loc = self.datasets_config["index_locations"].get(dataset_id, None)
        if not index_loc:
            response.status = WPS_STATUS.FAILED
            response.update_status("", status_percentage=100)
            return response
        if index_loc.startswith("/vsiswift"):
            if has_ctx:
                env_vars = {
                    "SWIFT_STORAGE_URL": ctx.nectar_objectstore_url,
                    "SWIFT_AUTH_TOKEN": ctx.nectar_keystone_token
                }
            else:
                env_vars = {
                    "SWIFT_STORAGE_URL": os.getenv("SWIFT_STORAGE_URL", ""),
                    "SWIFT_AUTH_TOKEN": os.getenv("SWIFT_AUTH_TOKEN", "")
                }
        else:
            env_vars = {}
        driver_name = "ESRI Shapefile"
        with TempEnv(**env_vars):
            # All of this needs to be atomic, to preserve the env files.
            # no thread swapping or async operations
            drv = ogr.GetDriverByName(driver_name)
            if drv is None:
                raise RuntimeError("{} driver not available.\n".format(driver_name))

            if index_loc.startswith("https") or index_loc.startswith("http"):
                ds = drv.Open(f"/vsicurl/{index_loc}")
            else:
                ds = drv.Open(index_loc)
            if not ds:
                del drv
                raise LookupError(f"Cannot open index file for dataset {dataset_id}")
            layers = ds.GetLayerCount()
            if layers < 1 or layers > 1:
                if not self.debugging:
                    del ds
                del drv
                raise LookupError(f"Wrong number of layers in index for {dataset_id}")
            layer = next(iter(ds))
            to_get = []
            gc.disable()
            for feature in layer:  # type: osgeo.ogr.Feature
                if layer_id is not None and len(layer_id) > 0:
                    lay = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("layer")))
                    if lay.lower() != layer_id.lower():
                        del feature
                        continue
                datestr = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("date")))
                dateat = parse_datetime(datestr)
                if dateat < startdate:
                    del feature
                    continue
                loc = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("location")))
                to_get.append((dateat, loc))
                del feature
                break  # just get the first one
            len_gets = len(to_get)
            if not self.debugging:
                del layer
                del ds
            del drv
            gc.collect()
            gc.enable()

        if len_gets < 1:
            response.status = WPS_STATUS.FAILED
            response.update_status("No slices found in that time period.", status_percentage=100)
            return response

        # After a potentially long index lookup, hand back to sanic to handle other requests waiting
        await asyncio.sleep(0)
        response.update_status(f"Found {len_gets} slices to cut portion.", status_percentage=1)
        tasks_meta = {}
        task_line_parts = {}
        tasks = []

        loop = asyncio.get_event_loop()
        for (group_id, group_coords) in linecoord_groups:
            for (d_date, d_loc) in to_get:
                if len(group_coords) < 1:
                    continue
                slice_loc = urljoin(index_loc, d_loc)
                old_x, old_y = group_coords[0][:2]
                for i, p in enumerate(group_coords[1:]):
                    x, y = p[:2]
                    a = loop.run_in_executor(self.ppe, subset1D, slice_loc, 1, old_x, x, old_y, y, 512)
                    tasks_meta[a] = {"date": d_date, "group_id": group_id}
                    task_line_parts[a] = (i, old_x, old_y)
                    tasks.append(a)
                    old_x, old_y = x, y
                break
        del linecoord_groups
        all_done = []
        task_count = len(tasks)
        gc.collect()
        finished, pending = await asyncio.wait(tasks, return_when=FIRST_COMPLETED)
        all_done.extend((tasks_meta[f], task_line_parts[f], f.result()) for f in finished)
        percentage = (len(all_done)*100)//task_count
        if percentage > 1:
            response.update_status(f"Completed {len(all_done)} lookups", percentage)
        while len(pending):
            finished, pending = await asyncio.wait(pending, return_when=FIRST_COMPLETED)
            all_done.extend((tasks_meta[f], task_line_parts[f], f.result()) for f in finished)
            new_percentage = (len(all_done) * 100) // task_count
            if new_percentage > percentage:
                response.update_status(f"Completed {len(all_done)} lookups", new_percentage)
                percentage = new_percentage
        all_done = sorted(all_done, key=lambda x: (x[0]['group_id'], x[1][0]))
        finished = time.perf_counter()
        elapsed = finished - started
        print("Elapsed: {}".format(elapsed), flush=True)
        # output_type = "tjs"
        if output_type == "tjs":
            # Headers designed for TerriaJS rendering
            csvString = f"row,time,FID,segment,sample,value\n"
        else:
            csvString = f"date,feature,segment,sample,value\n"
        _row = 0
        for _meta, (line_part, x, y), line_segment_samples in all_done:
            dt = _meta["date"]
            group_id = _meta["group_id"]
            if output_type == "tjs":
                for (si, v) in enumerate(line_segment_samples):
                    csvString += "".join(
                        (str(_row), ",", dt.isoformat(), ',', str(group_id), ",", str(line_part), ',', str(si), ',',
                         str(v), '\n'))
                    _row += 1
            else:
                for (si, v) in enumerate(line_segment_samples):
                    csvString += "".join(
                        (dt.isoformat(), ',', str(group_id), ",", str(line_part), ',', str(si), ',',
                         str(v), '\n'))
                    _row += 1

        if output_type == "link_literal":
            new_filename = "{}.csv".format(str(time.time()))
            filepath = "outputs/{}".format(new_filename)
            with open(filepath, "w") as f:
                f.write(csvString)
            output_url = str(self.status_url).rsplit("/", 1)[0]
            link = "/".join((output_url, new_filename))
            response.outputs['download_link'].data = link
            del response.outputs['csv']
        elif output_type == "csv_file":
            response.outputs['csv'].data = csvString
            if output_as_ref and request.store_execute:
                response.outputs['csv'].as_reference = True
                set_output_filename_for_complexoutput(response.outputs['csv'], "stats1d.csv")
            response.outputs['csv'].mimetype = "text/csv"
            response.outputs['csv'].schema = ""
            response.outputs['csv'].data_format = OUT_CSV
            del response.outputs['download_link']
        elif output_type == "tjs":
            resp_obj = ujson_dumps({
                "csvString": csvString,
                "type": "csv",
                "name": layer_id,
                "columns": [
                    {
                        "name": "row",
                        "type": "scalar",
                    },
                    {
                        "name": "time",
                        "type": "time",
                    },
                    {
                        "name": "FID",
                        "type": "region",
                    },
                    {
                        "name": "segment",
                        "type": "region",
                    },
                    {
                        "name": "sample",
                        "type": "region",
                    },
                    {
                        "name": "value",
                        "type": "scalar",
                    }
                ],
                "styles": [{
                    "id": "style1",
                    "chart": {
                        "xAxisColumn": "row",
                        "lines": [{
                            "name": "value",
                            "yAxisColumn": "value",
                            "isSelectedInWorkbench": True
                            }
                        ]
                    },
                    "time": {
                        "timeColumn": "time",
                        "idColumns": ["FID", "segment", "sample"]
                    }
                }],
                "activeStyle": "style1"
            }, ensure_ascii=False)
            response.outputs['csv'].data = resp_obj
            response.outputs['csv'].mimetype = "application/vnd.terriajs.catalog-member+json"
            response.outputs['csv'].schema = "https://tools.ietf.org/html/rfc7159"
            response.outputs['csv'].data_format = TJS_JSON
            del response.outputs['download_link']
        else:
            dt, (subset, affine) = all_done[0]
            data = subset
            resp_obj = {
                "type": "FeatureConnection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {"type": "Point", "coordinates": [1, 2]},
                        "properties": {"totalbucket": 10.0, "datetime": startdate},
                    }
                ],
            }
            response.outputs["results"].data = ujson_dumps(resp_obj, ensure_ascii=False)
        response._update_status(WPS_STATUS.SUCCEEDED, 'Landscapes WPS Process succeeded', 100)


def main():
    """Example of how to debug this process, running outside a PyWPS instance."""
    import asyncio
    datasets_config = {
        "dataset_layers": {
            "smips": ["totalbucket", "SMindex"],
            "aet": ["ETa"]
        },
        "index_locations": {
            #"smips": "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-smips-public/v1_0/index.shp",
            "smips": "/vsiswift/landscapes-csiro-smips-public/v1_0/index.shp",
            "aet": "/vsiswift/landscapes-csiro-aet-public/v2_2/index.shp",
            #"aet": "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-aet-public/v2_2/index.shp",
        }
    }
    subset_line = Subset1D(ppe=None, datasets_config=datasets_config)
    request = WPSRequest()
    request.check_and_set_language(None)
    workdir = os.path.abspath('./../../../../workdir')
    tempdir = tempfile.mkdtemp(prefix='pywps_process_', dir=workdir)
    subset_line.set_workdir(tempdir)
    request.store_execute = True
    response_cls = get_response("execute")
    response = response_cls(request, process=subset_line, uuid="test")
    dataset_in = subset_line.inputs[0]
    dataset_in.data = "aet:ETa"
    request.inputs["datasetId"] = [dataset_in]
    atdate_in = subset_line.inputs[1]
    atdate_in.data = datetime.datetime(2016, 1, 1)
    request.inputs["atDate"] = [atdate_in]
    rettype_in = subset_line.inputs[2]
    rettype_in.data = "auto"
    request.inputs["returnType"] = [rettype_in]
    line_string = """{
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "coordinates": [
          [
            153.13380152928357,
            -28.429931697180393
          ],
          [
            153.18355823576604,
            -28.20544478412547
          ],
          [
            153.22709535393864,
            -28.112226068173925
          ],
          [
            153.16489947083528,
            -27.974992456790126
          ],
          [
            153.16489947083528,
            -27.865079676771572
          ],
          [
            153.04050770462715,
            -27.74955116638509
          ],
          [
            152.90989635010925,
            -27.73303707029263
          ],
          [
            152.80416334884364,
            -27.50709400681754
          ],
          [
            152.83720491174296,
            -27.327748676387273
          ],
          [
            152.84342450005414,
            -27.123118820703134
          ],
          [
            152.98647503119298,
            -27.056671745376143
          ],
          [
            152.99269461950416,
            -26.895928849154373
          ],
          [
            152.99269461950416,
            -26.712735926340308
          ]
        ],
        "type": "LineString"
      }
    }
  ]
}"""
    line_in = subset_line.inputs[3]
    line_in.data = line_string
    request.inputs["line"] = [line_in]
    loop = asyncio.get_event_loop()
    loop.run_until_complete(subset_line._handler(request, response))
    # assert response.outputs["drill_output"].data == drill.SUCCESS_MESSAGE
    doc = response._construct_doc()
    print(doc)


if __name__ == "__main__":
    main()
