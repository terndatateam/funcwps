import asyncio
import datetime
import os
import sys
import tempfile
from _weakref import ProxyType
from copy import deepcopy, copy
from math import ceil
from typing import Union
from weakref import proxy
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from urllib.parse import urljoin
import time
from ciso8601 import parse_datetime
from pywps import LiteralInput, LiteralOutput, ComplexInput, FORMATS, Format, ComplexOutput, WPSRequest
from pywps.app.Common import Metadata
from pywps.exceptions import MissingParameterValue, InvalidParameterValue
from pywps.inout.literaltypes import AnyValue
from pandas._libs.json import ujson_loads, ujson_dumps
from pywps.response import get_response
from pywps.response.status import WPS_STATUS
from pywps.response.execute import ExecuteResponse
import dateutil
from osgeo import ogr, gdal

from shapely.geometry import shape
from distributed import Client, Security
from dask.distributed import Client
from dask import delayed
from src.blueprints.wps.processes.util import TRUTHS, roughly_split_shape_by_area, \
    get_identifier_for_feature, set_output_filename_for_complexoutput, complete_tasks
from src.blueprints.wps.processes.shared_functions import stats2D
import gc
from src.utils import TempEnv, is_debugging

try:
    from ..async_process import AsyncProcess
except ImportError:
    from src.blueprints.wps.async_process import AsyncProcess


GEOJSON = Format('application/vnd.geo+json', schema="http://geojson.org/geojson-spec.html#", extension='.geojson')
GEOJSON_POINT = Format('application/vnd.geo+json', schema="http://geojson.org/geojson-spec.html#point", extension='.geojson')
GEOJSON_POLYGON = Format('application/vnd.geo+json', schema="http://geojson.org/geojson-spec.html#polygon", extension='.geojson')
GEOJSON_ANY = Format(
    "application/vnd.geo+json", schema="http://geojson.org/geojson-spec.html#geojson", extension=".geojson"
)
TJS_JSON = Format("application/vnd.terriajs.catalog-member+json", encoding="UTF-8", schema="https://tools.ietf.org/html/rfc7159", extension=".json")
OUT_CSV = Format("text/csv", encoding="UTF-8", extension=".csv")
MODE = os.getenv("WPS_PROCESSING_MODE", "multiprocessing")
MAX_JOB_OUTPUT_MB = float(os.getenv("WPS_MAX_JOB_OUTPUT_MB", "350.0"))
MAX_JOB_MEMORY_MB = float(os.getenv("WPS_MAX_JOB_MEMORY_MB", "700.0"))

MAX_OPTIM_GROUP = 8  # This is a tunable, how many lookups can we do on the same file in the same task?

if MODE == "dask":
    if __name__ == "__main__":
        sec = Security(require_encryption=False)
        dask_client = Client("tcp://140.253.176.93:8999", security=sec, asynchronous=True)
else:
    dask_client = None

ogr.UseExceptions()

class Stats2D(AsyncProcess):
    SUCCESS_MESSAGE = 'Finished Stats'
    allowed_returns = ["auto", "terria", "csv", "link"]

    def __init__(self, ppe: Union[ProcessPoolExecutor, ThreadPoolExecutor, None] = None, datasets_config: dict = {}):
        allowed_datasets = []
        for ds, ds_layers in datasets_config["dataset_layers"].items():
            for l_name in ds_layers:
                allowed_datasets.append("{}:{}".format(str(ds), str(l_name)))

        inputs = [
            LiteralInput('datasetId',
                         'dataset identifier',
                         min_occurs=1,
                         allowed_values=allowed_datasets,
                         default="smips:totalbucket"),
            LiteralInput('atDate',
                         'Date of imagery to use',
                         data_type='date',
                         min_occurs=1,
                         allowed_values=AnyValue,
                         default="2019-01-01"),
            LiteralInput('returnType',
                         'Optional, specify function return type',
                         min_occurs=0,
                         allowed_values=self.allowed_returns,
                         default="auto"),
            ComplexInput("polygon", "Polygon", [GEOJSON_ANY])
        ]
        outputs = [
            LiteralOutput("download_link", "CSV Download Link", data_type="string"),
            ComplexOutput("csv", "CSV Results", [OUT_CSV, TJS_JSON])
        ]

        super(Stats2D, self).__init__(
            self._handler,
            identifier='stats2d',
            version='None',
            title='Stats 2D',
            abstract="Return CSV file containing stats within given 2D polygon.",
            profile='',
            metadata=[Metadata('slice'), Metadata('cut'), Metadata('stats')],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True
        )
        if ppe is None:
            #self.ppe = ProcessPoolExecutor(32)
            self.ppe = ThreadPoolExecutor(max_workers=os.cpu_count())
        else:
            if not isinstance(ppe, ProxyType):
                self.ppe = proxy(ppe)
            else:
                self.ppe = ppe
        self.datasets_config = datasets_config
        self.debugging = is_debugging()

    def __deepcopy__(self, memodict={}):
        ppe_ref = self.ppe
        self.ppe = None
        self.handler = None
        ourcopy = deepcopy(super(Stats2D, self), memodict)
        self.ppe = ppe_ref
        self.handler = self._handler
        ourcopy.ppe = ppe_ref
        ourcopy.handler = ourcopy._handler
        return ourcopy

    async def _handler(self, request, response: ExecuteResponse):
        try:
            app = self.service.app
            ctx = app.ctx.mywps
            has_ctx = True
        except (AttributeError, LookupError):
            #raise RuntimeError("Cannot get reference to App in Async Process.")
            has_ctx = False
        started = time.perf_counter()
        response._update_status(WPS_STATUS.STARTED, 'Landscapes WPS Process started', 0)
        dataset_id_lit: LiteralInput = request.inputs['datasetId'][0]
        dataset_id: str = dataset_id_lit.data
        if ":" in dataset_id:
            dataset_id, layer_id = dataset_id.split(":", 1)
        else:
            layer_id = None

        at_date_lit: LiteralInput = request.inputs['atDate'][0]
        at_date: datetime.datetime = at_date_lit.data

        try:
            ret_type_lit: LiteralInput = request.inputs['returnType'][0]
            ret_type = ret_type_lit.data
        except LookupError:
            ret_type = 'auto'

        poly: ComplexInput = request.inputs['polygon'][0]
        try:
            poly_data = ujson_loads(poly.data)
        except (ValueError, TypeError):
            raise
        polys = []
        try:
            crs = None
            features = []
            p_type = poly_data['type'].lower()
            if p_type == "featurecollection":
                # Get the first feature from featurecollection
                crs = poly_data.get('crs', crs)
                _features = poly_data.get('features', [])
                for (_ix, f) in enumerate(_features):
                    if 'geometry' in f and f['geometry'] is not None:
                        _id = get_identifier_for_feature(f, _ix)
                        features.append((_id, f['geometry']))
            elif p_type == "geometrycollection":
                # Get all geometries from geometrtycollection
                crs = poly_data.get('crs', crs)
                _geoms = poly_data.get('geometries', [])
                for (_ix, g) in enumerate(_geoms):
                    _id = get_identifier_for_feature(g, _ix)
                    features.append((_id, g))
            elif p_type == "feature":
                crs = poly_data.get('crs', crs)
                if 'geometry' in poly_data and poly_data['geometry'] is not None:
                    _id = get_identifier_for_feature(poly_data, 0)
                    features.append((_id, poly_data['geometry']))
            elif p_type == "polygon":
                _id = get_identifier_for_feature(poly_data, 0)
                polys.append((_id, poly_data))
            elif p_type == "multipolygon":
                _id = get_identifier_for_feature(poly_data, 0)
                polys.append((_id, poly_data))
            elif p_type == "point":
                # Weird, but we can do it. Need to get rid of Z
                poly_data['coordinates'] = poly_data['coordinates'][:2]
                _id = get_identifier_for_feature(poly_data, 0)
                polys.append((_id, poly_data))
            for (_id, f) in features:
                if f['type'].lower() == "polygon":
                    polys.append((_id, f))
                elif f['type'].lower() == "multipolygon":
                    polys.append((_id, f))
                elif f["type"].lower() == "point":
                    # Need to get rid of Z
                    f['coordinates'] = f['coordinates'][:2]
                    polys.append((_id, f))
            if crs is None:
                # All Geojson is assumed EPSG:4326 unless it has a CRS
                # Note, CRS is no longer part of the GeoJSON standard
                crs = poly_data.get("crs", "epsg:4326")
        except LookupError:
            raise
        total_sq_degrees = 0
        try:
            estimate_size_ratio = self.datasets_config["estimate_size_ratio"][f"{dataset_id}:{layer_id}"]
        except LookupError:
            estimate_size_ratio = 20
        poly_shapes = []
        for (_id, p) in polys:
            in_shape = shape(p)
            # Get area in square degrees
            total_sq_degrees += in_shape.area
            poly_shapes.append((_id, in_shape))
        estimate_size = total_sq_degrees * estimate_size_ratio

        # This flag is set if data for the whole poly area can't fit into the allowed memory.
        # When it is set, we operate on each feature without multiprocessing
        operate_individually = False
        if estimate_size >= MAX_JOB_MEMORY_MB:
            #response.status = WPS_STATUS.FAILED
            #response.update_status("Geometry area results in dataset subset that cannot fit in the server's memory.", status_percentage=100)
            #return response
            operate_individually = True

        if isinstance(at_date, datetime.date):
            startdate = datetime.datetime(
                at_date.year, at_date.month, at_date.day,
                tzinfo=datetime.timezone.utc
            )

        if startdate.tzinfo is None:
            startdate = startdate.replace(tzinfo=datetime.timezone.utc)
        output_as_ref = False
        if "download_link" in request.outputs:
            output_type = "link_literal"
            output_as_ref = request.outputs["download_link"].get("asReference", "") in TRUTHS
            if output_as_ref:
                response.status = WPS_STATUS.FAILED
                response.update_status("Literal response cannot be asRef.", status_percentage=100)
                return response
            mimetype = request.outputs["download_link"].get("mimetype", "text/csv")
        elif "csv" in request.outputs:
            output_type = "csv_file"
            output_as_ref = request.outputs["csv"].get("asReference", "") in TRUTHS
            mimetype = request.outputs["csv"].get("mimetype", "text/csv")
            if mimetype == "text/csv":
                output_type = "csv_file"
            else:
                # Assume mimetype is "application/vnd.terriajs.catalog-member+json"
                # but of course TerriaJS doesn't set this option on the request
                output_type = "tjs"
        else:
            if ret_type in self.allowed_returns:
                if ret_type == "csv":
                    output_type = "csv_file"
                    mimetype = "text/csv"
                elif ret_type == "link":
                    output_type = "link_literal"
                    mimetype = "text/csv"
                elif ret_type == "terria":
                    output_type = "tjs"
                    mimetype = "application/vnd.terriajs.catalog-member+json"
                else:
                    # TODO: Determine how to detect a TerriaJS request automatically
                    output_type = "link_literal"
                    mimetype = "text/csv"
            else:
                response.status = WPS_STATUS.FAILED
                response.update_status("Cannot determine output type", status_percentage=100)
                return response


        index_loc = self.datasets_config["index_locations"].get(dataset_id, None)
        if not index_loc:
            response.update_status("", status_percentage=100)
            return response
        if index_loc.startswith("/vsiswift"):
            if has_ctx:
                env_vars = {
                    "SWIFT_STORAGE_URL": ctx.nectar_objectstore_url,
                    "SWIFT_AUTH_TOKEN": ctx.nectar_keystone_token
                }
            else:
                env_vars = {
                    "SWIFT_STORAGE_URL": os.getenv("SWIFT_STORAGE_URL", ""),
                    "SWIFT_AUTH_TOKEN": os.getenv("SWIFT_AUTH_TOKEN", "")
                }

        else:
            env_vars = {}
        driver_name = "ESRI Shapefile"
        with TempEnv(**env_vars):
            # All of this needs to be atomic, to preserve the env files.
            # no thread swapping or async operations
            drv = ogr.GetDriverByName(driver_name)
            if drv is None:
                raise RuntimeError("{} driver not available.\n".format(driver_name))
            if index_loc.startswith("https") or index_loc.startswith("http"):
                ds = drv.Open(f"/vsicurl/{index_loc}")
            else:
                ds = drv.Open(index_loc)
            if not ds:
                del drv
                raise LookupError(f"Cannot open index file for dataset {dataset_id}")
            layers = ds.GetLayerCount()
            if layers < 1 or layers > 1:
                if not self.debugging:
                    del ds
                del drv
                raise LookupError(f"Wrong number of layers in index for {dataset_id}")
            layer = next(iter(ds))
            to_get = []
            gc.disable()
            for feature in layer:  # type: osgeo.ogr.Feature
                if layer_id is not None:
                    lay = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("layer")))
                    if lay.lower() != layer_id.lower():
                        del feature
                        continue
                datestr = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("date")))
                dateat = parse_datetime(datestr)
                if dateat < startdate:
                    del feature
                    continue
                loc = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("location")))
                to_get.append((dateat, loc))
                del feature
                break  # we only want one
            len_gets = len(to_get)
            if not self.debugging:
                del layer
                del ds
            del drv
            gc.collect()
            gc.enable()

        if len_gets < 1:
            response.status = WPS_STATUS.FAILED
            response.update_status("No slices found in that time period.", status_percentage=100)
            return response

        # After a potentially long index lookup, hand back to sanic to handle other requests waiting
        await asyncio.sleep(0)
        response.update_status(f"Found {len_gets} slices to slice up.", status_percentage=0)
        tasks_meta = {}
        tasks = []
        unscheduled_ppe = []
        worker_pool = None
        if MODE == "dask" and not operate_individually:
            delays = []
            delays_dates = {}
            delays_polyids = {}
            delays_taskids = {}
            # c = Client(address=cluster, asynchronous=True)
            c = dask_client
            task_id = -1
            for poly_id, poly_shape in poly_shapes:
                # TODO: send optim_group for Dask
                # But that is harder because of the meta used.
                optim_group = []
                for (d_date, d_loc) in to_get:
                    slice_loc = urljoin(index_loc, d_loc)
                    task_id += 1
                    # TODO: work out how to turn layer_id into band number (1)
                    band_number = 1
                    task_tuple = (task_id, slice_loc, band_number, poly_shape)
                    d = delayed(stats2D)([task_tuple], "auto", env_vars)
                    delays.append(d)
                    delays_dates[d.key] = d_date
                    delays_polyids[d.key] = poly_id
                    delays_taskids[d.key] = task_id
                    break  # only ever doing 1 date in stats2d
            futures = c.compute(delays, traverse=False)
            for f in futures:
                d = delays_dates[f.key]
                poly_id = delays_polyids[f.key]
                task_id = delays_taskids[f.key]
                g = c.gather((f,))
                t = asyncio.create_task(g)
                tasks.append(t)
                tasks_meta[t] = {task_id: {"date": d, "poly": poly_id, "task_id": task_id}}
        elif MODE == "multiprocessing" and not operate_individually:
            # Don't put too many jobs on the queue at once, so others can share process pool too
            worker_pool = self.ppe
            task_id = -1
            for (d_date, d_loc) in to_get:
                slice_loc = urljoin(index_loc, d_loc)
                optim_group = []
                task_metas = {}
                for poly_id, poly_shape in poly_shapes:
                    task_id += 1
                    # TODO: work out how to turn layer_id into band number (1)
                    band_number = 1
                    task_tuple = (task_id, slice_loc, band_number, poly_shape)
                    task_meta = {"task_id": task_id, "date": d_date, "poly": poly_id}
                    task_metas[task_id] = task_meta
                    optim_group.append(task_tuple)
                    if len(optim_group) < MAX_OPTIM_GROUP:
                        continue
                    else:
                        unscheduled_ppe.append((stats2D, (optim_group, "auto", env_vars), task_metas))
                        task_metas = {}
                        optim_group = []
                if len(optim_group) > 0:
                    unscheduled_ppe.append((stats2D, (optim_group, "auto", env_vars), task_metas))
                del poly_shape
                break # Only do one date max
        else:
            single_executor = ThreadPoolExecutor(1)
            worker_pool = single_executor
            async def dummy_async(*args, **kwargs):
                _loop = asyncio.get_event_loop()
                res = await _loop.run_in_executor(single_executor, stats2D, *args, **kwargs)
                return res

            if len(poly_shapes) > 1:
                response.update_status(
                    "Geometry area is too large to fit in system memory. Cannot run the job in parallel, falling back to serialized execution. This operation will take longer than usual to complete.",
                    status_percentage=0)
            task_id = -1
            for poly_id, poly_shape in poly_shapes:
                estimate_mem = (poly_shape.area * estimate_size_ratio)
                if estimate_mem >= MAX_JOB_MEMORY_MB:
                    response.update_status(
                        "Geometry area is too large to fit in system memory. Chunking geometry to save memory. This operation will take much longer than usual to complete, and may fail.",
                        status_percentage=0)
                    splits_required = ceil(estimate_mem / MAX_JOB_MEMORY_MB)
                    #splits_required = 4
                    if splits_required > 8:
                        response.status = WPS_STATUS.FAILED
                        response.update_status(
                            "Geometry area results in dataset subset that cannot fit in the server's memory.",
                            status_percentage=100)
                        return response
                else:
                    splits_required = 0

                for (d_date, d_loc) in to_get:
                    band_number = 1
                    slice_loc = urljoin(index_loc, d_loc)
                    if splits_required > 0:
                        try:
                            splits = roughly_split_shape_by_area(poly_shape, splits_required)
                            for split_id, (split_poly, split_weight) in enumerate(splits):
                                task_id += 1
                                task_tuple = (task_id, slice_loc, band_number, split_poly)
                                task_meta = {"task_id": task_id, "date": d_date, "poly": poly_id, "split": True,
                                             "split_id": split_id, "split_weight": split_weight}
                                unscheduled_ppe.append(
                                    (dummy_async, ([task_tuple], "auto", env_vars), {task_id: task_meta}))
                            del split_poly
                        except Exception as e:
                            print(e)
                    else:
                        task_id += 1
                        task_tuple = (task_id, slice_loc, band_number, poly_shape)
                        task_meta = {"task_id": task_id, "date": d_date, "poly": poly_id, "split": False}
                        unscheduled_ppe.append((dummy_async, ([task_tuple], "auto", env_vars), {task_id: task_meta}))
                    break
            del poly_shape
        del poly_shapes
        gc.collect()
        all_done = await complete_tasks(tasks, tasks_meta, response, worker_pool, unscheduled_ppe)
        # unpack the ones that have optim_groups
        computed_results = []
        for (task_metas, task_res_list) in all_done:
            for task_res in task_res_list:
                task_id = task_res[0]
                task_meta = task_metas[task_id]
                computed_results.append((task_meta, task_res[1:]))
        # This is the default ordering for the non-wide table format
        # sort by polyid, from the task meta
        computed_results = sorted(computed_results, key=lambda x: x[0]['poly'])
        finished = time.perf_counter()
        elapsed = finished - started
        print("Elapsed: {}".format(elapsed))
        if output_type == "tjs":
            # Headers designed for TerriaJS rendering
            csvString = f"time,FID,min,max,mean,valid,stddev\n"
        else:
            csvString = f"date,feature,min,max,mean,valid,stddev\n"
        reassemble_split_stats = False
        for (task_meta, _) in computed_results:
            if task_meta.get("split", False):
                reassemble_split_stats = True

        if reassemble_split_stats:
            poly_parts = {}
            for (task_meta, val) in computed_results:
                dt = task_meta["date"]
                poly_id = task_meta["poly"]
                split_id = task_meta.get("split_id", 1)
                split_weight = task_meta.get("split_weight", 1.0)
                if (poly_id, dt) not in poly_parts:
                    poly_parts[(poly_id, dt)] = {}
                poly_parts[(poly_id, dt)][split_id] = (split_weight, val)
            for (poly_id, _dt), _parts in poly_parts.items():
                if len(_parts) < 1:
                    continue
                part_count = 0
                (_, first_vals) = _parts[next(iter(_parts.keys()))]
                _min = first_vals[0]
                _max = first_vals[1]
                _mean_accum = 0
                _valid_percent_accum = 0
                _std_deviation_accum = 0
                for (split_id, (split_weight, vals)) in _parts.items():
                    part_count += 1
                    if vals[0] < _min:
                        _min = vals[0]
                    if vals[1] > _max:
                        _min = vals[1]
                    _mean_accum += vals[2] * split_weight
                    _valid_percent_accum += vals[3] * split_weight
                    _std_deviation_accum += vals[4] * split_weight
                _std_dev = _std_deviation_accum / part_count
                _vld_percent = _valid_percent_accum / part_count
                _mean = _mean_accum / part_count
                csvString += "".join((_dt.isoformat(), ',', str(poly_id), ",", str(_min), ',', str(_max), ',', str(_mean), ',',
                                      str(_vld_percent), ',', str(_std_dev), '\n'))
        else:
            for (task_meta, val) in computed_results:
                dt = task_meta["date"]
                poly_id = task_meta["poly"]

                csvString += "".join((dt.isoformat(), ',', str(poly_id), ",", str(val[0]), ',', str(val[1]), ',', str(val[2]), ',',
                                      str(val[3]), ',', str(val[4]), '\n'))

        if output_type == "link_literal":
            new_filename = "{}.csv".format(str(time.time()))
            filepath = "outputs/{}".format(new_filename)
            with open(filepath, "w") as f:
                f.write(csvString)
            output_url = str(self.status_url).rsplit("/", 1)[0]
            link = "/".join((output_url, new_filename))
            response.outputs['download_link'].data = link
            del response.outputs['csv']
        elif output_type == "csv_file":
            response.outputs['csv'].data = csvString
            if output_as_ref and request.store_execute:
                response.outputs['csv'].as_reference = True
                set_output_filename_for_complexoutput(response.outputs['csv'], "stats2d.csv")
            response.outputs['csv'].mimetype = "text/csv"
            response.outputs['csv'].schema = ""
            response.outputs['csv'].data_format = OUT_CSV
            del response.outputs['download_link']
        elif output_type == "tjs":
            resp_obj = ujson_dumps({
                "csvString": csvString,
                "type": "csv",
                "name": layer_id,
                "columns": [
                    {
                        "name": "time",
                        "type": "time",
                    },
                    {
                        "name": "FID",
                        "type": "region",
                    },
                    {
                        "name": "mean",
                        "type": "scalar",
                    },
                    {
                        "name": "min",
                        "type": "scalar",
                    },
                    {
                        "name": "max",
                        "type": "scalar",
                    },
                    {
                        "name": "valid",
                        "type": "scalar",
                    },
                    {
                        "name": "stddev",
                        "type": "scalar",
                    }
                ],
                "styles": [{
                    "id": "style1",
                    "chart": {
                        "xAxisColumn": "time",
                        "lines": [{
                            "name": "mean",
                            "yAxisColumn": "mean",
                            "isSelectedInWorkbench": True
                        },
                            {
                                "yAxisColumn": "min",
                                "name": "min",
                                "isSelectedInWorkbench": True
                            },
                            {
                                "yAxisColumn": "max",
                                "name": "max",
                                "isSelectedInWorkbench": True
                            },
                        ]
                    },
                    "time": {
                        "timeColumn": "time",
                        "idColumns": ["FID"]
                    }
                }],
                "activeStyle": "style1"
            }, ensure_ascii=False)
            response.outputs['csv'].data = resp_obj
            response.outputs['csv'].mimetype = "application/vnd.terriajs.catalog-member+json"
            response.outputs['csv'].schema = "https://tools.ietf.org/html/rfc7159"
            response.outputs['csv'].data_format = TJS_JSON
            del response.outputs['download_link']
        else:
            resp_obj = {
                "type": "FeatureConnection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Point",
                            "coordinates": [1, 2]
                        },
                        "properties": {
                            "totalbucket": 10.0,
                            "datetime": startdate
                        }
                    }
                ]
            }
            response.outputs['csv'].data = ujson_dumps(resp_obj, ensure_ascii=False)
            del response.outputs['download_link']
        response._update_status(WPS_STATUS.SUCCEEDED, 'Landscapes WPS Process succeeded', 100)

def main():
    """Example of how to debug this process, running outside a PyWPS instance.
    """
    datasets_config = {
        "dataset_layers": {
            "smips": ["totalbucket", "SMindex"],
            "aet": ["ETa"]
        },
        "index_locations": {
            "smips": "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-smips-public/v1_0/index.shp",
            #"smips": "/vsiswift/landscapes-csiro-smips-public/v1_0/index.shp",
            #"aet": "/vsiswift/landscapes-csiro-aet-public/v2_2/index.shp",
            "aet": "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-aet-public/v2_2/index.shp",
        },
        "estimate_size_ratio": {
            "smips:totalbucket": 0.0405,
            "smips:SMindex": 0.0405,
            "aet:ETa": 25.6,
        }
    }
    import asyncio
    import shapefile
    import json
    stats_op = Stats2D(ppe=None, datasets_config=datasets_config)
    request = WPSRequest()
    request.check_and_set_language(None)
    workdir = os.path.abspath('./../../../../workdir')
    tempdir = tempfile.mkdtemp(prefix='pywps_process_', dir=workdir)
    stats_op.set_workdir(tempdir)
    request.store_execute = True
    request.outputs['csv'] = {"mimetype": 'text/csv', 'asReference': "true"}
    response_cls = get_response("execute")
    response = response_cls(request, process=stats_op, uuid="test")
    dataset_in = stats_op.inputs[0]
    #dataset_in.data = "aet:ETa"
    dataset_in.data = "smips:totalbucket"
    request.inputs["datasetId"] = [dataset_in]
    atdate_in = stats_op.inputs[1]
    atdate_in.data = datetime.datetime(2016,1,1)
    request.inputs["atDate"] = [atdate_in]
    rettype_in = stats_op.inputs[2]
    rettype_in.data = "auto"
    request.inputs["returnType"] = [rettype_in]
    poly_in = stats_op.inputs[3]
    #r = shapefile.Reader("../../../../states/STE_2021_AUST_GDA94")
    #shp_type = str(r.__geo_interface__['type']).lower()
    #poly_in.data = json.dumps(r.__geo_interface__)
    poly_in.data = '{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[143.07458284473583,-25.299778026434737],[145.48941912557842,-24.191150157964923],[147.0971879845574,-25.018840622094245],[146.32395844216842,-26.294764561422262],[144.92154403855236,-26.871773668571137],[143.39132901313374,-26.679774495095096],[143.07458284473583,-25.299778026434737]]]}}]}'
    request.inputs["polygon"] = [poly_in]
    loop = asyncio.get_event_loop()
    loop.run_until_complete(stats_op._handler(request, response))
    print(response.outputs['csv'].data)
    doc = response._construct_doc()
    print(doc)


if __name__ == "__main__":
    main()
