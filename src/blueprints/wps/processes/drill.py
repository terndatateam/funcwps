# Copyright (c) 2016 PyWPS Project Steering Committee
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import asyncio
import datetime
import gc
import os
from copy import deepcopy
from functools import partial
from typing import Union
from weakref import proxy, ProxyType
from asyncio import FIRST_COMPLETED
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from urllib.parse import urljoin
import time

import rasterio
from osgeo.ogr import DataSource
from pywps import LiteralInput, LiteralOutput, ComplexInput, FORMATS, Format, ComplexOutput, WPSRequest
from pywps.app.Common import Metadata
from pywps.exceptions import MissingParameterValue, InvalidParameterValue
from pywps.inout.literaltypes import AnyValue
from pandas._libs.json import ujson_dumps, ujson_loads
from pywps.response import get_response
from pywps.response.status import WPS_STATUS
from pywps.response.execute import ExecuteResponse
import dateutil
from ciso8601 import parse_datetime
from osgeo import ogr, gdal, osr
import rasterio as rio
from rasterio import RasterioIOError
from rasterio.mask import mask

from distributed import Client, Security
from dask.distributed import Client
from dask import delayed
from shapely.geometry import shape, MultiPoint, Point
from shapely.wkb import dumps as dump_wkb, loads as load_wkb
from shapely.ops import clip_by_rect

from src.blueprints.wps.processes.util import TRUTHS, set_output_filename_for_complexoutput
from src.utils import TempEnv, is_debugging
from src.blueprints.wps.processes.util import minmaxmean

try:
    from ..async_process import AsyncProcess
except ImportError:
    from src.blueprints.wps.async_process import AsyncProcess


GEOJSON = Format('application/vnd.geo+json', schema="http://geojson.org/geojson-spec.html#", extension='.geojson')
GEOJSON_POINT = Format('application/vnd.geo+json', schema="http://geojson.org/geojson-spec.html#point", extension='.geojson')
GEOJSON_POLYGON = Format('application/vnd.geo+json', schema="http://geojson.org/geojson-spec.html#polygon", extension='.geojson')
TJS_JSON = Format("application/vnd.terriajs.catalog-member+json", schema="https://tools.ietf.org/html/rfc7159", extension=".json")
OUT_CSV = Format("text/csv", schema="https://tools.ietf.org/html/rfc7159", extension=".csv")

MODE = os.getenv("WPS_PROCESSING_MODE", "multiprocessing")
MAX_JOB_OUTPUT_MB = float(os.getenv("WPS_MAX_JOB_OUTPUT_MB", "350.0"))
MAX_JOB_MEMORY_MB = float(os.getenv("WPS_MAX_JOB_MEMORY_MB", "700.0"))
#MODE = "dask"

if MODE == "dask":
    if __name__ == "__main__":
        sec = Security(require_encryption=False)
        dask_client = Client("tcp://140.253.176.93:8999", security=sec, asynchronous=True)
else:
    dask_client = None


ogr.UseExceptions()

def drill_slice(filename: str, layer: int, x: float, y: float, scale_offset="none", env_vars={}):
    # Layer is 1-based index of bands in the raster
    layer_i = layer - 1
    with TempEnv(**env_vars):
        if filename.startswith("/vsicurl/"):
            filename = filename[9:]
        try:
            ds = rio.open(filename)  # type: rio.DatasetReader
        except Exception as e:
            raise FileNotFoundError(e.args)
        try:
            layer_scale = ds.scales[layer_i]
        except LookupError:
            layer_scale = None
        try:
            layer_offset = ds.offsets[layer_i]
        except LookupError:
            layer_offset = None
        try:
            samples = ds.sample(xy=((x, y),), indexes=[layer])
            point_sample = next(iter(samples))  # Get the first point (the only point)
            layer_sample = next(iter(point_sample))  # Get the first layer (the only layer)
        except Exception as e:
            print(e)
            raise
        ds.close()
    # This allows other threads or processes to get a CPU time slot, if they are waiting
    time.sleep(0)
    if scale_offset is None or scale_offset == "none":
        pass
    elif scale_offset == "auto":
        if (layer_offset is None or layer_offset == 0.0) and\
                 (layer_scale is None or layer_scale == 1.0):
            pass
        else:
            if layer_scale is not None:
                fscale = float(layer_scale)
            else:
                fscale = 1.0
            if layer_offset is not None:
                foffset = float(layer_offset)
            else:
                foffset = 0.0
            layer_sample = (layer_sample * fscale) + foffset
    elif callable(scale_offset):
        layer_sample = scale_offset(layer_sample)
    return layer_sample

def masked_means_slice(slicefile: str, layer: int, mask_shape, scale_offset="none", env_vars={}):
    # Layer is 1-based index of bands in the raster
    layer_i = layer - 1
    with TempEnv(**env_vars):
        if slicefile.startswith("/vsicurl/"):
            slicefile = slicefile[9:]
        try:
            ds = rio.open(slicefile)  # type: rio.DatasetReader
        except RasterioIOError as e:
            raise FileNotFoundError(e.args)
        try:
            layer_scale = ds.scales[layer_i]
        except LookupError:
            layer_scale = None
        try:
            layer_offset = ds.offsets[layer_i]
        except LookupError:
            layer_offset = None
        try:
            masked, affine = mask(ds, [mask_shape], crop=True, filled=True, indexes=[layer])
        except Exception as e:
            print(e)
            raise
        subset_data = next(iter(masked))
        ds.close()
    # This allows other threads or processes to get a CPU time slot, if they are waiting
    time.sleep(0)
    if scale_offset is None or scale_offset == "none":
        pass
    elif scale_offset == "auto":
        if layer_scale is not None:
            fscale = float(layer_scale)
            if fscale == 1.0:
                pass
            else:
                subset_data = subset_data * fscale
        if layer_offset is not None:
            foffset = float(layer_offset)
            if foffset == 0.0:
                pass
            else:
                subset_data = subset_data + foffset
    elif callable(scale_offset):
        subset_data = scale_offset(subset_data)
    mi, ma, mean, valid_percent, std_dev = minmaxmean(subset_data, ds.nodata)
    # Free my memory!!
    del subset_data
    del masked
    del ds
    gc.collect()
    return mi, ma, mean, valid_percent, std_dev


def make_centroids(ds_name, filename: str, env_vars={}):

    driver = ogr.GetDriverByName('ESRI Shapefile')
    if driver is None:
        raise RuntimeError("{} driver not available.\n".format(driver))
    try:
        os.unlink(f'./{ds_name}_centroids2.shp')
    except FileNotFoundError:
        pass


    #shape_data = driver.CreateDataSource(f'./{ds_name}_centroids1.shp')
    srs = osr.SpatialReference()

    with TempEnv(**env_vars):
        ds = rio.open(filename)
        if not ds.transform.is_rectilinear or not ds.transform.is_conformal:
            raise RuntimeError(f"Cannot turn raster into centroids: {filename}")
        (x_count, y_count) = (ds.width, ds.height)
        (x_offset, x_step, y_offset, y_step) = (ds.transform.xoff, ds.transform.a, ds.transform.yoff, ds.transform.e)

        srs.ImportFromWkt(ds.crs.wkt)
        # This evaluation makes x/y arrays for all cells having the fixed value, 1000.
        # (y_index, x_index) = np.nonzero(a == 1000)

        # DEBUG: take a look at the arrays..
        # print repr((y_index, x_index))


        #layer = shape_data.CreateLayer(f'{ds_name}_pts', srs, ogr.wkbPoint)
        #layer_definition = layer.GetLayerDefn()
        points = []
        c = 0
        for i in range(x_count):
            for j in range(y_count):
                longitude = x_offset + (i * x_step) + (x_step / 2.0)
                latitude = y_offset + (j * y_step) + (y_step / 2.0)
                points.append(Point(longitude, latitude))
                #point = ogr.Geometry(ogr.wkbPoint)
                #point.SetPoint(0, longitude, latitude)
                #feature = ogr.Feature(layer_definition)
                #feature.SetGeometry(point)
                #feature.SetFID(c)
                #layer.CreateFeature(feature)
                c += 1
        my_multipoint = MultiPoint(points=points)
    with open(f"{ds_name}_centroids2.bin", "wb") as f:
        f.write(dump_wkb(my_multipoint, srid=4326))


class Drill(AsyncProcess):
    SUCCESS_MESSAGE = 'finished drill operation'
    allowed_returns = ["auto", "terria", "csv", "link"]

    def __init__(self, ppe: Union[ProcessPoolExecutor, ThreadPoolExecutor, None] = None, datasets_config: dict = {}):
        allowed_datasets = []
        for ds, ds_layers in datasets_config["dataset_layers"].items():
            for l_name in ds_layers:
                allowed_datasets.append("{}:{}".format(str(ds), str(l_name)))
        inputs = [
            LiteralInput('datasetId',
                         'dataset identifier',
                         min_occurs=1,
                         allowed_values=allowed_datasets,
                         default="smips:totalbucket"),
            LiteralInput('startDate',
                         'Date of start of timeseries',
                         data_type='date',
                         min_occurs=1,
                         allowed_values=AnyValue,
                         default="1970-01-01"),
            LiteralInput('endDate',
                         'Date of end of timeseries',
                         data_type='date',
                         min_occurs=1,
                         allowed_values=AnyValue,
                         default="19-01-2038"),
            LiteralInput('returnType',
                         'Optional, specify function return type',
                         min_occurs=0,
                         allowed_values=self.allowed_returns,
                         default="auto"),
            ComplexInput("point", "Point", [GEOJSON_POINT])
        ]
        outputs = [
            LiteralOutput("download_link", "CSV Download Link", data_type="string"),
            ComplexOutput("csv", "CSV Results", [TJS_JSON, OUT_CSV])
        ]

        super(Drill, self).__init__(
            self._handler,
            identifier='temporalDrill',
            version='None',
            title='Temporal Drill',
            abstract="Get timeseries values from a single point over a given time",
            profile='',
            metadata=[Metadata('drill'), Metadata('timeseries'), Metadata('point')],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True
        )
        if ppe is None:
            #self.ppe = ProcessPoolExecutor(32)
            self.ppe = ThreadPoolExecutor(max_workers=os.cpu_count())
        else:
            if not isinstance(ppe, ProxyType):
                self.ppe = proxy(ppe)
            else:
                self.ppe = ppe
        self.datasets_config = datasets_config
        self.debugging = is_debugging()

    def __deepcopy__(self, memodict={}):
        ppe_ref = self.ppe
        self.ppe = None
        self.handler = None
        ourcopy = deepcopy(super(Drill, self), memodict)
        self.ppe = ppe_ref
        self.handler = self._handler
        ourcopy.ppe = ppe_ref
        ourcopy.handler = ourcopy._handler
        return ourcopy

    async def _handler(self, request: WPSRequest, response: ExecuteResponse):
        try:
            app = self.service.app
            ctx = app.ctx.mywps
            has_ctx = True
        except (AttributeError, LookupError):
            #raise RuntimeError("Cannot get reference to App in Async Process.")
            has_ctx = False
        started = time.perf_counter()
        response._update_status(WPS_STATUS.STARTED, 'Landscapes WPS Process started', 0)
        dataset_id_lit: LiteralInput = request.inputs['datasetId'][0]
        dataset_id: str = dataset_id_lit.data
        if ":" in dataset_id:
            dataset_id, layer_id = dataset_id.split(":", 1)
        else:
            layer_id = None

        startdate_lit: LiteralInput = request.inputs['startDate'][0]
        startdate: datetime.datetime = startdate_lit.data

        enddate_lit: LiteralInput = request.inputs['endDate'][0]
        enddate: datetime.datetime = enddate_lit.data

        try:
            ret_type_lit: LiteralInput = request.inputs['returnType'][0]
            ret_type = ret_type_lit.data
        except LookupError:
            ret_type = 'auto'

        point: ComplexInput = request.inputs['point'][0]
        try:
            point_data = ujson_loads(point.data)
        except (ValueError, TypeError):
            raise
        try:
            crs = None
            if point_data['type'].lower() == "featurecollection":
                # Get the first point from featurecollection
                crs = point_data.get('crs', crs)
                point_data = point_data['features'][0]
            if point_data['type'].lower() == "feature":
                crs = point_data.get('crs', crs)
                point_data = point_data['geometry']
            if point_data['type'].lower() == "geometrycollection":
                crs = point_data.get('crs', crs)
                point_data = point_data['geometries'][0]

            if point_data['type'].lower() == "multipoint":
                crs = point_data.get('crs', crs)
                # Just get the first point from the collection
                x, y = point_data['coordinates'][0][:2]
                polygon_dict = None
            elif point_data['type'].lower() == "multipolygon":
                crs = point_data.get('crs', crs)
                # Just get the first point from the collection
                x, y = None, None
                polygon_dict = {"type": "Polygon", "coordinates": point_data['coordinates'][0]}
            elif point_data['type'].lower() == "point":
                x, y = point_data['coordinates'][:2]  # Longitude, Latitude, Altitude
                polygon_dict = None
                if crs is None:
                    # All Geojson is assumed EPSG:4326 unless it has a CRS
                    # Note, CRS is no longer part of the GeoJSON standard
                    crs = point_data.get("crs", "epsg:4326")
            elif point_data['type'].lower() == "polygon":
                x, y = None, None
                polygon_dict = point_data
                if crs is None:
                    # All Geojson is assumed EPSG:4326 unless it has a CRS
                    # Note, CRS is no longer part of the GeoJSON standard
                    crs = point_data.get("crs", "epsg:4326")
            else:
                raise InvalidParameterValue("point")
        except LookupError:
            raise InvalidParameterValue("point")

        operate_individually = False
        if polygon_dict is not None:
            polygon_shape = shape(polygon_dict)
            # Get area in square degrees
            area_sq_degrees = polygon_shape.area
            try:
                estimate_size_ratio = self.datasets_config["estimate_size_ratio"][f"{dataset_id}:{layer_id}"]
            except LookupError:
                estimate_size_ratio = 20
            estimate_size = area_sq_degrees * estimate_size_ratio
            if estimate_size >= MAX_JOB_MEMORY_MB:
                # response.status = WPS_STATUS.FAILED
                # response.update_status("Resulting data size will be too large for this server to process.",
                #                        status_percentage=100)
                # return response
                operate_individually = True
        else:
            polygon_shape = None
            estimate_size = 0.1

        if isinstance(startdate, datetime.date):
            startdate = datetime.datetime(startdate.year, startdate.month, startdate.day,
                                          tzinfo=datetime.timezone.utc)
        if isinstance(enddate, datetime.date):
            enddate = datetime.datetime(enddate.year, enddate.month, enddate.day,
                                        hour=23, minute=59, second=59, tzinfo=datetime.timezone.utc)

        if startdate.tzinfo is None:
            startdate = startdate.replace(tzinfo=datetime.timezone.utc)
        if enddate.tzinfo is None:
            enddate = enddate.replace(tzinfo=datetime.timezone.utc)

        output_as_ref = False
        if "download_link" in request.outputs:
            output_type = "link_literal"
            output_as_ref = request.outputs["download_link"].get("asReference", "") in TRUTHS
            if output_as_ref:
                response.status = WPS_STATUS.FAILED
                response.update_status("Literal response cannot be asRef.", status_percentage=100)
                return response
            mimetype = request.outputs["download_link"].get("mimetype", "text/csv")
        elif "csv" in request.outputs:
            output_type = "csv_file"
            output_as_ref = request.outputs["csv"].get("asReference", "") in TRUTHS
            mimetype = request.outputs["csv"].get("mimetype", "text/csv")
            if mimetype == "text/csv":
                output_type = "csv_file"
            else:
                # Assume mimetype is "application/vnd.terriajs.catalog-member+json"
                # but of course TerriaJS doesn't set this option on the request
                output_type = "tjs"
        else:
            if ret_type in self.allowed_returns:
                if ret_type == "csv":
                    output_type = "csv_file"
                    mimetype = "text/csv"
                elif ret_type == "link":
                    output_type = "link_literal"
                    mimetype = "text/csv"
                elif ret_type == "terria":
                    output_type = "tjs"
                    mimetype = "application/vnd.terriajs.catalog-member+json"
                else:
                    # TODO: Determine how to detect a TerriaJS request automatically
                    output_type = "link_literal"
                    mimetype = "text/csv"
            else:
                response.status = WPS_STATUS.FAILED
                response.update_status("Cannot determine output type", status_percentage=100)
                return response

        index_loc = self.datasets_config["index_locations"].get(dataset_id, None)
        if not index_loc:
            response.update_status("", status_percentage=100)
            return response
        if index_loc.startswith("/vsiswift"):
            if has_ctx:
                env_vars = {
                    "SWIFT_STORAGE_URL": ctx.nectar_objectstore_url,
                    "SWIFT_AUTH_TOKEN": ctx.nectar_keystone_token
                }
            else:
                env_vars = {
                    "SWIFT_STORAGE_URL": os.getenv("SWIFT_STORAGE_URL",""),
                    "SWIFT_AUTH_TOKEN": os.getenv("SWIFT_AUTH_TOKEN","")
                }
        else:
            env_vars = {}
        driver_name = "ESRI Shapefile"
        with TempEnv(**env_vars):
            # All of this needs to be atomic, to preserve the env files.
            # no thread swapping or async operations
            drv = ogr.GetDriverByName(driver_name)
            if drv is None:
                raise RuntimeError("{} driver not available.\n".format(driver_name))
            if index_loc.startswith("https") or index_loc.startswith("http"):
                ds: DataSource = drv.Open(f"/vsicurl/{index_loc}")
            else:
                ds = drv.Open(index_loc)
            if not ds:
                del drv
                raise LookupError(f"Cannot open index file for dataset {dataset_id}")
            layers = ds.GetLayerCount()
            if layers < 1 or layers > 1:
                if not self.debugging:
                    del ds
                del drv
                raise LookupError(f"Wrong number of layers in index for {dataset_id}")
            layer = next(iter(ds))
            to_drill = []
            gc.disable()
            for feature in layer:  # type: osgeo.ogr.Feature
                if layer_id is not None:
                    lay = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("layer")))
                    if lay.lower() != layer_id.lower():
                        del feature
                        continue
                datestr = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("date")))
                dateat = parse_datetime(datestr)
                if not(startdate <= dateat <= enddate):
                    del feature
                    continue
                loc = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("location")))
                to_drill.append((dateat, loc))
                del feature
                #gc.collect()
            len_drills = len(to_drill)
            if not self.debugging:
                del layer
                del ds
            del drv
            gc.collect()
            gc.enable()

        if len_drills < 1:
            response.status = WPS_STATUS.FAILED
            response.update_status("No slices found in that time period.", status_percentage=100)
            return response

        # After a potentially long index lookup, hand back to sanic to handle other requests waiting
        await asyncio.sleep(0)

        if MODE == "multiprocessing":
            estimate_size = estimate_size * min(len_drills, self.ppe._max_workers)
            if estimate_size >= MAX_JOB_MEMORY_MB:
                operate_individually = True
                # response.status = WPS_STATUS.FAILED
                # response.update_status("Resulting data size will be too large for this server to process.",
                #                        status_percentage=100)
                # return response
        response.update_status(f"Found {len(to_drill)} slices to drill through.", status_percentage=0)
        if polygon_shape:
            operation_fn = masked_means_slice
            operation_args = tuple()
            operation_kwargs = {"layer": 1, "mask_shape": polygon_shape, "scale_offset": "auto", "env_vars": env_vars}
        else:
            operation_fn = drill_slice
            operation_args = tuple()
            # TODO: work out how to turn layer_id into band number (0)
            operation_kwargs = {"layer": 1, "x":x, "y": y, "scale_offset": "auto", "env_vars":env_vars}
        tasks_dates = {}
        tasks = []
        if MODE == "dask":
            delays = []
            delays_dates = {}
            #c = Client(address=cluster, asynchronous=True)
            c = dask_client
            for (d_date, d_loc) in to_drill:
                slice_loc = urljoin(index_loc, d_loc)
                d = delayed(operation_fn)(slice_loc, *operation_args, **operation_kwargs)
                delays.append(d)
                delays_dates[d.key] = d_date
            futures = c.compute(delays, traverse=False)
            for f in futures:
                d = delays_dates[f.key]
                g = c.gather((f,))
                t = asyncio.create_task(g)
                tasks.append(t)
                tasks_dates[t] = d
        elif MODE == "multiprocessing" and not operate_individually:
            loop = asyncio.get_event_loop()
            for (d_date, d_loc) in to_drill:
                slice_loc = urljoin(index_loc, d_loc)
                parameterised_op = partial(operation_fn, slice_loc, *operation_args, **operation_kwargs)
                a = loop.run_in_executor(self.ppe, parameterised_op)
                tasks_dates[a] = d_date
                tasks.append(a)
        else:
            single_executor = ThreadPoolExecutor(1)
            async def dummy_async(slice_loc, *args):
                nonlocal operation_fn
                nonlocal operation_args
                nonlocal operation_kwargs
                parameterised_op = partial(operation_fn, slice_loc, *operation_args, **operation_kwargs)
                _loop = asyncio.get_event_loop()
                res = await _loop.run_in_executor(single_executor, parameterised_op, *args)
                return res

            loop = asyncio.get_event_loop()
            for (d_date, d_loc) in to_drill:
                slice_loc = urljoin(index_loc, d_loc)
                a = loop.create_task(dummy_async(slice_loc))
                tasks_dates[a] = d_date
                tasks.append(a)
        all_done = []
        task_count = len(tasks)
        finished, pending = await asyncio.wait(tasks, return_when=FIRST_COMPLETED)
        all_done.extend((tasks_dates[f], f.result()) for f in finished)
        percentage = (len(all_done)*100) // task_count
        if percentage > 1:
            response.update_status(f"Completed {len(all_done)} lookups", percentage)
        while len(pending):
            #print(len(all_done), len(pending), flush=True)
            finished, pending = await asyncio.wait(pending, return_when=FIRST_COMPLETED)
            all_done.extend((tasks_dates[f], f.result()) for f in finished)
            new_percentage = (len(all_done) * 100) // task_count
            if new_percentage > percentage:
                response.update_status(f"Completed {len(all_done)} lookups", new_percentage)
                percentage = new_percentage
        finished = time.perf_counter()
        elapsed = finished - started
        print("Elapsed: {}".format(elapsed))
        date_col = "time" if output_type == "tjs" else "date"
        if polygon_shape:
            csvString = f"{date_col},min_{layer_id},max_{layer_id},mean_{layer_id},valid_percent,stddev\n"

            for (dt, val) in all_done:
                csvString += "".join((dt.isoformat(), ',', str(val[0]), ',', str(val[1]), ',', str(val[2]), ',',
                                      str(val[3]), ',', str(val[4]), '\n'))
        else:
            csvString = f"{date_col},{layer_id}\n"
            for (dt, val) in all_done:
                csvString += "".join((dt.isoformat(), ',', str(val), '\n'))
        if output_type == "link_literal":
            new_filename = "{}.csv".format(str(time.time()))
            filepath = "outputs/{}".format(new_filename)
            with open(filepath, "w") as f:
                f.write(csvString)
            output_url = str(self.status_url).rsplit("/", 1)[0]
            link = "/".join((output_url, new_filename))
            response.outputs['download_link'].data = link
            del response.outputs['csv']
        elif output_type == "csv_file":
            response.outputs['csv'].data = csvString
            if output_as_ref and request.store_execute:
                response.outputs['csv'].as_reference = True
                set_output_filename_for_complexoutput(response.outputs['csv'], "drill.csv")
            response.outputs['csv'].mimetype = "text/csv"
            response.outputs['csv'].schema = ""
            response.outputs['csv'].data_format = OUT_CSV
            del response.outputs['download_link']
        elif output_type == "tjs":
            resp_obj = ujson_dumps({
                "csvString": csvString,
                "type": "csv",
                "name": layer_id,
                "columns": [
                    {
                        "name": f"{date_col}",
                        "type": "time",
                    },
                    {
                        "name": f"{layer_id}",
                        "type": "scalar",
                    }
                ],
                "styles": [{
                    "id": "style1",
                    "chart": {
                        "xAxisColumn": f"{date_col}",
                        "lines": [{
                            "name": f"{layer_id}",
                            "yAxisColumn": f"{layer_id}",
                            "isSelectedInWorkbench": True
                        }]
                    },
                    "time": {
                        "timeColumn": f"{date_col}"
                    }
                }],
                "activeStyle": "style1"
            })
            response.outputs['csv'].data = resp_obj
            response.outputs['csv'].mimetype = "application/vnd.terriajs.catalog-member+json"
            response.outputs['csv'].schema = "https://tools.ietf.org/html/rfc7159"
            response.outputs['csv'].data_format = TJS_JSON
            del response.outputs['download_link']
        else:
            resp_obj = {
                "type": "FeatureConnection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Point",
                            "coordinates": [x, y]
                        },
                        "properties": {
                            "totalbucket": 10.0,
                            "datetime": startdate
                        }
                    }
                ]
            }
            response.outputs['csv'].data = resp_obj
            del response.outputs['download_link']
        response._update_status(WPS_STATUS.SUCCEEDED, 'Landscapes WPS Process succeeded', 100)


def main():
    datasets_config = {
        "dataset_layers": {
            "smips": ["totalbucket", "SMindex"],
            "aet": ["ETa"]
        },
        "index_locations": {
            "smips": "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-smips-public/v1_0/index.shp",
            #"smips": "/vsiswift/landscapes-csiro-smips-public/v1_0/index.shp",
            #"aet": "/vsiswift/landscapes-csiro-aet-public/v2_2/index.shp",
            "aet": "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-aet-public/v2_2/index.shp",
        },
        "estimate_size_ratio": {
            "smips:totalbucket": 0.0405,
            "smips:SMindex": 0.0405,
            "aet:ETa": 25.6,
        }
    }

    """Example of how to debug this process, running outside a PyWPS instance.
    """
    import asyncio
    #a = make_centroids("smips", "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-smips-public/v1_0/totalbucket/2016/smips_totalbucket_mm_20160101.tif")
    #b = make_centroids("aet", "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-aet-public/v2_2/2021/2021_01_01/CMRSET_LANDSAT_V2_2_2021_01_01_ETa.vrt")
    drill = Drill(ppe=None, datasets_config=datasets_config)
    request = WPSRequest()
    request.check_and_set_language(None)
    request.outputs['csv'] = {"mimetype": 'text/csv'}
    response_cls = get_response("execute")
    response = response_cls(request, process=drill, uuid="test")
    dataset_in = drill.inputs[0]
    dataset_in.data = "smips:totalbucket"
    request.inputs["datasetId"] = [dataset_in]
    startdate_in = drill.inputs[1]
    startdate_in.data = datetime.datetime(2005, 1, 9)
    request.inputs["startDate"] = [startdate_in]
    enddate_in = drill.inputs[2]
    enddate_in.data = datetime.datetime(2007, 1, 10)
    request.inputs["endDate"] = [enddate_in]
    rettype_in = drill.inputs[3]
    rettype_in.data = "csv"
    request.inputs["returnType"] = [rettype_in]
    #point_in = drill.inputs[3]
    #point_in.data = '{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Point","coordinates":[150.28024266564645,-25.843395966056466,-44801.28606662725]},"properties":{}}]}'
    point_in = drill.inputs[4]
    #point_in.data = '{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[143.07458284473583,-25.299778026434737],[145.48941912557842,-24.191150157964923],[147.0971879845574,-25.018840622094245],[146.32395844216842,-26.294764561422262],[144.92154403855236,-26.871773668571137],[143.39132901313374,-26.679774495095096],[143.07458284473583,-25.299778026434737]]]}}]}'
    point_in.data = '{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Point","coordinates":[143.07458284473583,-25.299778026434737]}}]}'
    request.inputs["point"] = [point_in]
    loop = asyncio.get_event_loop()
    loop.run_until_complete(drill._handler(request, response))
    print(response.outputs['csv'].data)
    #assert response.outputs["drill_output"].data == drill.SUCCESS_MESSAGE
    print("All good!")


if __name__ == "__main__":
    main()
