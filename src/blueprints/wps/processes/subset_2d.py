import asyncio
import datetime
import gc
import os
import tempfile
from _weakref import ProxyType
from copy import deepcopy, copy
from math import ceil
from weakref import proxy
from asyncio import FIRST_COMPLETED
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from urllib.parse import urljoin
import time
from ciso8601 import parse_datetime
from pywps import LiteralInput, LiteralOutput, ComplexInput, FORMATS, Format, ComplexOutput, WPSRequest
from pywps.app.Common import Metadata
from pywps.response.status import WPS_STATUS
from pywps.inout.literaltypes import AnyValue
from pywps.response import get_response
from pywps.response.execute import ExecuteResponse
from osgeo import ogr, gdal
from src.gdal_merge import gdal_merge
import rasterio as rio
from rasterio.mask import mask
from io import BytesIO
from distributed import Client, Security
from dask.distributed import Client
from dask import delayed
from shapely.geometry import shape
from ujson import dumps, loads
from src.blueprints.wps.processes.util import minmaxmean, TRUTHS, get_identifier_for_feature, \
    roughly_split_shape_by_area, set_output_filename_for_complexoutput

from src.utils import TempEnv, is_debugging

try:
    from ..async_process import AsyncProcess
except ImportError:
    from src.blueprints.wps.async_process import AsyncProcess


GEOJSON = Format("application/vnd.geo+json", schema="http://geojson.org/geojson-spec.html#", extension=".geojson")
GEOJSON_POINT = Format(
    "application/vnd.geo+json", schema="http://geojson.org/geojson-spec.html#point", extension=".geojson"
)
GEOJSON_POLYGON = Format(
    "application/vnd.geo+json", schema="http://geojson.org/geojson-spec.html#polygon", extension=".geojson"
)
GEOJSON_ANY = Format(
    "application/vnd.geo+json", schema="http://geojson.org/geojson-spec.html#geojson", extension=".geojson"
)
TJS_JSON = Format(
    "application/vnd.terriajs.catalog-member+json", schema="https://tools.ietf.org/html/rfc7159", extension=".json"
)
OUT_CSV = Format("text/csv", extension=".csv")
OUT_TIF = Format("image/tiff", extension=".tif")
USE_COG_DRIVER = True  # When stitching together GeoTiffs, use the GDAL_Warp COG output driver
MODE = os.getenv("WPS_PROCESSING_MODE", "multiprocessing")
MAX_JOB_OUTPUT_MB = float(os.getenv("WPS_MAX_JOB_OUTPUT_MB", "999.0"))
MAX_JOB_MEMORY_MB = float(os.getenv("WPS_MAX_JOB_MEMORY_MB", "700.0"))

if MODE == "dask":
    if __name__ == "__main__":
        sec = Security(require_encryption=False)
        dask_client = Client("tcp://140.253.176.93:8999", security=sec, asynchronous=True)
else:
    dask_client = None

ogr.UseExceptions()


def subset2D(filename: str, layer: int, input_polygon: dict, save=True, virtual=False, workdir=None, env_vars={}):
    rets = []
    if workdir is None:
        workdir = "./workdir"
    with TempEnv(**env_vars):
        if filename.startswith("/vsicurl/"):
            filename = filename[9:]
        ds = rio.open(filename)  # type: rio.DatasetReader
        masked, affine = mask(ds, [input_polygon], crop=True, filled=True, indexes=[layer])
        subset_data = masked[0]
    if not save:
        return (subset_data, affine)
    else:
        profile = copy(ds.profile)
        profile["transform"] = affine
        profile["count"] = 1
        profile["driver"] = "COG"
        profile["width"] = subset_data.shape[1]
        profile["height"] = subset_data.shape[0]
        profile["compress"] = "deflate"
        min, max, mean, valid_percent, std_dev = minmaxmean(subset_data, ds.nodata)
        if virtual:
            filepath = BytesIO()
        else:
            new_filename = "{}.cog.tif".format(str(time.time()))
            filepath = f"{workdir}/{new_filename}"
        with rio.open(filepath, "w", **profile) as dst:
            dst.write(subset_data, 1)
            dst.update_tags(**ds.tags())
            dst.update_tags(
                1,
                STATISTICS_MAXIMUM=max,
                STATISTICS_MINIMUM=min,
                STATISTICS_MEAN=mean,
                STATISTICS_VALID_PERCENT=valid_percent,
                STATISTICS_STDDEV=std_dev,
            )
        # Free my memory!!
        del subset_data
        del masked
        del ds
        gc.collect()
        return filepath


class Subset2D(AsyncProcess):
    SUCCESS_MESSAGE = "Finished 2d subset"
    allowed_returns = ["auto", "terria", "geotiff", "cog", "link"]

    def __init__(self, ppe: ProcessPoolExecutor, datasets_config: dict = {}):
        allowed_datasets = []
        for ds, ds_layers in datasets_config["dataset_layers"].items():
            for l_name in ds_layers:
                allowed_datasets.append("{}:{}".format(str(ds), str(l_name)))
        inputs = [
            LiteralInput("datasetId",
                         "dataset identifier",
                         min_occurs=1,
                         allowed_values=allowed_datasets,
                         default="smips:totalbucket"),
            LiteralInput("atDate",
                         "Date of imagery to use",
                         data_type="date",
                         min_occurs=1,
                         allowed_values=AnyValue,
                         default="2019-01-01"),
            LiteralInput('returnType',
                         'Optional, specify function return type',
                         min_occurs=0,
                         allowed_values=self.allowed_returns,
                         default="auto"),
            ComplexInput("polygon", "Polygon", [GEOJSON_ANY]),
        ]
        outputs = [
            LiteralOutput("download_link", "GeoTiff Download Link", data_type="string"),
            ComplexOutput("geotiff", "GeoTiff Data", [TJS_JSON, OUT_TIF])
        ]

        super(Subset2D, self).__init__(
            self._handler,
            identifier="subset2d",
            version="None",
            title="SubSet 2D",
            abstract="Return a GeoTiff file containing the dataset subset by the 2D given polygon.",
            profile="",
            metadata=[Metadata("slice"), Metadata("cut"), Metadata("subset"), Metadata("geotiff"), Metadata("COG")],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True,
        )
        if ppe is None:
            #self.ppe = ProcessPoolExecutor(32)
            self.ppe = ThreadPoolExecutor(max_workers=os.cpu_count())
        else:
            if not isinstance(ppe, ProxyType):
                self.ppe = proxy(ppe)
            else:
                self.ppe = ppe
        self.datasets_config = datasets_config
        self.debugging = is_debugging()

    def __deepcopy__(self, memodict={}):
        ppe_ref = self.ppe
        self.ppe = None
        self.handler = None
        ourcopy = deepcopy(super(Subset2D, self), memodict)
        self.ppe = ppe_ref
        self.handler = self._handler
        ourcopy.ppe = ppe_ref
        ourcopy.handler = ourcopy._handler
        return ourcopy

    async def _handler(self, request, response: ExecuteResponse):
        try:
            app = self.service.app
            ctx = app.ctx.mywps
            has_ctx = True
        except (AttributeError, LookupError):
            #raise RuntimeError("Cannot get reference to App in Async Process.")
            has_ctx = False
        started = time.perf_counter()
        dataset_id_lit: LiteralInput = request.inputs["datasetId"][0]
        dataset_id: str = dataset_id_lit.data
        if ":" in dataset_id:
            dataset_id, layer_id = dataset_id.split(":", 1)
        else:
            layer_id = ""

        at_date_lit: LiteralInput = request.inputs["atDate"][0]
        at_date: datetime.datetime = at_date_lit.data

        try:
            ret_type_lit: LiteralInput = request.inputs['returnType'][0]
            ret_type = ret_type_lit.data
        except LookupError:
            ret_type = 'auto'

        poly: ComplexInput = request.inputs["polygon"][0]
        try:
            poly_data = loads(poly.data)
        except (ValueError, TypeError):
            raise
        polys = []
        try:
            crs = None
            features = []
            p_type = poly_data['type'].lower()
            if p_type == "featurecollection":
                # Get the first feature from featurecollection
                crs = poly_data.get('crs', crs)
                _features = poly_data.get('features', [])
                for (_ix, f) in enumerate(_features):
                    if 'geometry' in f and f['geometry'] is not None:
                        _id = get_identifier_for_feature(f, _ix)
                        features.append((_id, f['geometry']))
            elif p_type == "geometrycollection":
                # Get all geometries from geometrtycollection
                crs = poly_data.get('crs', crs)
                _geoms = poly_data.get('geometries', [])
                for (_ix, g) in enumerate(_geoms):
                    _id = get_identifier_for_feature(g, _ix)
                    features.append((_id, g))
            elif p_type == "feature":
                crs = poly_data.get('crs', crs)
                if 'geometry' in poly_data and poly_data['geometry'] is not None:
                    _id = get_identifier_for_feature(poly_data, 0)
                    features.append((_id, poly_data['geometry']))
            elif p_type == "polygon":
                _id = get_identifier_for_feature(poly_data, 0)
                polys.append((_id, poly_data))
            elif p_type == "multipolygon":
                _id = get_identifier_for_feature(poly_data, 0)
                polys.append((_id, poly_data))
            elif p_type == "point":
                # Weird, but we can do it. Need to get rid of Z
                poly_data['coordinates'] = poly_data['coordinates'][:2]
                _id = get_identifier_for_feature(poly_data, 0)
                polys.append((_id, poly_data))
            for (_id, f) in features:
                if f['type'].lower() == "polygon":
                    polys.append((_id, f))
                elif f['type'].lower() == "multipolygon":
                    polys.append((_id, f))
                elif f["type"].lower() == "point":
                    # Need to get rid of Z
                    f['coordinates'] = f['coordinates'][:2]
                    polys.append((_id, f))
            if crs is None:
                # All Geojson is assumed EPSG:4326 unless it has a CRS
                # Note, CRS is no longer part of the GeoJSON standard
                crs = poly_data.get("crs", "epsg:4326")
        except LookupError:
            raise

        try:
            estimate_size_ratio = self.datasets_config["estimate_size_ratio"][f"{dataset_id}:{layer_id}"]
        except LookupError:
            estimate_size_ratio = 20
        poly_shapes = []
        total_sq_degrees = 0
        for (_id, p) in polys:
            in_shape = shape(p)
            # Get area in square degrees
            total_sq_degrees += in_shape.area
            poly_shapes.append((_id, in_shape))
        estimate_size = total_sq_degrees * estimate_size_ratio

        if estimate_size >= MAX_JOB_OUTPUT_MB:
            response.status = WPS_STATUS.FAILED
            response.update_status("Geometry area results in dataset subset that is too large to store on this server.", status_percentage=100)
            return response
        # This flag is set if data for the whole poly area can't fit into the allowed memory.
        # When it is set, we operate on each feature without multiprocessing
        operate_individually = False
        if estimate_size >= MAX_JOB_MEMORY_MB:
            operate_individually = True

        if isinstance(at_date, datetime.date):
            startdate = datetime.datetime(at_date.year, at_date.month, at_date.day, tzinfo=datetime.timezone.utc)

        if startdate.tzinfo is None:
            startdate = startdate.replace(tzinfo=datetime.timezone.utc)

        output_as_ref = False
        if "download_link" in request.outputs:
            output_type = "link_literal"
            output_as_ref = request.outputs["download_link"].get("asReference", "") in TRUTHS
            if output_as_ref:
                response.status = WPS_STATUS.FAILED
                response.update_status("Literal response cannot be asRef.", status_percentage=100)
                return response
            mimetype = request.outputs["download_link"].get("mimetype", "image/tiff")
        elif "geotiff" in request.outputs:
            output_type = "cog_file"
            output_as_ref = request.outputs["geotiff"].get("asReference", "") in TRUTHS
            mimetype = request.outputs["geotiff"].get("mimetype", "image/tiff")
        else:
            if ret_type in self.allowed_returns:
                if ret_type == "geotiff" or ret_type == "cog":
                    output_type = "cog_file"
                    mimetype = "image/tiff"
                elif ret_type == "link":
                    output_type = "link_literal"
                    mimetype = "image/tiff"
                elif ret_type == "terria":
                    output_type = "tjs"
                    mimetype = "application/vnd.terriajs.catalog-member+json"
                else:
                    # TODO: Determine how to detect a TerriaJS request automatically
                    output_type = "link_literal"
                    mimetype = "image/tiff"
            else:
                response.status = WPS_STATUS.FAILED
                response.update_status("Cannot determine output type", status_percentage=100)
                return response

        index_loc = self.datasets_config["index_locations"].get(dataset_id, None)
        if not index_loc:
            response.status = WPS_STATUS.FAILED
            response.update_status("", status_percentage=100)
            return response
            return response
        if index_loc.startswith("/vsiswift"):
            if has_ctx:
                env_vars = {
                    "SWIFT_STORAGE_URL": ctx.nectar_objectstore_url,
                    "SWIFT_AUTH_TOKEN": ctx.nectar_keystone_token
                }
            else:
                env_vars = {
                    "SWIFT_STORAGE_URL": os.getenv("SWIFT_STORAGE_URL", ""),
                    "SWIFT_AUTH_TOKEN": os.getenv("SWIFT_AUTH_TOKEN", "")
                }
        else:
            env_vars = {}
        driver_name = "ESRI Shapefile"
        with TempEnv(**env_vars):
            # All of this needs to be atomic, to preserve the temp env vars.
            # no thread swapping or async operations
            drv = ogr.GetDriverByName(driver_name)
            if drv is None:
                raise RuntimeError("{} driver not available.\n".format(driver_name))
            if index_loc.startswith("https") or index_loc.startswith("http"):
                ds = drv.Open(f"/vsicurl/{index_loc}")
            else:
                ds = drv.Open(index_loc)
            if not ds:
                del drv
                raise LookupError(f"Cannot open index file for dataset {dataset_id}")
            layers = ds.GetLayerCount()
            if layers < 1 or layers > 1:
                if not self.debugging:
                    del ds
                del drv
                raise LookupError(f"Wrong number of layers in index for {dataset_id}")
            layer = next(iter(ds))
            to_get = []
            gc.disable()
            for feature in layer:  # type: osgeo.ogr.Feature
                if layer_id is not None and len(layer_id) > 0:
                    lay = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("layer")))
                    if lay.lower() != layer_id.lower():
                        del feature
                        continue
                datestr = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("date")))
                dateat = parse_datetime(datestr)
                if dateat < startdate:
                    del feature
                    continue
                loc = "".join(chr(_o) for _o in map(ord, feature.GetFieldAsString("location")))
                to_get.append((dateat, loc))
                del feature
                break  # just get the first one
            len_gets = len(to_get)
            if not self.debugging:
                del layer
                del ds
            del drv
            gc.collect()
            gc.enable()

        if len_gets < 1:
            response.status = WPS_STATUS.FAILED
            response.update_status("No slices found in that time period.", status_percentage=100)
            return response

        # After a potentially long index lookup, hand back to sanic to handle other requests waiting
        await asyncio.sleep(0)
        response.update_status(f"Found time slice to cut portion.", status_percentage=0)
        tasks_meta = {}
        tasks = []
        do_save = output_type in ("link_literal", "cog_file", "tjs")
        #do_virtual = output_type in ("cog_file",) and not output_as_ref
        do_virtual = False
        cog_workdir = self.workdir
        if MODE == "dask" and not operate_individually:
            delays = []
            delays_dates = {}
            delays_polyids = {}
            # c = Client(address=cluster, asynchronous=True)
            c = dask_client
            for poly_id, poly_shape in poly_shapes:
                for (d_date, d_loc) in to_get:
                    slice_loc = urljoin(index_loc, d_loc)
                    d = delayed(subset2D)(slice_loc, 1, poly_shape, do_save, do_virtual, cog_workdir, env_vars)
                    delays.append(d)
                    delays_dates[d.key] = d_date
                    delays_polyids[d.key] = poly_id
                    break
            futures = c.compute(delays, traverse=False)
            for f in futures:
                d = delays_dates[f.key]
                poly_id = delays_polyids[f.key]
                g = c.gather((f,))
                t = asyncio.create_task(g)
                tasks.append(t)
                tasks_meta[t] = {"date": d, "poly": poly_id}
        elif MODE == "multiprocessing" and not operate_individually:
            loop = asyncio.get_event_loop()
            for poly_id, poly_shape in poly_shapes:
                for (d_date, d_loc) in to_get:
                    slice_loc = urljoin(index_loc, d_loc)
                    # TODO: work out how to turn layer_id into band number (1)
                    a = loop.run_in_executor(
                        self.ppe, subset2D, slice_loc, 1, poly_shape, do_save, do_virtual, cog_workdir, env_vars
                    )
                    tasks_meta[a] = {"date": d_date, "poly": poly_id}
                    tasks.append(a)
                    break
            del poly_shape
        else:
            single_executor = ThreadPoolExecutor(1)

            async def dummy_subset2D_async(*args, **kwargs):
                _loop = asyncio.get_event_loop()
                res = await _loop.run_in_executor(single_executor, subset2D, *args, **kwargs)
                return res

            if len(poly_shapes) > 1:
                response.update_status(
                    "Geometry area is too large to fit in system memory. Cannot run the job in parallel, falling back to serialized execution. This operation will take longer than usual to complete.",
                    status_percentage=0)

            loop = asyncio.get_event_loop()
            for poly_id, poly_shape in poly_shapes:
                estimate_mem = (poly_shape.area * estimate_size_ratio)
                if estimate_mem >= MAX_JOB_MEMORY_MB:
                    response.update_status(
                        "Geometry area is too large to fit in system memory. Chunking geometry to save memory. This operation will take much longer than usual to complete, and may fail.",
                        status_percentage=0)
                    splits_required = ceil(estimate_mem / MAX_JOB_MEMORY_MB)
                    # splits_required = 4
                    if splits_required > 8:
                        response.status = WPS_STATUS.FAILED
                        response.update_status(
                            "Geometry area results in dataset subset that cannot fit in the server's memory.",
                            status_percentage=100)
                        return response
                else:
                    splits_required = 0

                for (d_date, d_loc) in to_get:
                    slice_loc = urljoin(index_loc, d_loc)
                    if splits_required > 0:
                        try:
                            splits = roughly_split_shape_by_area(poly_shape, splits_required)
                            for split_id, (split_poly, split_weight) in enumerate(splits):
                                a = loop.create_task(
                                    dummy_subset2D_async(slice_loc, 1, split_poly, do_save, do_virtual, cog_workdir, env_vars)
                                )
                                tasks_meta[a] = {"date": d_date, "poly": poly_id, "split": True,
                                                 "split_id": split_id, "split_weight": split_weight}
                                print(f"{poly_id}_{split_id}", flush=True)
                                tasks.append(a)
                            del split_poly
                        except Exception as e:
                            print(e)
                    else:
                        a = loop.create_task(
                            dummy_subset2D_async(slice_loc, 1, poly_shape, do_save, do_virtual, cog_workdir, env_vars)
                        )
                        tasks_meta[a] = {"date": d_date, "poly": poly_id, "split": False}
                        print(f"{poly_id}", flush=True)
                        tasks.append(a)
                    break
            del poly_shape
        del poly_shapes
        all_done = []
        task_count = len(tasks)
        gc.collect()
        finished, pending = await asyncio.wait(tasks, return_when=FIRST_COMPLETED)
        all_done.extend((tasks_meta[f], f.result()) for f in finished)
        percentage = (len(all_done)*100)//task_count
        if percentage > 1:
            response.update_status(f"Completed {len(all_done)} lookups", min(percentage, 99))
        while len(pending):
            finished, pending = await asyncio.wait(pending, return_when=FIRST_COMPLETED)
            all_done.extend((tasks_meta[f], f.result()) for f in finished)
            new_percentage = (len(all_done) * 100) // task_count
            if new_percentage > percentage:
                response.update_status(f"Completed {len(all_done)} lookups", min(new_percentage, 99))
                percentage = new_percentage
        all_done = sorted(all_done, key=lambda x: x[0]['poly'])
        if len(all_done) > 1:
            response.update_status(f"Stitching {len(all_done)} files into one raster output.", 99)
            out_file = f"{self.workdir}/stitched.cog.tif"
            merge_filenames = []
            for result_meta, result in all_done:
                merge_filenames.append(result)
            if USE_COG_DRIVER:
                warp_options = gdal.WarpOptions(format="COG", multithread=True, warpOptions=["NUM_THREADS=4"],
                                                creationOptions=["OVERVIEWS=AUTO", "BLOCKSIZE=512", "COMPRESS=DEFLATE"])
            else:
                warp_options = gdal.WarpOptions(format="GTiff", multithread=True, warpOptions=["NUM_THREADS=4"],
                                                creationOptions=["COPY_SRC_OVERVIEWS=YES", "TILED=TRUE",
                                                               "BLOCKXSIZE=512", "BLOCKYSIZE=512", "COMPRESS=DEFLATE"])
            gdal.Warp(out_file, merge_filenames, options=warp_options)
            #gdal_merge(argv=["-v", "-o", out_file, "-f", "GTiff", "-co", "COPY_SRC_OVERVIEWS=YES", "-co", "TILED=YES", "-co", "BLOCKXSIZE=512", "-co", "BLOCKYSIZE=512", "-co", "COMPRESS=DEFLATE", *merge_filenames])
        else:
            response.update_status(f"Completed {len(all_done)} lookups", 100)
            result_meta, result = all_done[0]
            out_file = result
        finished = time.perf_counter()
        elapsed = finished - started
        print("Elapsed: {}".format(elapsed), flush=True)
        response.outputs['geotiff'].file = out_file
        if output_type in ("link_literal",):
            response.outputs['geotiff'].as_reference = True
            # This generates the output file, copies it to the outputs dir, and makes a URL for it
            resp_parts = response.outputs['geotiff'].json
            response.outputs['download_link'].data = resp_parts['href']
            del response.outputs['geotiff']
        elif output_type in ("cog_file",):
            if output_as_ref and request.store_execute:
                response.outputs['geotiff'].as_reference = True
            response.outputs['geotiff'].mimetype = "image/tiff"
            response.outputs['geotiff'].schema = ""
            response.outputs['geotiff'].data_format = OUT_TIF
            del response.outputs["download_link"]
        elif output_type == "tjs":
            response.outputs['geotiff'].as_reference = True
            # This generates the output file, copies it to the outputs dir, and makes a URL for it
            resp_parts = response.outputs['geotiff'].json
            resp_obj = dumps({
                "url": resp_parts['href'],
                "type": "url-reference",
                "name": f"Subset of {layer_id}",
                "isMappable": True,
                "isOpenInWorkbench": True
            })
            response.outputs['geotiff'].as_reference = False
            response.outputs['geotiff'].data = resp_obj
            response.outputs['geotiff'].mimetype = "application/vnd.terriajs.catalog-member+json"
            response.outputs['geotiff'].schema = "https://tools.ietf.org/html/rfc7159"
            response.outputs['geotiff'].data_format = TJS_JSON
            del response.outputs['download_link']
        else:
            dt, (subset, affine) = all_done[0]
            data = subset
            resp_obj = {
                "type": "FeatureConnection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {"type": "Point", "coordinates": [1, 2]},
                        "properties": {"totalbucket": 10.0, "datetime": startdate},
                    }
                ],
            }
            response.outputs["results"].data = dumps(resp_obj)
        response._update_status(WPS_STATUS.SUCCEEDED, 'Landscapes WPS Process succeeded', 100)


def main():
    """Example of how to debug this process, running outside a PyWPS instance.
    """
    datasets_config = {
        "dataset_layers": {
            "smips": ["totalbucket", "SMindex"],
            "aet": ["ETa"]
        },
        "index_locations": {
            #"smips": "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-smips-public/v1_0/index.shp",
            "smips": "/vsiswift/landscapes-csiro-smips-public/v1_0/index.shp",
            "aet": "/vsiswift/landscapes-csiro-aet-public/v2_2/index.shp",
            #"aet": "https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/landscapes-csiro-aet-public/v2_2/index.shp",
        },
        "estimate_size_ratio": {
            "smips:totalbucket": 0.0405,
            "smips:SMindex": 0.0405,
            "aet:ETa": 25.6,
        }
    }
    import asyncio
    import shapefile
    import json
    subset_op = Subset2D(ppe=None, datasets_config=datasets_config)
    request = WPSRequest()
    request.check_and_set_language(None)
    workdir = os.path.abspath('./../../../../workdir')
    tempdir = tempfile.mkdtemp(prefix='pywps_process_', dir=workdir)
    subset_op.set_workdir(tempdir)
    request.store_execute = True
    #request.outputs['download_link'] = {"mimetype": 'image/tiff', 'asReference': "false"}
    request.outputs['geotiff'] = {"mimetype": 'image/tiff', 'asReference': "true"}
    response_cls = get_response("execute")
    response = response_cls(request, process=subset_op, uuid="test")
    dataset_in = subset_op.inputs[0]
    dataset_in.data = "aet:ETa"
    #dataset_in.data = "smips:totalbucket"
    request.inputs["datasetId"] = [dataset_in]
    atdate_in = subset_op.inputs[1]
    atdate_in.data = datetime.datetime(2016,1,1)
    request.inputs["atDate"] = [atdate_in]
    rettype_in = subset_op.inputs[2]
    rettype_in.data = "auto"
    request.inputs["returnType"] = [rettype_in]
    poly_in = subset_op.inputs[3]
    r = shapefile.Reader("../../../../states/STE_2021_AUST_GDA94")
    shp_type = str(r.__geo_interface__['type']).lower()
    poly_in.data = json.dumps(r.__geo_interface__)
    #poly_in.data = '{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[143.07458284473583,-25.299778026434737],[145.48941912557842,-24.191150157964923],[147.0971879845574,-25.018840622094245],[146.32395844216842,-26.294764561422262],[144.92154403855236,-26.871773668571137],[143.39132901313374,-26.679774495095096],[143.07458284473583,-25.299778026434737]]]}}]}'
    request.inputs["polygon"] = [poly_in]
    loop = asyncio.get_event_loop()
    loop.run_until_complete(subset_op._handler(request, response))
#    print(response.outputs['geotiff'].data)
    doc = response._construct_doc()
    print(doc)


if __name__ == "__main__":
    main()
