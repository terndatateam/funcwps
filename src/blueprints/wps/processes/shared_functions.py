import gc
import time
import rasterio as rio
from rasterio import RasterioIOError
from rasterio.mask import mask
from src.blueprints.wps.processes.util import minmaxmean
from src.utils import TempEnv, is_debugging


def stats2D(jobs: list, scale_offset="none", env_vars: dict = {}):
    resp_list = []
    with TempEnv(**env_vars):
        last_filename = ""
        ds = None
        for (i, filename, band, input_polygon) in jobs:
            # Layer is 1-based index of bands in the raster
            layer_i = band - 1
            if filename.startswith("/vsicurl/"):
                filename = filename[9:]
            if filename != last_filename:
                if ds:
                    try:
                        ds.close()
                    except Exception:
                        pass
                try:
                    ds = rio.open(filename)  # type: rio.DatasetReader
                except RasterioIOError as e:
                    raise FileNotFoundError(e.args)
            last_filename = filename
            try:
                layer_scale = ds.scales[layer_i]
            except LookupError:
                layer_scale = None
            try:
                layer_offset = ds.offsets[layer_i]
            except LookupError:
                layer_offset = None
            use_nodata = ds.nodata
            try:
                masked, affine = mask(ds, [input_polygon], crop=True, filled=True, indexes=[band])
            except Exception as e:
                print(e)
                raise
            subset_data = masked[0]

            if scale_offset is None or scale_offset == "none":
                pass
            elif scale_offset == "auto":
                if (layer_scale is None or layer_scale == 1.0) and (layer_offset is None or layer_offset == 0.0):
                    pass
                else:
                    # We can't scale the nodata value! Keep the nodata mask, to replace it
                    nodata_mask = subset_data == use_nodata
                    if layer_scale is not None:
                        fscale = float(layer_scale)
                        if fscale == 1.0:
                            pass
                        else:
                            subset_data = subset_data * fscale
                    if layer_offset is not None:
                        foffset = float(layer_offset)
                        if foffset == 0.0:
                            pass
                        else:
                            subset_data = subset_data + foffset
                    subset_data[nodata_mask] = use_nodata
            elif callable(scale_offset):
                subset_data = scale_offset(subset_data)

            resp_list.append((i, *minmaxmean(subset_data, use_nodata)))
            # Free my memory!!
            del subset_data
            del masked
            gc.collect()
            # This allows other threads or processes to get a CPU time slot, if they are waiting
            time.sleep(0)
        if ds:
            try:
                ds.close()
            except Exception:
                pass
    del ds
    gc.collect()
    return resp_list
