# -*- coding: utf-8 -*-
#
"""Main config file for the app, place in here things that don't go in the other config files"""
import sys
from os import getenv
from .utils import do_load_dotenv

undefined = object()
do_load_dotenv()

module = sys.modules[__name__]
# These are config defaults
defaults = module.defaults = {
    "DEBUG_FILE": "",  # Empty string is closest we can get to "No"
    "USE_SYSLOG": "",  # Empty string is closest we can get to "False"
    "USE_STDOUT": "AUTO",
    "SYSLOG_SOCKET": "/dev/log",  # might be /run/systemd/journal/syslog or dev-log if running under systemd
}
config = module.config = dict()
# get configs from env vars
config['DEBUG_FILE'] = getenv("DEBUG_FILE", undefined)
config['USE_SYSLOG'] = getenv("USE_SYSLOG", undefined)
config['USE_STDOUT'] = getenv("USE_STDOUT", undefined)
config['SYSLOG_SOCKET'] = getenv("SYSLOG_SOCKET", undefined)

# fill defaults into config if env vars are not provided
for k, v in defaults.items():
    if k not in config or config[k] is undefined:
        if v is None or v is undefined:
            raise RuntimeError(f"No default value available for config key \"{k}\"")
        config[k] = v

# write config values to module top-level, for easier import into other files
DEBUG_FILE = config['DEBUG_FILE']
USE_STDOUT = config['USE_STDOUT']
USE_SYSLOG = config['USE_SYSLOG']
SYSLOG_SOCKET = config['SYSLOG_SOCKET']
