import functools
import logging
import sys
from inspect import isawaitable
from uuid import uuid4

import sanic
from sanic.response import json_dumps
from pycadf.event import Event
from pycadf.credential import Credential
from pycadf.reason import Reason
from pycadf.resource import Resource, RESOURCE_KEYNAME_ID
from pycadf.timestamp import get_utc_now
from pycadf import cadftaxonomy, cadftype
from ._logging import init_logger

observer_id = "13d03810-7fb2-4ca5-99fb-a551b82c9e58"  # This is not a secret

class EasyResource(Resource):
    """
    Relaxed version of CADF Resource, that can take any string in its ID field
    """
    id = cadftype.ValidatorDescriptor(RESOURCE_KEYNAME_ID,
                                      lambda x: isinstance(x,str))

observer = EasyResource(id=observer_id, typeURI="service/wps", name="funcwps")

# before server start
def make_audit_logger(app, loop):
    audit_log = logging.getLogger('funcwps_audit_log')
    init_logger(audit_log, logging.INFO)
    app.ctx.audit_log = audit_log


async def _audit_this(request: sanic.Request, *args, real_f=None, **kwargs):
    # First, run the real handler, without delay or interference
    if not real_f:
        return
    response: sanic.HTTPResponse = real_f(request, *args, **kwargs)
    if isawaitable(response):
        response = await response
    if getattr(request.ctx, "skip_audit", None):
        return
    ctx = request.app.ctx
    audit_id = str(uuid4())
    ranges = []
    total_size = 0
    h_ranges = request.headers.getall("range", [])
    for h_range in h_ranges:
        h_range = (h_range.split("bytes=", 1)[-1]).strip()
        subranges = [r.strip() for r in h_range.split(",")]
        for sr in subranges:
            try:
                if sr.endswith("-"):
                    #range-start only, we don't have the end, no way to know total size?
                    ranges.append((int(sr[:-1].strip()), None))
                elif sr.startswith("-"):
                    #range-end only. This is good, we know how many bytes were asked for
                    count = int(sr[1:].strip())
                    total_size += count
                    ranges.append((count*-1, None))
                else:
                    (start, end) = (p.strip() for p in sr.split("-", 1))
                    start, end = int(start), int(end)
                    total_size += (end - start) + 1
                    ranges.append((start, end))
            except ValueError:
                # bad range
                continue
    req_path = getattr(request.ctx, "req_path", request.path)
    if req_path == "/wps" or req_path.startswith("/wps/"):
        target_type = "data/workload/service"
    else:
        target_type = "data/file"
    target = EasyResource(f"object:{req_path}", typeURI=target_type)
    obj_path = getattr(request.ctx, "obj_path", None)
    if obj_path:
        if ":" not in obj_path:
            target.url = f"swift:{obj_path}"
        else:
            target.url = obj_path
    if total_size > 0:
        target.contentLength = total_size
    if ranges:
        tr = target.ranges = []
        for (start, end) in ranges:
            if end is not None:
                tr.append([start, end])
            else:
                tr.append([start])
    tern_token = getattr(request.ctx, "tern_token", None)
    tern_user = getattr(request.ctx, "user", None)
    landscapes_token = getattr(request.ctx, "landscapes_token", None)
    if tern_token is not None:
        credential = Credential(tern_token, "Apikey-v1")
    elif landscapes_token is not None:
        credential = Credential(landscapes_token["access_token"], "Bearer")
    else:
        credential = Credential("Anon", "Apikey-v1")
    if tern_user is not None:
        use_email = tern_user.get("email", None)
        use_username = tern_user.get("preferred_username", None)
        try:
            ident = tern_user["sub"]
        except LookupError:
            if use_email:
                ident = use_email
            elif use_username:
                ident = use_username
            else:
                ident = None
        initiator = EasyResource(id=ident, typeURI=cadftaxonomy.ACCOUNT_USER, credential=credential)
    elif landscapes_token is not None:
        use_email = landscapes_token.get("email", None)
        use_username = landscapes_token.get("preferred_username", None)
        try:
            ident = landscapes_token["sub"]
        except LookupError:
            if use_email:
                ident = use_email
            elif use_username:
                ident = use_username
            else:
                ident = None
        initiator = EasyResource(id=ident, typeURI=cadftaxonomy.ACCOUNT_USER, credential=credential)
    else:
        use_email = None
        use_username = None
        initiator = EasyResource(id="anon", typeURI=cadftaxonomy.ACCOUNT_USER, credential=credential)
    if use_email:
        initiator.email = use_email
    if use_username:
        initiator.preferred_username = use_username
    if response is not None and response.status:
        if response.status in (200, 206, 301, 302, 303, 307, 308):
            outcome = cadftaxonomy.OUTCOME_SUCCESS
        else:
            outcome = cadftaxonomy.OUTCOME_FAILURE
        reason = Reason(reasonType="HTTP", reasonCode=str(response.status))
    else:
        outcome = cadftaxonomy.OUTCOME_PENDING
        reason = Reason(reasonType="HTTP", reasonCode="PENDING")
    ev = Event(
        cadftype.EVENTTYPE_ACTIVITY,
        id=audit_id,
        eventTime=get_utc_now(),
        action=cadftaxonomy.ACTION_READ,
        outcome=outcome,
        reason=reason,
        observer=observer,
        target=target,
        initiator=initiator
    )
    ev.requestPath = req_path
    audit_dict = ev.as_dict()
    audit_dict["name"] = "audit"
    audit_dict["levelname"] = "AUDIT"
    # TODO: this audit log output is SLOW under uvicorn!
    # Its something to do with locking the stdout to write the file, just don't use uvicorn, sanic is faster
    ctx.audit_log.info(json_dumps(audit_dict))
    return response


# response wrapper
# def audit_this(f, *args):
#     def audit_wrapper(request, *args1, **kwargs1):
#         return _audit_this(request, *args1, real_f=f, **kwargs1)
#     return audit_wrapper
#     #return functools.wraps(f)(functools.partial(_audit_this, real_f=f))
def audit_this(*args, **kwargs):
    if len(args) == 1 and (isinstance(args[0], type) or callable(args[0])):
        # calling the decorator with no params
        f_or_type = args[0]
        if isinstance(f_or_type, type):
            # decorating a class
            dec = f_or_type._decorators
            if not dec:
                dec = f_or_type._decorators = []
            dec.append((audit_this, [], {}))
            return f_or_type
        elif callable(f_or_type):
            f = f_or_type
            return functools.wraps(f)(functools.partial(_audit_this, real_f=f))
        else:
            return f_or_type

    # calling the decorator with params
    def audit_wrapper(f_or_type2):
        if isinstance(f_or_type2, type):
            # decorating a class
            dec = f_or_type2._decorators
            if not dec:
                dec = f_or_type2._decorators = []
            dec.append((audit_this, args, kwargs))
            return f_or_type2
        else:
            return functools.wraps(f_or_type2)(functools.partial(_audit_this, real_f=f_or_type2))
    return audit_wrapper
