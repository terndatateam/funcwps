#
#
"""Sets up logging to either syslog or local output file"""

import sys
import logging
from pathlib import Path
from logging import DEBUG, INFO, WARNING, ERROR, CRITICAL
from logging import FileHandler, Handler, StreamHandler
from logging.handlers import SysLogHandler
from copy import copy
from .utils import is_truth
from ._app_config import USE_SYSLOG, USE_STDOUT, SYSLOG_SOCKET, DEBUG_FILE
def isatty() -> bool:
    ret = False
    try:
        ret = sys.stdin.isatty()
    except:
        ret = False
    return ret


class StderrHandler(StreamHandler):
    """
    This class is like a StreamHandler using sys.stderr, but always uses
    whatever sys.stderr is currently set to rather than the value of
    sys.stderr at handler construction time.
    """

    def __init__(self, level: int = WARNING):
        """
        Initialize the handler.
        """
        # Note, init for Handler, not StreamHandler
        Handler.__init__(self, level)

    @property
    def stream(self):
        # Always return what is _currently_ stderr.
        return sys.stderr


class StdoutFilter(logging.Filter):
    # This prevents logs _above_ this level from going to stdout (kind of the opposite of normal stderr filter)
    def __init__(self, level: int = WARNING):
        # Note, super init is object here, not logging.Filter
        object.__init__(self)
        self._level = level

    def filter(self, record: logging.LogRecord):
        if record.levelno >= self._level:
            return False
        return True


class StdoutHandler(StreamHandler):
    """
    This class is like a StreamHandler using sys.stdout, but always uses
    whatever sys.stdout is currently set to rather than the value of
    sys.stdout at handler construction time.
    """

    def __init__(self, level=INFO, upper=WARNING):
        """
        Initialize the handler.
        """
        # Note, init for Handler, not StreamHandler
        Handler.__init__(self, level)
        self.filters.append(StdoutFilter(upper))

    @property
    def stream(self):
        # Always return what is _currently_ stderr.
        return sys.stdout


def init_logger(logger: logging.Logger, level: int):
    for h in copy(logger.handlers):
        logger.handlers.remove(h)
    for f in copy(logger.filters):
        logger.filters.remove(f)
    if USE_SYSLOG:
        endpoint = SYSLOG_SOCKET or "/dev/log"
        handler = SysLogHandler(endpoint, SysLogHandler.LOG_DAEMON)
        handler.setLevel(level)
        logger.addHandler(handler)
    elif DEBUG_FILE:
        handler = FileHandler(Path(DEBUG_FILE).absolute())
        handler.setLevel(level)
        logger.addHandler(handler)
    if (USE_STDOUT.lower() == "auto" and isatty()) or is_truth(USE_STDOUT):
        h1 = StdoutHandler(level, WARNING)  # equal or above level, but below WARNING
        h2 = StderrHandler(WARNING)  # equal or above WARNING
        logger.addHandler(h1)
        logger.addHandler(h2)
    logger.setLevel(level)
    return logger
