# -*- coding: utf-8 -*-
#
"""utils.py"""
import functools
import os
import sys
from datetime import datetime, date, timedelta
from asyncio.locks import Lock

from cachetools.keys import hashkey
import base64
import json
import struct

def do_load_dotenv():
    if do_load_dotenv.completed:
        return True
    from dotenv import load_dotenv
    load_dotenv()
    do_load_dotenv.completed = True
    return True
do_load_dotenv.completed = False

def is_truth(var: str):
    """Checks if an environment variable value string is considered to be a True value"""
    return str(var).lower() in ("t", "1", "true", "y", "yes")

def isostring_to_datetime(iso_string):
    """
    :param iso_string:
    :type iso_string: str
    :return: the python datetime obj
    :rtype: datetime
    """
    if iso_string.endswith('Z'):
        iso_string = iso_string[:-1]+"+0000"
    else:
        last4 = iso_string[-4:]
        if ":" in last4:
            # something like +04:30, change to 0430
            iso_string = iso_string[:-4] + last4.replace(":", "")
    try:
       return datetime.strptime(iso_string, "%Y-%m-%dT%H:%M:%S%z")
    except:
       return datetime.strptime(iso_string, "%Y-%m-%dT%H:%M:%S.%f%z")

delta_1h = timedelta(hours=1)
delta_1m = timedelta(minutes=1)
delta_1s = timedelta(seconds=1)
def datetime_to_isostring(py_datetime):
    """
    :param py_datetime:
    :type py_datetime: datetime
    :return: the iso string representation
    :rtype: str
    """
    tz = getattr(py_datetime, "tzinfo", None)  # type: tzinfo
    if tz:
        off = tz.utcoffset(py_datetime)
        if off is not None:
            if off.days < 0:
                sign = "-"
                off = -off
            else:
                sign = "+"
            hh, mm = divmod(off, delta_1h)
            mm, ss = divmod(mm, delta_1m)
            if ss >= delta_1s:
                raise RuntimeError("ISO Datetime string cannot have UTC offset with seconds component")
            if hh == 0 and mm == 0:
                suffix = "Z"
            else:
                suffix = "%s%02d%02d" % (sign, hh, mm)
        else:
            suffix = "Z"
    else:
        suffix = "Z"

    if isinstance(py_datetime, date) and not isinstance(py_datetime, datetime):
        datetime_string = py_datetime.strftime("%Y-%m-%dT00:00:00")
    else:
        micros = getattr(py_datetime, 'microsecond', 0)
        if micros > 0:
            datetime_string = py_datetime.strftime("%Y-%m-%dT%H:%M:%S.%f")
        else:
            datetime_string = py_datetime.strftime("%Y-%m-%dT%H:%M:%S")
    return datetime_string + suffix


def aiocached(cache, key=hashkey, lock=None):
    """
    Decorator to wrap a function or a coroutine with a memoizing callable
    that saves results in a cache.
    When ``lock`` is provided for a standard function, it's expected to
    implement ``__enter__`` and ``__exit__`` that will be used to lock
    the cache when gets updated. If it wraps a coroutine, ``lock``
    must implement ``__aenter__`` and ``__aexit__``.
    """
    lock = lock or Lock()

    def decorator(func):
        async def wrapper(*args, **kwargs):
            k = key(*args, **kwargs)
            try:
                async with lock:
                    return cache[k]

            except KeyError:
                pass  # key not found

            val = await func(*args, **kwargs)

            try:
                async with lock:
                    cache[k] = val

            except ValueError:
                pass  # val too large

            return val
        new_fn = functools.wraps(func)(wrapper)
        async def update(val, *args, **kwargs):
            k = key(*args, **kwargs)
            try:
                async with lock:
                    cache[k] = val
            except ValueError:
                pass  # val too large
        async def uncache(*args, **kwargs):
            k = key(*args, **kwargs)
            try:
                async with lock:
                    del cache[k]
            except LookupError:
                pass
        setattr(new_fn, "update", update)
        setattr(new_fn, "uncache", uncache)
        setattr(new_fn, "orig_fn", func)
        return new_fn
    return decorator

undef = object()

class TempEnv(dict):
    def __init__(self, **kwargs):
        super(TempEnv, self).__init__(**kwargs)
        self._pre_enter = {}

    def __enter__(self):
        for k, v in self.items():
            old_v = os.getenv(k, undef)
            if old_v is not undef:
                self._pre_enter[k] = old_v
            os.environ[k] = v

    def __exit__(self, a, b, c):
        for k, v in self.items():
            if k in self._pre_enter:
                os.environ[k] = self._pre_enter[k]
            else:
                del os.environ[k]


def is_debugging() -> bool:
    return (gettrace := getattr(sys, 'gettrace', None)) and (gettrace() is not None)
