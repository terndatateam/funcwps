#!/usr/bin/env python3
#
"""
Application for redirecting and logging objectstore requests
"""
import sys
import os
from pathlib import Path

from sanic import Sanic
from sanic.exceptions import ServerError
import logging
if __name__ == "__main__":
    from blueprints.objectstore_tracker import ost
    from blueprints.wps import mywps
    from auth import auth_bp, make_tern_client_token, init_landscapes_client
    from audit import make_audit_logger
    from _logging import init_logger
else:
    from .blueprints.objectstore_tracker import ost
    from .blueprints.wps import mywps
    from .auth import auth_bp, make_tern_client_token, init_landscapes_client
    from .audit import make_audit_logger
    from ._logging import init_logger


def find_static_dir(here, search_for="static") -> Path:
    h = Path(here)
    while len(h.name) and (h.parent is not h):
        subdirs = [x for x in h.iterdir() if x.is_dir()]
        found = list(filter(lambda p: p.name == search_for, subdirs))
        if found:
            h = h / search_for
            break
        h = h.parent
    else:
        raise ServerError("Cannot find static dir for app.")
    return h

def make_app_logger(app, loop):
    logger = logging.getLogger('funcwps')
    init_logger(logger, logging.INFO)
    app.ctx.logger = logger


app = Sanic("funcwps")
app.before_server_start(make_app_logger)
app.before_server_start(make_audit_logger)

@app.middleware("response")
def apply_cors(request, response):
    acao = response.headers.getall("Access-Control-Allow-Origin", [])
    if len(acao) < 1:
        response.headers["Access-Control-Allow-Origin"] = "*"

app.static("/static", find_static_dir(Path(__file__).parent))
app.blueprint(mywps, url_prefix="/")
app.blueprint(auth_bp, url_prefix="/")
app.blueprint(ost, url_prefix="/object")

host = os.getenv("SERVER_LISTEN_HOST", "127.0.0.1")
port = os.getenv("SERVER_INTERNAL_PORT", "8082")
try:
    port = int(port)
except ValueError:
    port = 8082


def standalone() -> int:
    app.run(host, port, auto_reload=False, debug=False, access_log=False)
    return 0


if __name__ == "__main__":
    sys.exit(standalone())
