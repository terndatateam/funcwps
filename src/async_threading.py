from concurrent.futures import ThreadPoolExecutor
from swiftclient.multithreading import MultiThreadingManager, ConnectionThreadPoolExecutor


class AsyncConnectionThreadPoolExecutor(ConnectionThreadPoolExecutor):
    """
    A wrapper class to maintain a pool of connections alongside the thread
    pool. We start by creating a priority queue of connections, and each job
    submitted takes one of those connections (initialising if necessary) and
    passes it as the first arg to the executed function.

    At the end of execution that connection is returned to the queue.

    By using a PriorityQueue we avoid creating more connections than required.
    We will only create as many connections as are required concurrently.
    """
    def __init__(self, loop, create_connection, max_workers):
        """
        Initializes a new ThreadPoolExecutor instance.

        :param create_connection: callable to use to create new connections
        :param max_workers: the maximum number of threads that can be used
        """
        self.loop = loop
        super(AsyncConnectionThreadPoolExecutor, self).__init__(create_connection, max_workers)

    def submit(self, fn, *args, **kwargs):
        """
        Schedules the callable, `fn`, to be executed inside a thread on the eventloop

        :param fn: the callable to be invoked
        :param args: the positional arguments for the callable
        :param kwargs: the keyword arguments for the callable
        :returns: a Future object representing the execution of the callable
        """
        return self.loop.run_in_executor(super(AsyncConnectionThreadPoolExecutor, self), fn, *args, **kwargs)
        #return super(AsyncConnectionThreadPoolExecutor, self).submit(conn_fn)

class AsyncMultiThreadingManager(MultiThreadingManager):
    """
    One object to manage context for multi-threading.  This should make
    bin/swift less error-prone and allow us to test this code.
    """

    def __init__(self, loop, create_connection, segment_threads=10,
                 object_dd_threads=10, object_uu_threads=10,
                 container_threads=10):
        """
        :param loop: the async loop running
        :param segment_threads: The number of threads allocated to segment
                                uploads
        :param object_dd_threads: The number of threads allocated to object
                                  download/delete jobs
        :param object_uu_threads: The number of threads allocated to object
                                  upload/update based jobs
        :param container_threads: The number of threads allocated to
                                  container/account level jobs
        """
        self.loop = loop
        self.segment_pool = AsyncConnectionThreadPoolExecutor(
            loop, create_connection, max_workers=segment_threads)
        self.object_dd_pool = AsyncConnectionThreadPoolExecutor(
            loop, create_connection, max_workers=object_dd_threads)
        self.object_uu_pool = AsyncConnectionThreadPoolExecutor(
            loop, create_connection, max_workers=object_uu_threads)
        self.container_pool = AsyncConnectionThreadPoolExecutor(
            loop, create_connection, max_workers=container_threads)
