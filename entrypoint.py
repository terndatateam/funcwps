#!/usr/bin/env python3
#
"""
Use this file to run the application directly on the commandline, eg:
> python3 entrypoint.py
Or with uvicorn:
> uvicorn -args entrypoint:app

Note, you can run the module directly (without using this file)
> python3 -m src
"""
import sys
from os import path

here = path.dirname(__file__)
sys.path.insert(0, here)

from src import application
from src.application import app

__all__ = ("app",)

if __name__ == "__main__":
    sys.exit(application.standalone())
